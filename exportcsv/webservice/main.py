from flask import Flask, request, redirect, jsonify, Response
from datetime import datetime
import subprocess
import logging
import json
from flask_cors import CORS
from flask_cors import cross_origin

app = Flask(__name__)
CORS(app, resources={r"/exportcsv": {"origins": "*"}})


@app.route('/')
def api_root():
    return 'CSV Export'


@app.route('/exportcsv', methods=['POST'])
@cross_origin(origin='*')
def api_exportcsv():

    id = request.json['id']
    category= request.json['category'].replace(" ","_")
    logging.info('Unique Id ======> ' + id)
    logging.info('Temp table ======> ' + category)
    csvfile = "gs://merck_dev_export/"+ category+"_"+id+"-*.csv";
    cmd_str = "gcloud auth activate-service-account --key-file=GCE_serviceaccount.json"
    cmd_str = cmd_str + ";" + "gsutil -m compose "+csvfile+" gs://merck_dev_export/"+ category+ "_" +id+".csv"
    cmd_str = cmd_str + ";" + "gsutil -m rm gs://merck_dev_export/"+ category+ "_"+id+"-0*.csv"

    print 'Command ======> ', cmd_str
    logging.info('Command ======> ', cmd_str)

    try:
        subprocess.check_output(cmd_str, shell=True)
    except subprocess.CalledProcessError as e:
        print e.output

    url = "https://storage.cloud.google.com/merck_dev_export/"+ category+ "_" +id+".csv"
    logging.info('Url for download ======> ' + category)
    return jsonify(cs_url=url)


if __name__ == '__main__':
    app.run()
