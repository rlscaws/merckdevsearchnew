//defining globals
let cognitoUser;
let interval;
let isSummary = false;
let authorizer;
let emailId;
let org;
var pageTokens = []; // this array will cache the pageTokens
var pageidx = 0; // current page index
var jobId; // define an empty global to keep track of the jobId
var pageSize = 20; // nmumber of records per page
var timeout = 300000; // timeout in ms for bq api calls
var resultCount = 0; // the # of results returned froma  bq api calll
var pCol = []; // pivoted columns to be selected in queries
var bqView = sum_table; // used for authentication and initial load of page
var sortOn = ''; //sort results based on this column
var orderBy = 'DESC'; // 'ASC' or 'DESC' ?should we set default?
var filtersObj = {"NOT": []}; // this list should contain statements like compound_id in [..]
var filtersList = []; // this will be a list of strings populated by readFilters that contains SQL syntax for the UI inputs from each row
var filterRows = []; // this holds the id # of the html of the filter row (ie. if user clicks plus 6x then removes the third row filterRows will contain [1,2,4,5,6])
var count = []; // keys are columns to be counted values are names for count ie 'compound_id':'Compounds'
var is_summ = false; // is this a summary query
var schema; //this is set by getSchema
var sel; //select
var x; // counts number of filter conditions
var pref = [] // holds data from filter conditions
var a; var j;

var isEditQuery = false;        // flag use to enable/disable the query builder textarea
var queryForCSVdownload = '';   // query used by the download feature
var selected_category = '';     // variable container for the selected category in layer 1
var selected_datamartView = ''; // variable container for the selected datamarts / view(s) in layer 2
var selected_datatypes = [];    // variable list container for the selected datatypes / sub-categories in layer 3
var selected_Programs = [];     // variable list container for the selected programs / projects
var selected_assays = [];       // variable list container for the selected assays
var selected_fields = [];       // variable list container for the selected fields / results in layer 4
var tempFieldsObjList = [];     // variable list container for the field object used for checking of "fieldName"
                                // and it's corresponding "isPivoted" and "dataType"
var col = [];                   // variable list container for non-pivoted selected fields
var viewsInputType = '';        // variable to check what input type (checkbox or radiobutton) to display in layer 2
                                // if category is "Correlate Data" or not
var tempSelectedViews = [];     // temporary variable list container to save and monitor the newly selected/deselected
                                // items in layer 2 specifically when category is "Correlate Data"
var tempSelectedprogram = [];        // temporary variable list container to save and monitor the newly selected/deselected
                                // items in layer 3 specifically when category is "Correlate Data"
var tempselectedProg = [];    // temporary variable list container of objects containing the selected datatype

var tempselectedassays = [];        // temporary variable list container to save and monitor the newly selected/deselected
var tempSelectedFields = [];    // temporary variable list container of objects containing the selected field
                                // and its corresponding selected datatype in layer 3
var tempSelectedSubCat = [];
var pageNum = 0;                // the page number used in pagination
var resultCount = 0;            // the total result count
var totalPageCount = 0;         // the total number of page used in the pagination
var lastDeSelectedView = '';// variable that contains the last selected datatype
var lastDeSelectedProgram = '';// variable that contains the last selected datatype
var lastDeSelectedAssay = '';// variable that contains the last selected datatype
var lastDeSelectedDatatype = '';// variable that contains the last selected datatype
var savedFilterInputs = [];     // list container of the inputted and selected items in filter row stored in an object
var savedFilterInputObj = {};   // object declaration that will contains the inputted and selected items in filter row
var isInitialSelect = true;     // flag to check if initial select of item in layer 4
                                // this is necessary to show the selected item in filter rows

// this list holds the names of datatypes that should be automatically selected
var autoSelectAssays = ["Metabolite ID","Physicochemical Properties","Physiochemical Properties"];

// this list holds the names of datatypes that should be automatically selected
var autoSelectDatatypes = ["Metabolite ID","Metadata_DLCsum","Metadata_DLCevent", "In Vivo PKPD Assay Results", "PPDM Assay Results", "In Vitro Pharmacology Assay Summary Results", "In Vitro Pharmacology Assay Individual Results"];

//autoselection for fields
// L-number info
var autoSelectFields = ["parent_id", "compound_id", "sample_id"];
//program / project
autoSelectFields = autoSelectFields.concat(["project_long_name","program_name","project_ChemProp","program_invivoPKPD","program_MetID","sample_program_name_PPDM"]);
//assay name
autoSelectFields = autoSelectFields.concat(["assay_long_name", "procedure_name_invivoPKPD", "study_type_PPDM"]);
var view_dict = { // maps suffix to actual table reference
    "DLCevent": "merck_dev_access_control.v_dlc_test_event_result",
    "DLCsum": "merck_dev_access_control.v_dlc_sum_result_cmpd",
    "MetID": "merck_dev_access_control.v_webmtbs_unified_view",
    "invivoPKPD": "merck_dev_access_control.v_in_vivo_pkpd",
    "ChemProp": "merck_dev_access_control.v_compound_phys_properties",
    "InvitroPharm": "merck_dev_access_control.v_in_vitro_assay_results",
    "InvitroPharmSum": "merck_dev_access_control.v_in_vitro_assay_sum_results",
    "PPDM": "merck_dev_access_control.v_tier2_ppdm"
};
var base_dict = { // this nested object holds the mapping between abbreviations and base columns for pivoting in DLC
    "DLCsum":{
        "AVG":"AVERAGE_VALUE",
        "AVG_QUAL":"AVERAGE_VALUE_QUALIFIER",
        "AVG_UNIT":"AVERAGE_VALUE_UNIT",
        "RXD_QUAL":"avg_res_max_dose_qualifier",
        "RXT":"avg_res_max_included_count",
        "RXS":"avg_res_max_standard_deviation",
        "RND_QUAL":"avg_res_min_dose_qualifier",
        "RNT":"avg_res_min_included_count",
        "RNS":"avg_res_min_standard_deviation",
        "RXD":"avg_response_max_dose",
        "RND":"avg_response_min_dose",
        "GEO_QUAL":"geometric_average_qualifier",
        "GEO":"geometric_average_value",
        "GSE":"geometric_standard_error",
        "GED":"global_event_date",
        "ICT":"included_count",
        "LVD":"latest_value_date",
        "MXD":"max_dose",
        "MAX":"maximum_value",
        "MAX_QUAL":"maximum_value_qualifier",
        "MED_RES_QUAL":"median_response_qualifier",
        "MRD":"median_result_dose",
        "MRD_UNIT":"median_result_dose_unit",
        "MRR":"median_result_response",
        "MND":"min_dose",
        "MIN":"minimum_value",
        "MIN_QUAL":"minimum_value_qualifier",
        "DEV":"standard_deviation",
        "ERR":"standard_error",
        "VCT":"value_count",
        "VAR":"variance_value"
    },
    "DLCevent":{
        "VALUE":"value",
        "VALUE_QUAL":"value_qualifier",
        "VALUE_UNIT":"value_unit"
    }
  };
var pro_dict = {
      "DLCevent": "project_long_name", // ??
      "DLCsum": "project_long_name", // ??
      "invivoPKPD": "program",
      "ChemProp": "project", // ??
      "MetID": null, // ??
      "PPDM": "sample_program_name", // ?? project_short_name ?? depends on selected assay ??
      "InvitroPharm": "project_short_name", // ??
      "InvitroPharmSum": "project_short_name" // ??
}

var assay_dict = {
      "DLCevent": "assay_long_name", // ??
      "DLCsum": "assay_long_name", // ??
      "invivoPKPD": "procedure_name", // no assay information for invivo data
      "ChemProp": null, // no assay information for ChemProp data
      "MetID": null, // ?? work in progress
      "PPDM": "study_type", // ?? pretty surre this is all one assay_id = 11377.0
      "InvitroPharm": "assay_long_name", // ??
      "InvitroPharmSum": "assay_long_name" // ??
}

  //error message body
var searchModalBody = "The current search is taking longer than expected possibly due to the amount of data being retrieved or inadequate filtering criteria. Please consider applying more restrictive filtering or adding additional filtering criteria to reduce your search result. If you wish to continue with the current search, click CONTINUE. If you wish to cancel, click CANCEL SEARCH."
var downloadModalBody="The current search result exceeds the recommended size limit. The download may take longer than expected or time out. Please consider applying more restrictive filtering or adding additional filtering criteria to reduce your search result. You may also want to use ODBC connection (e.g. within Spotfire) to download your file. If you wish to continue with the current download, click OK. If you wish to cancel, click CANCEL DOWNLOAD."

//helper filter function for getting only unique values of an array
function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

//  This function pushes the mathematical symbol to the symbolSelect dropdown
function getSymbol(x) { //populates the symbol in a filter row
    var symbol = $("#symbol"+ x).empty();
    var symbolselect = $("<select>", {'id': 'symbolSelect'+ x, 'class': 'form-control'}).appendTo(symbol);
    var str_symboloption = ["=", "in", "contains", "does not contain", "exists"];
    for (var i in str_symboloption) {
        symbolselect.append($("<option>").html(str_symboloption[i]));
    }
    $('#symbol'+x).append(symbolselect);
    return symbolselect.html();
}

// shows datepicker
function showDatePicker(id, row_idx) {
    var domSelector = $('#' + id + ' option:selected'); // we get the symbol ID
    var selected_symbol = domSelector.val().trim(); // we get the value of that symbol
    var textAreaDiv = domSelector.parent().parent().next(); // we get the textarea div of that corresponding symbol
    // console.log('Selected Symbol =======> ', selected_symbol);
    if (['before', 'after', 'between'].indexOf(selected_symbol) > -1) {
        textAreaDiv.empty(); // we empty the textarea div then replace it with the date picker
        textAreaDiv.append('<div class="input-group date" id="dtpicker'+ row_idx +'a"> \
                                <input type="text" id="inputprogrampicker'+ row_idx +'a" class="form-control" /> \
                                <span class="input-group-addon"> \
                                    <img src="images/si-glyph-calendar-1.svg" height="20" width="20"/> \
                                </span> \
                            </div>');

        // initialize the datetimepicker under this documentation:
        // http://eonasdan.github.io/bootstrap-datetimepicker/#bootstrap-3-datepicker-v4-docs
        // then modify the date formatting to MM/DD/YY
        $('#dtpicker'+ row_idx +'a').datetimepicker({
             format: 'MM/DD/YYYY'
        });

        if (selected_symbol === 'between') {
            // if the selected symbol is 'between', we append another div containing a new datepicker
            textAreaDiv.append('<div class="input-group date" id="dtpicker'+ row_idx +'b"> \
                                    <input type="text" id="inputprogrampicker'+ row_idx +'b" class="form-control" /> \
                                    <span class="input-group-addon"> \
                                        <img src="images/si-glyph-calendar-1.svg" height="20" width="20"/> \
                                    </span> \
                                </div>');
            // initialize the 2nd datetimepicker
            $('#dtpicker'+ row_idx +'b').datetimepicker({
                 format: 'MM/DD/YYYY',
                 useCurrent: false
            });

            // validation for in between operation for two dates
            $('#dtpicker'+ row_idx +'a').on("dp.change", function (e) {
                $('#dtpicker'+ row_idx +'b').data("DateTimePicker").minDate(e.date);
            });
            $('#dtpicker'+ row_idx +'b').on("dp.change", function (e) {
                $('#dtpicker'+ row_idx +'a').data("DateTimePicker").maxDate(e.date);
            });
        }
        // we re-add the plus and minus buttons for adding filter row
        addPlusAndMinusButtons(textAreaDiv, row_idx);
    }
    else {
        if (selected_symbol === 'exists') {
            // we just remove the datepicker and textarea if selected item is 'exists'
            textAreaDiv.empty();
            // we re-add the plus and minus buttons for adding filter row
            addPlusAndMinusButtons(textAreaDiv, row_idx);
        }
        else {
            textAreaDiv.empty();
            textAreaDiv.append('<textarea rows="2" id="searchPhraseCmpdID' + row_idx +'" class="form-control" \
                                style="width: 80%" onchange="saveFilterInputs()"></textarea>');
            // we retain the value of the textarea
            if (savedFilterInputs[row_idx - 1]) $("textarea#searchPhraseCmpdID" + row_idx).val(savedFilterInputs[row_idx - 1].inputtedText);
            // we re-add the plus and minus buttons for adding filter row
            addPlusAndMinusButtons(textAreaDiv, row_idx);
        }
    }
}

//this function creates the +/- buttons for the filter rows
function addPlusAndMinusButtons(textAreaDiv, row_idx) {

    if (row_idx > 1) { // no minus button as to prevent user from removing all filter rows
        console.log("appending minus button")
        // textAreaDiv.append('&nbsp;<img src="images/minusRemoveRow.png" class="remove_field" onclick="removeFilterRow(' + row_idx + ')/>&nbsp;');
        $('#entry' + row_idx).append('<img src="images/minusRemoveRow.png" class="remove_field" width: 5%; onclick="removeFilterRow(' + row_idx + ');"/>');
    }
    $("#entry" + row_idx).append('<img src="images/plusAddRow.png" class="add_field_button" onclick="addFilterRow(' + row_idx + ');" />');
}

function updateSymbols(tableColSelectIDindex, selectedCol){ //updates symbol in a filter row when field selection changes
    // we look into the selected fields objects to get the datatype
    var fieldObj = tempFieldsObjList.find(o => o.fieldName === selectedCol); // contains the filedName and fieldDataType ie. "STRING"

    if (fieldObj) {
        var type = fieldObj.fieldDataType;
        var symboloption;
        var i = tableColSelectIDindex;
        var symbol = $("#symbol" + i).empty();
        var symbolselect = $("<select>", {'id': 'symbolSelect' + i,
                                        'class': 'symbol_ddown form-control',
                                        'onchange': 'showDatePicker(this.id,' + i + ')'}).appendTo(symbol);
        var str_symboloption = ["=", "in", "contains", "does not contain", "exists"];
        var num_symboloption = ["=", "<", ">",">=","<=", "exists"];
        var time_symboloption = ["before", "after", "between", "exists"];

        if (type == "STRING"){
            symboloption = str_symboloption;
        }
        else if (type == "TIMESTAMP"){
            symboloption = time_symboloption;
        }
        else { // it is a number
            symboloption = num_symboloption;
        }
        for (var k in symboloption) {
            symbolselect.append($("<option>").html(symboloption[k]));
        }

        $('#symbol'+ i).replaceWith(symbol);

        showDatePicker('symbolSelect' + i, i);
    }
}

//  This function pushes the operator  [AND | OR] to the operatorSelect dropdown in each filter row
function getOperator(x) {
    var operator = $("#operator" + x).empty();
    var operatorselect = $("<select>", {'id': 'operator_select' + x, 'class': 'form-control', 'onchange': 'saveFilterInputs()'}).appendTo(operator);
    var operatoroption = ["AND", "OR"];
    for (var i in operatoroption) {
        operatorselect.append($("<option>").html(operatoroption[i]));
    }
    return operatorselect.html();
}

function setTable(){ //sets the initial table when testing user's authentication on initial page load
    // this is a legacy funtion called as soon as the page loads and the user authenticates .. it initializes the page
    bqView = "merck_dev_access_control.v_dlc_sum_result_cmpd"

    //show the search buttons
    $("#submit_button").show();
    $("#summary_button").show();

    let userId = localStorage.getItem("lookup");
    console.log("LOOKUP: ", userId);

    let body = {
      lookupId: userId
    }

    console.log("BODY: ", body);
    // console.log("SEARCH USER IP:", searchUserIP)
    let stmntStr = JSON.stringify(body);
    $.ajax({
        type: 'POST',
        url: 'https://sp0dd5huq2.execute-api.us-east-2.amazonaws.com/dev',
        data: stmntStr,
        crossDomain: true,
        contentType: 'application/json',
        success: function(data, status) {
          if(data.statusCode === "200") {
            cognitoUser = "usr_" + data.userName;
            authorizer = data.authorization;
            org = data.organization;
            emailId = data.emailId;
            try{
                populateCategories();
                hideSummaryTable();
                hideResultsTable();
                hideResultsLoadingSpinner();

                if (!$('#input_fields_wrap1').length){
                    addFilterRow(0); // adds the first filter row
                }

                $("#filterCriteriaTxt").show();
                if(!$('#querytxtbox').length){
                    addQueryTxtbox();
                }
            }
            catch (e) {
                alert(e);
                $("tableCols").hide();
                schema = '';
            }
          } else {
            console.log("USER DOES NOT MATCH");
            window.location.href = "/";
          }

    }, error: function(err) {
      console.log(err);
    }
  });
}

//reset pagination related globals upon a new search or sorting
function resetPagination(){
    pageTokens = []; // this array will cache the pageTokens
    pageidx = 0;
    jobId; // define an empty global to keep track of the jobId
    pageSize = 20;
}

//reset query related globals
function resetQuery(){
    // cols = []; //columns to be selected in the query
    sortOn = ''; //sort results based on this column
    orderBy = 'DESC'; // 'ASC' or 'DESC' ?should we set default?
    filtersObj = {"OR": [], "AND": [], "NOT": []}; // this list should contain statements like compound_id in [..]
    filtersList = [];
    duplicates = false;
    selectSize = 50;
    count = [];
}

// load the selected fields in all of the fields drop down in filter row section
function loadSelectedfields(row_idx) {

    var colresults = $("#tableCols" + row_idx).empty();
    var select = $("<select>", {'id': 'select' + row_idx,
                                'class': 'form-control',
                                'onchange': 'setDomIDIndex(this.id)'}).appendTo(colresults);

    for (i in selected_fields) {
        $("<option>", {'id': 'option' + row_idx}).text(selected_fields[i]).appendTo(select);
    }
}

// this function reads the user inputs from the filter rows into the filtersList object for parsing the WHERE clause of the SQL
function readFilters(view){
  var kk = 0; // loop variable over index of filter row (top to bottom)
  var i = 0;
  // filtersList = [];
  console.log('========= Reading Filters ===========');
  console.log("for view ", view);
  console.log("x ", x)
// note that filterRows contains the reference #'s to the divs for the filter rows that have not been deleted
  var numOR = 0;
  while (kk < filterRows.length){ // reads filter rows top to bottom
      var exists = false; // boolean = if filterRows[kk] is an "is not null filter"
      var myX = filterRows[kk]; // # of the filter row for this loop


      c = wrapper.find(':selected').map(function(){ // this reads the dropdown selections [AND/OR1, field_name1, symbol1, AND/OR2, field_name2, symbol2,...]
          return $(this).text();
      });

      pref = jQuery.extend(true,[],c); //pref is a list containing the first three inputs: operator, field and symbol

      if (kk === 0 && pref.indexOf("exists") > -1 && x === 1){ // handles the lack of AND/OR for first filter
          pref = pref;
      }
      else if (pref[0] === "AND"){ //handles issue if user hits select all in layer 4
        pref = pref;
      }
      else{

          pref = [" "].concat(pref)
      }
      var opp = pref[i]; // "AND" or "OR"
      // field naming corrects special characters this is important for pivoted fields
      var field = pref[i+1].toString().replace(/\|\|/g,"_").toString().replace(/[%]/g,"percent"); //field name
      field = field.replace(/[-|/\.]/g,"_").toString(); //field name
      var sym = pref[i+2]; //symbol ie <, >, =, exists, before, after, contains, in etc. depending on selections
      if(field === "exists"){
          sym = field;
          field = opp;
          pref = [" "].concat(pref); // don't disrupt subsequent rows
      }

      if (sym === "before"){ // parses the date symbols for the query
          sym = "<";
      }
      else if (sym === "after"){
          sym = ">";
      }

      if(sym != "exists"){
          var str = $("#searchPhraseCmpdID" + myX).val();
          var txtquery = "";
          var isDate = false;
          try{ // this handles everything but date picker
              if (sym === "in"){ // this means we expect user to enter space, comma OR newline separateed entries to form a list (ie. compound_id in L-000,L-0001,L-0002)
                  var myString = str.toString() // .replace(/\n/gm, ' ');  // replace newlines with spaces
                  myString = myString.toString().replace(/\,/g,'\n'); // replace commas with spaces
                  var res = myString.split("\n"); // separate the list on spaces
                  $.each(res, function( index, value ) { // puts quotes around strings for in operator
                      console.log(value);
                      var searchPhraseCmpdID = value.toString().replace("\'","\\\'");
                      console.log(searchPhraseCmpdID);
                      if (res.length > 1){
                          if (isNaN(searchPhraseCmpdID)){
                            txtquery = txtquery + "UPPER('" + searchPhraseCmpdID + "'),";
                          }
                          else{
                              txtquery = searchPhraseCmpdID;
                          }
                      }
                      else{
                          if (isNaN(searchPhraseCmpdID)){
                            txtquery = "UPPER('" + searchPhraseCmpdID + "'))";
                          }
                          else{
                              txtquery = searchPhraseCmpdID;
                          }
                      }


              txtquery = txtquery.toString().replace("\\","\\\\"); // escape the backslashes (importnant if searching on smile string)

              });
            txtquery = txtquery.substring(0,txtquery.length - 1); //remove trailing comma
            } // end if sym === "in"

            else {
                txtquery = str.toString().replace("\\","\\\\").replace("\'","\\\'").replace("\"","\\\""); // escape special characters (importnant if searching on smile string)
            }
            // console.log("txtquery ", txtquery)
            if (sym === "contains"){ // handles wild card search for contains operator
                  // console.log("replacing contains with like '%*%' .....")
                  sym = "like";
                  txtquery = "UPPER('%" + txtquery + "%')"
                  // console.log("===> txtquery = ", txtquery)
            }
            else if (sym === "does not contain"){ // handles negative wildcard search
                  // console.log("replacing does not contain with not like '%*%' .....")
                  sym = "not like";
                  txtquery = "UPPER('%" + txtquery + "%')"
                  // console.log("===> txtquery = ", txtquery)
            }
            else if (sym === "="){ // handles if txtquery is text .. if not it is a number
                var fieldObj = tempFieldsObjList.find(o => o.fieldName === field); // used for getting the data type of the selected field
                var type = fieldObj.fieldDataType;
                console.log("type: ",type);
                if (type === "STRING"){ // if string needs quotes (note timestamp will already have thrown and error is handled by the catch )
                    txtquery = "UPPER('" + txtquery + "')";
                }
                else{ // if number does not need quotes
                    txtquery = txtquery;
                }
            }
          }
          catch (e) { // searchPhraseCmpdID is undefined cuz it's now a date picker
              isDate = true;
              // console.log("===> date picker for myX = ", myX);
              //parsing datepicker input to correct format for SQL
              var raw_a = $("#dtpicker" + myX + "a").data("DateTimePicker").viewDate()._d.toLocaleDateString(); //date format is MM/DD/YYYY
              var a = raw_a.split("/");
              a = [a[2], a[0], a[1]].join("-"); // date format YYYYMMDD
              txtquery = " TIMESTAMP('" + a + "') ";
              if (sym === "between"){
                  var raw_b = $("#dtpicker" + myX + "b").data("DateTimePicker").viewDate()._d.toLocaleDateString();
                  var b = raw_b.split("/");
                  b = [b[2], b[0], b[1]].join("-"); // date format YYYYMMDD
                  txtquery = txtquery + " and TIMESTAMP('" + b + "') ";
              }
          }

          if (txtquery === "" || txtquery === "''"){ //error handling when user forgets to finish inputs for a filter row
              alert("ERROR: Cannot have empty filter row! Unless 'exists' filter");
          }
          else if (!isDate){
              txtquery = '(' + txtquery + ')';
          }
      }
      else{ // sym === "exists"
          txtquery = "";
          exists = true;
      }
      //read the "prefix" of each filter
      if (kk > 0){ //not first filter row
          if (field.split("_").pop().toLowerCase() === view.toString().toLowerCase() || (view === "ChemProp" && (field.split("_").pop() === "MeasChemProp" || field.split("_").pop() === "CalcChemProp") )){ //checks that this filter appplies to this view
              if (opp === "OR"){ //special because groups filters on either side .. this is how the top to bottom AND OR logic is done
                  numOR++; //counts number of OR filters on this view
                  if (exists){
                      filtersList.push([")",opp, "(", field, " is not null " ].join(' '));
                  }
                  else{
                      filtersList.push([")",opp, "(", field, sym, txtquery ].join(' '));
                  }
              }
              else{ //opp === AND
                  if (exists){
                      filtersList.push([opp, field, " is not null "].join(' '));
                  }
                  else{
                      filtersList.push([opp, field, sym, txtquery].join(' '));
                  }
              }
          }
      }
      else{ //first filter (opp omitted)
        if (field.split("_").pop().toLowerCase() === view.toString().toLowerCase() || (view === "ChemProp" && (field.split("_").pop() === "MeasChemProp" || field.split("_").pop() === "CalcChemProp") )){ //checks that this filter appplies to this view
            if (exists){
                filtersList.push([field, " is not null "].join(' '));
            }
            else {
                filtersList.push([field, sym, txtquery].join(' '));
            }
        }
      }

      i += 3; // this is used for reading pref
      kk++; // this is used for reading filterRows
  } //end reading filter rows

}

// this function creates a SQL query for bq form the user entered inputs to the controls on the ui
function writeQuery(view, isTemp){
        selected_fields.forEach(f => filterPivotedAndNot(f)) // makes sure col and pCol are properly populated with autoselected fields
        try{ //error handling for javascript errors within this function
        var q = ""; // q holds the query string we build throughout this funciton
        // console.log("selected_fields =====>", selected_fields);
        var myfields = []; //should hold only the fields for this raw view
        var myCol = []; // should hold only the unpivoted fields for this raw view
        var mypCol = []; // should hold only the picoted columns
        var mySelected_Programs = []; //list holding the programs applicable to this view
        var myselected_assays = []; // list holding the assays applicable to this view

        if (selected_category === "Correlate Data"){ // we are using this function to write the sub queries
            for (var i = 0; i < selected_fields.length; i++){ //populate myfields
                if (view === "ChemProp"){ // this handles the 3 suffixes that pertain to the ChemProp view
                    var suff = selected_fields[i].toString().split("_").pop();
                    if (suff === view || suff === "CalcChemProp" || suff === "MeasChemProp"){
                        myfields.push(selected_fields[i]);
                    }

                }
                else{ // not ChemProp
                    if (selected_fields[i].toString().split("_").pop() === view){
                        console.log("yes");
                        myfields.push(selected_fields[i]);
                    }
                }
            }
            for (var i=0; i < col.length; i++){ //populate myCol which holds non-pivoted fields for this view
                if (view === "ChemProp"){ // needs special handling becuase the multiple sub categories but no pivoting
                    var c_suff = col[i].toString().split("_").pop();
                    if (c_suff === "ChemProp" || c_suff === "CalcChemProp" || c_suff === "MeasChemProp"){
                        myCol.push(col[i]);
                    }
                }
                else{ // not chem prop
                    var c_suff = col[i].toString().split("_").pop();
                    if (c_suff === view){
                        myCol.push(col[i]);
                    }
                }
            }
            for (var i=0; i < pCol.length; i++){ //populate mypCol which holds pivoted fields for this view
                if (pCol[i].toString().split("_").pop() === view){
                    mypCol.push(pCol[i]);
                }
            }
            if (!!pro_dict[view]){
              for (var i=0; i < selected_Programs.length; i++){ //populate mypCol which holds pivoted fields for this view
                  if (selected_Programs[i]){
                      var lp = selected_Programs[i].toString().split("_");
                      var v = lp.pop(); // getting rid of the view suffix
                      if (view !== "MetID" && v === view){
                          if(lp.indexOf("(null)") > -1){ // handles (null) program
                              mySelected_Programs.push(null);
                          }
                          else{
                              mySelected_Programs.push(lp.join("_"));
                          }
                      }
                  }
                  else {
                      mySelected_Programs.push(null);
                  }
              }
            }
            if (!!assay_dict[view]){ // filter on assays if this view has assays
              for (var i=0; i < selected_assays.length; i++){ //populate mypCol which holds pivoted fields for this view
                if (selected_assays[i]){
                  var la = selected_assays[i].toString().split("_");
                  var v = la.pop();
                  if (v === view){
                          if(la.indexOf("(null)") > -1){ // handles (null) assay
                              myselected_assays.push(null);
                          }
                          else{
                              myselected_assays.push(la.join("_"));
                          }
                  }
                }
                else {
                    myselected_assays.push(null);
                }
              }
            }

        }
        else{ // not correlate data
            myfields = selected_fields;
            myCol = col;
            mypCol = pCol;
            if(!!assay_dict[view]){ // populate only myselected_assays only if assay information is in this view
                for (var i=0; i < selected_assays.length; i++){ //populate mypCol which holds pivoted fields for this view
                    if (selected_assays[i]){
                        var la = selected_assays[i].toString().split("_");
                        var v = la.pop(); // getting rid of view suffix
                          if(la.indexOf("(null)") > -1){
                              myselected_assays.push(null);
                          }
                          else{
                              myselected_assays.push(la.join("_"));
                          }
                    }
                    else {
                        myselected_assays.push(null);
                    }
                }
            }
            else{
                myselected_assays = [];
            }
            if(!!pro_dict[view]){
                if(pro_dict[view].length > 0){ // populate only myselected_assays only if program information is in this view
                    for (var i=0; i < selected_Programs.length; i++){ //populate mypCol which holds pivoted fields for this view
                        if(selected_Programs[i]){
                            var lp = selected_Programs[i].toString().split("_");
                            var v = lp.pop(); // getting rid of view suffix
                              if(lp.indexOf("(null)") > -1){
                                  mySelected_Programs.push(null);
                              }
                              else{
                                  mySelected_Programs.push(lp.join("_"));
                              }
                        }
                        else{
                            mySelected_Programs.push(null);
                        }
                    }
                }
                else{
                    mySelected_Programs = [];
                }
            }
            else{
                mySelected_Programs = [];
            }
        }
        if (selected_category === "Correlate Data"){
            if (view === "MetID"){ // handles issue with aliasing of parent_id_MetID field when selected
                var sel_fields = ["parent_id"].concat(myfields);
                q = q + "SELECT DISTINCT " + sel_fields.join(", ").toString().replace(/\|\|/g,"_").replace(/[-|/\.]/g,"_").toString().replace(/[%]/g,"percent") + " FROM ("
            }
            else{
                q = q + "SELECT DISTINCT " + joiners_dict[view].join(", ") + ", " + myfields.join(", ").toString().replace(/\|\|/g,"_").replace(/[-|/\.]/g,"_").toString().replace(/[%]/g,"percent") + " FROM ("
            }
        }
        else{
            if (view === "DLCsum" || view === "DLCevent"){
                //create an outer query for display columns
                q = q + "SELECT DISTINCT " + myfields.join(", ").toString().replace(/\|\|/g,"_").replace(/[-|/\.]/g,"_").toString().replace(/[%]/g,"percent") + " FROM ("
                //include primary key info in inner query
                q = q + "SELECT DISTINCT " + joiners_dict[view].join(", ") + ", " + myfields.join(", ").toString().replace(/\|\|/g,"_").replace(/[-|/\.]/g,"_").toString().replace(/[%]/g,"percent") + " FROM ("
            }
            else{
                q = q + "SELECT DISTINCT " + myfields.join(", ").toString().replace(/[-|/\.]/g,"_").toString().replace(/\|\|/g,"_").replace(/[%]/g,"percent") + " FROM ("
            }
        }

        q = q + ' SELECT '; //begins selecting on raw view

        if (selected_category === "Correlate Data"){ // add joiners to sub query for not for display
            if (view === "MetID"){
                q = q + " parent_id_MetID as parent_id";
                if (myCol.length > 0){
                    q = q + ", ";
                }
            }
            else{
                q = q + joiners_dict[view].join(", ");
                if (myCol.length > 0){
                    q = q + ", ";
                }
            }
        }
        else if (view === "DLCsum" || view === "DLCevent"){
            q = q + joiners_dict[view].join(", ")
            q = q + ", "
        }
        for (var i =0; i < myCol.length; i++){
            console.log("adding field ", myCol[i]);
            if (view === "MetID"){ //source columns already have _MetID suffix
                q = q + myCol[i] + ", ";
            }
            else{
                q = q + getSourceColumn(myCol[i]) + " AS " + myCol[i] + ", ";
            }
        }
        if (myCol.length > 0){
            q = q.substring(0,q.length-2) + ' '; //remove trailing comma
        }
        //add the aggregation for the selected pivots
        // console.log("mypCol: ",mypCol)
        if (mypCol.length > 0){ //if pivoting
           q = q + ', ' ; // comma after non-pivoted columns and/or joiners
           for (var i=0; i < mypCol.length; i++){
              var pivot = mypCol[i]; // holds the name of this pivoted column
              var pivotList = pivot.toString().split("_"); //deconstruct the pivoted column
              pivotList.reverse();
              console.log("reversed: ", pivotList);
              var rtn = ""; //result_type_name
              while (rtn === ""){ //avoid leading underscore
                rtn = pivotList.pop();
              }
              rtn = rtn.toString().replace(/\|\|/g," "); // handles spaces in rtn
              rtn = rtn.toString().replace(/\|/g, "_"); //handles result_type_names with underscores in them like 95%_CI
              var val = pivotList.pop(); // result_value_type_name
              val = ((val === "") ? "VALUE" : val); // cosmetic thing for naming convention
              val = val.toString().replace(/\|/g,"_");  //handles result_value_type_names with underscores in them like FRACTION_UNBOUND
              pivotList.reverse(); // get it facing the right way aggain
              var sourceView = pivotList.pop(); // remove view origin suffix
              base = pivotList.join("_");
              // gets things ready to make a BQ acceptable fieldname
              var frtn = (('0123456789'.indexOf(rtn[0]) >=0) ? "_" + rtn : rtn).toString().replace(/[\s-|/\.]/g,"_").toString().replace(/[%]/g,"percent");
              var fval = ((val === "VALUE")? "" : val).toString().replace(/[\s-|/]/g,"_").toString().replace(/[%]/g,"percent")

              if (val === "DLCsum" || val === "DLCevent"){ // handles issue with variant_type_name pivot
                  sourceView = val;
              }

              //note base_dict maps abbreviations to actualsource base columns
              if (sourceView === "DLCsum"){ // DLCsum  pivoting
                  var dict = base_dict[sourceView]; // select the nested object containing the correct mapping
                  if (Object.keys(dict).indexOf(base) >= 0){ //checks that this is a result_type_name pivot
                      q = q + "MAX(CASE WHEN result_type_name='" + rtn + "' and result_value_type_name = '" + val + "' THEN " + dict[base] + " ELSE NULL END) AS " + [frtn, fval, base, sourceView].join('_') + ", ";
                  }
                  else{ // variant_type_name  pivot
                      q = q + "MAX(CASE WHEN variant_type_name='" + rtn + "' THEN variant_value ELSE NULL END) AS " + [frtn, sourceView].join('_') + ", ";
                  }
              }
              else if (sourceView === "DLCevent"){
                  var lb = base.split("_");
                  base = lb[lb.length-1]
                  //handles issue with parsing base columns with _
                  if (base === "UNIT"){
                      base = "VALUE_UNIT";
                  }
                  else if (base === "VALUE"){
                      base = "VALUE";
                  }
                  else if (base === "QUAL"){
                      base = "VALUE_QUAL";
                  }

                  var dict = base_dict[sourceView]; // select the nested object containing the correct mapping
                  if (Object.keys(dict).indexOf(base) >= 0){ //checks that this is a result_type_name pivot
                      q = q + "MAX(CASE WHEN result_type_name='" + rtn + "' and result_value_type_name = '" + val + "' THEN " + dict[base] + " ELSE NULL END) AS " + [frtn, fval, base, sourceView].join('_') + ", ";
                  }
                  else{ // variant_type_name  pivot
                      q = q + "MAX(CASE WHEN variant_type_name='" + rtn + "' THEN variant_value ELSE NULL END) AS " + [frtn, sourceView].join('_') + ", ";
                  }
              }
              else {
                  alert("ERROR: unsupported pivot"); // if config tables are right should never hit this error
              }
           }
           q = q.substring(0,q.length-2) + ' '; //remove trailing comma and space
        } //end if pivoting

        filtersList = []; // will hold a list of each filter parsed by readFilters
        var numOR = 0; // number of OR filters
        readFilters(view); // see helper function above

        //add the filter based on the initially identified parent_id list
        if (selected_category === "Correlate Data" && view !== "MetID" && view.indexOf("InvitroPharm") !==0){
            if (!isTemp){ /// this is not the temp table at beginning of unified query
                if(filtersList.length > 0 ){ // there are already filters populated above
                    filtersList.push(" AND parent_id in (SELECT UPPER(parent_id) FROM temp) ");
                }
                else {// the only filter on this view is the compound list
                    filtersList.push(" parent_id in (SELECT UPPER(parent_id) FROM temp) ");
                }
            }
        }
        //add the filter based on the initially identified parent_id list
        if (selected_category === "Correlate Data" && view === "MetID" && view.indexOf("InvitroPharm") !==0){
            if (!isTemp){ /// this is not the temp table at beginning of unified query
                if(filtersList.length > 0 ){ // there are already filters populated above
                    filtersList.push(" AND parent_id_MetID in (SELECT UPPER(parent_id) FROM temp) ");
                }
                else {// the only filter on this view is the compound list
                    filtersList.push(" parent_id_MetID in (SELECT UPPER(parent_id) FROM temp) ");
                }
            }
        }
        if (selected_category === "Correlate Data" && view !== "MetID" && view.indexOf("InvitroPharm") ===0){
            if (!isTemp){ /// this is not the temp table at beginning of unified query
                if(filtersList.length > 0 ){ // there are already filters populated above
                    filtersList.push(" AND SUBSTR(compound_id,1,11) in (SELECT UPPER(parent_id) FROM temp) ");
                }
                else {// the only filter on this view is the compound list
                    filtersList.push(" SUBSTR(compound_id,11) in (SELECT UPPER(parent_id) FROM temp) ");
                }
            }
        }
        var non_pivoted_filters = false; // a boolean indicating the presence of filters not on pivoted fields

        for (var i in filtersList){ // checks each filter and updates non_pivoted_filters accordingly
            var lf = filtersList[i].trim().split(" ");
            var fi = 0; // index of field name
            while ("ANDOR() ".indexOf(lf[fi]) === 0){
                fi++;
            }
            var filter_field = lf[fi];

            if(selected_category === "Correlate Data" && view.indexOf("InvitroPharm") === 0  && filter_field.indexOf("SUBSTR(") === 0){ // handles the correlate data InvitroPharm filter on parent_id from temp
                filter_field = "compound_id";
            }

            var findf = tempFieldsObjList.find(obj => obj.fieldName === filter_field);
            if (findf){
                if(!tempFieldsObjList.find(obj => obj.fieldName === filter_field).isPivoted){
                        non_pivoted_filters = true;
                }
            }
            else if(col.indexOf(filter_field) > -1){
                non_pivoted_filters = true;
            }
            else if(selected_category === "Correlate Data" && (filter_field === "parent_id"|| filter_field === "compound_id")){ // for correlate data perfomance
                non_pivoted_filters = true;
            }

        }

            q = q + " FROM " + view_dict[view] + " WHERE (";
        if (non_pivoted_filters){
            // open () for user entered filters
            q = q + "(";

            //open first group for user entered () or ()
            q = q + "(";
            // console.log("writing where clause based on filtersList = ", filtersList);
            //add the filter based on the initially identified compound list
          var inN = 0; // number of inner filters (non-pivoted occur on raw view)
          for (var n = 0; n < filtersList.length; n++){
            var isPvt = false;
            var f = filtersList[n].trim();
            var lf = f.split(" ");
            var fi = 0; //index of fieldname
            while ("ANDOR() ".indexOf(lf[fi]) > -1){ // 0th  if first filter
                fi++;
            };

            filter_field = lf[fi];

            if(selected_category === "Correlate Data" && view.indexOf("InvitroPharm") === 0  && filter_field.indexOf("SUBSTR(") === 0){ // handles the correlate data InvitroPharm filter on parent_id from temp
                filter_field = "compound_id";
            }

            for (var j = 0; j < mypCol.length; j++){ // is this a pivoted field??
                if (filter_field === mypCol[j].toString().replace(/[-|/\.]/g,"_").toString().replace(/[%]/g,"percent")){
                    isPvt = true;
                    console.log("pivoted field!")
                }
            }

            if (isPvt){
            }
            else { // unpivoted column
                var sf = "";
                if (view === "MetID"){
                    sf = filter_field;
                }
                else{
                    sf = getSourceColumn(filter_field);
                }
                console.log("filter_field: ",filter_field);
                if (selected_category === "Correlate Data" && (filtersList[0].indexOf("parent_id in (SELECT UPPER(parent_id) FROM temp)") > -1 || filtersList[0].indexOf("parent_id in (SELECT parent_id FROM temp)") > -1) ){
                  // just filtering based on the compound_id list from temp
                }
                else {
                  try{
                      var fieldObj = tempFieldsObjList.find(o => o.fieldName === filter_field); // used for getting the data type of the selected field
                      var type = fieldObj.fieldDataType;
                      console.log("type: ",type);
                      if (type === "STRING"){ // if string needs quotes (note timestamp will already have thrown and error is handled by the catch )
                          sf = "UPPER("+getSourceColumn(filter_field)+")";
                      }

                      var fi = 0;
                      while ("ANDOR() ".indexOf(lf[fi]) > -1){ // 0th  if first filter
                          fi++;
                      };



                      lf[fi] = sf; // replace with source column cuz raw view filter


                      if (inN === 0){
                          if (lf[0] === "AND" || lf[0] === "OR"){
                              lf.reverse();
                              lf.pop(); // remove the AND/OR
                              lf.reverse();
                          }
                      }
                  }

                catch (e){
                  console.log("filtering on temp list", lf.join(" "));
                }
            }
                var myf  = lf.join(" ")
                q = q + " " + myf + " " ;
                inN++;
            }
          }
            //close group for () OR ()
            q = q + ")";
        }

        if (mypCol.length > 0){
            // add filters on raw view to remove any records where
            // none of the selected pivoted columns are represented in result_type_name result_value_type_name and variant_type_name
            // this addresses issue #22

            var pivotFilters = []; // we build a list of these filters
            if (q[q.length-1] === ")"){
                q =  q + " AND (";
            }
            else{
                q = q + "(";
            }
            for (var i = 0; i < mypCol.length; i++){
                    var pivot = mypCol[i];
                    var pivotList = pivot.toString().split("_"); //deconstruct the pivoted column
                    pivotList.reverse();
                    // console.log("reversed: ", pivotList);
                    var rtn = "";
                    while (rtn === ""){//avoid leading underscore
                      rtn = pivotList.pop();
                    }
                    rtn = rtn.toString().replace(/\|\|/g, " ");
                    rtn = rtn.toString().replace(/[|]/g, "_");
                    var val = pivotList.pop(); // result_value_type_name
                    val = ((val === "") ? "VALUE" : val);
                    val = val.toString().replace(/[|]/g,"_")
                    pivotList.reverse(); // get it facing the right way aggain
                    var sourceView = pivotList.pop(); // remove view origin suffix
                    base = pivotList.join("_");

                    var frtn = (('0123456789'.indexOf(rtn[0]) >=0) ? "_" + rtn : rtn).toString().replace(/[\s-|/\.]/g,"_").toString().replace(/[%]/g,"percent");
                    var fval = ((val === "VALUE")? "" : val).toString().replace(/[-/]/g,"_").toString().replace(/[%]/g,"percent")

                    if (val === "DLCsum" || val === "DLCevent"){
                        sourceView = val;
                    }
                    if (sourceView === "DLCsum"){ // dlc pivoting
                        var dict = base_dict[sourceView]; // select the nested object containing the correct mapping
                        if (Object.keys(dict).indexOf(base) >= 0){ //checks that this is a result_type_name pivot
                            pivotFilters.push(" (result_type_name='" + rtn + "' and result_value_type_name = '" + val + "') ");
                        }
                        else{ // variant_type_name  pivot
                            pivotFilters.push(" (variant_type_name='" + rtn + "') ");
                        }
                    }
                    else if (sourceView === "DLCevent"){
                        var lb = base.split("_");
                        console.log("lb",lb)
                        base = lb[lb.length-1]
                        console.log("base",base);
                        if (base === "UNIT"){
                            base = "VALUE_UNIT";
                        }
                        else if (base === "VALUE"){
                            base = "VALUE";
                        }
                        else if (base === "QUAL"){
                            base = "VALUE_QUAL";
                        }

                        var dict = base_dict[sourceView]; // select the nested object containing the correct mapping
                        if (Object.keys(dict).indexOf(base) >= 0){ //checks that this is a result_type_name pivot
                            pivotFilters.push(" (result_type_name='" + rtn + "' and result_value_type_name = '" + val + "') ");
                        }
                        else{ // variant_type_name  pivot
                            pivotFilters.push(" (variant_type_name='" + rtn + "') ");
                        }
                }
            }
            // only return records that correspond to at least one of the selected pivots
            q = q + pivotFilters.filter( onlyUnique ).join(" OR ");

            //close pivoting filters
            q = q + ")";
        }
        if (selected_category !== "Correlate Data"){ // this should remove inherited filters from correlate data
            if (mySelected_Programs.length > 0){
                var nidx = mySelected_Programs.indexOf(null);

                //open inherited program filters
                if (q[q.length-1] == ")"){
                    q =  q + " AND (";
                }
                else{
                    q = q + "(";
                }

                //identify "programs" field for this view
                var proField = pro_dict[view];
                if (nidx > -1){
                    mySelected_Programs.splice(nidx,1);
                    q = q + "(" + proField + " in (\'" + mySelected_Programs.join("\',\'") + "\') ";
                    q = q +  " OR " + proField + " is null"
                    q = q + ")) "; //close inherited program filters
                }
                else {
                    q = q + proField + " in (\'" + mySelected_Programs.join("\',\'") + "\') ";
                    q = q + ") "; //close inherited program filters
                }
            }

            if (myselected_assays.length > 0){
                q = q + "AND ( "; //open inherited  assay filters
                //identify "Assays" field for this view
                var nidx = myselected_assays.indexOf(null);
                // nidx = mySelected_Programs.indexOf("(null)_",view); // once kim updates the config tables
                var assayField = assay_dict[view];
                if (nidx > -1){
                    myselected_assays.splice(nidx,1);
                    q = q + "("+ assayField + " in (\'" + myselected_assays.join("\',\'") + "\') ";
                    q = q + " OR " + assayField + " is null)";
                    q = q + ") "; //close inherited assay filters
                }
                else{
                    q = q + assayField + " in (\'" + myselected_assays.join("\',\'") + "\') ";
                    q = q + ") "; //close inherited assay filters
                }
            }
        }


        q = q + ")"; // close the where
        if(non_pivoted_filters){
            q = q + ")";
        }



          if (mypCol.length > 0){ // add a group by if any pivoted columns
                q = q + " GROUP BY ";
                if (selected_category === "Correlate Data"){ // add joiners to sub query for not for display
                    if (view === "MetID"){
                        q = q + "parent_id"
                    }
                    else{
                        q = q + joiners_dict[view].join(", ");
                    }
                    if (myCol.length > 0){
                        q= q + ", ";
                    }
                }

                else if (view === "DLCsum" || view === "DLCevent"){
                    q = q + joiners_dict[view].join(", ");
                    q = q + ", "
                }

                q = q + myCol.join(", ") + ") "; // ends raw view query
          }
          else{
              q = q + ")"
          }

          var outer_where = true; // this will handle pivoted filters which are not done on the raw view


          var anyPvt = false;
          for (var n = 0; n < filtersList.length; n++ ){ // loop over the field names in the filters
            var f = filtersList[n].trim();
            var lf = f.split(" ");
            // console.log("lf", lf);
            var fi = 0; //index of fieldname
            while ("ANDOR() ".indexOf(lf[fi]) > -1 ){ // 0th  if first filter
                fi++;
            };

             var filter_field = lf[fi];
             if (selected_category === "Correlate Data" && (filtersList[0].indexOf("compound_id in (SELECT ") > -1 || filtersList[0].indexOf("parent_id in (SELECT ") > -1 ) ){
               filtersList[n] = lf.join(" ")
             }
             else {
               try{
               var fieldObj = tempFieldsObjList.find(o => o.fieldName === filter_field); // used for getting the data type of the selected field
               var type = fieldObj.fieldDataType;
               if (type === "STRING"){ // if string needs quotes (note timestamp will already have thrown and error is handled by the catch )
                   lf[fi] = "UPPER("+filter_field+")";
               }
               filtersList[n] = lf.join(" ")
              }
              catch (e){
                console.log("filtering on compound_id list", lf.join(" "));
              }
            }
            anyPvt = false;
            for (var j = 0; j < mypCol.length; j++){ // is this a pivoted field??
                if (filter_field === mypCol[j].toString().replace(/[-|/\.]/g,"_").toString().replace(/[%]/g,"percent")){

                    anyPvt = true;
                }
            }
          }
          outer_where = anyPvt

          if (filtersList.length === 0 && filtersObj["NOT"].length === 0){ // ??? why is this here ???
            outer_where = false;
          }

          if (outer_where){
              q = q + ' WHERE (';
              if (filtersObj["NOT"].length > 0 ){ //handles removing nulls when sorting
                  q = q +  '(' + filtersObj["NOT"].join(',') + ') AND ';
              }
              q = q + filtersList.join(" ")
                  q = q + ")";
          }
          if (selected_category != "Correlate Data" && (view === "DLCsum" || view === "DLCevent")){
                  q = q + ")";
          }
          if (sortOn != ''){
            q = q + " ORDER BY " + sortOn + " " + orderBy;
          }

          // throw("intentional error"); // for testing error handling
          return(q);
        // }//end not corelate data else
    }
    catch (e){ // there was a javascript error within this function
        alert("Internal Query Builder Error: " + e + "\n Please take a screen shot of your inputs and contact support team");
        throw(e);
    }
} // end writeQuery


function sortCol(event){ // this is a function to support sorting on a field when the column header is pressed
    $(this).toggleClass('sorted');
    $(this).text('Sorting...');
    resetPagination();
    var eCol = event.data.col // which column did they click on
    console.log(eCol) ;
    sortOn = eCol;
    filtersObj["NOT"] = [];
    filtersObj["NOT"].push(sortOn +' IS NOT null');
    // bqView = selected_datamartView.toString();
    // var tempTable;
    // var getTempTable = gapi.client.bigquery.jobs.get({
    //   'projectId': project_id,
    //   'jobId': jobId
    // });


    // getTempTable.execute(function(json, raw) { // this uses the temporary table created in BQ when the original query was run
    //   // console.log("after getting temp table");
    //   // console.log(json);
    //   var proj = json.configuration.query.destinationTable.projectId;
    //   var dataset  = json.configuration.query.destinationTable.datasetId;
    //   var tableId = json.configuration.query.destinationTable.tableId;
    //   tempTable = [proj, dataset, tableId].join(".");
    //   q = "SELECT * FROM `" + tempTable +"` ORDER BY " + sortOn + " " + orderBy + " ";
    //   console.log("Q: ", q);
    //   getResultsTable(q);
    // });

    console.log('ORDER BY IS ORIGINALLY: ', orderBy);
    //switch ascending or descending search
    if (orderBy === 'ASC'){
        orderBy = 'DESC';
    } else {
        orderBy = 'ASC';
    }

    let body = {
      userName: cognitoUser,
      sortColumn: sortOn,
      order: orderBy
    }
    let stmntStr = JSON.stringify(body);

    userTableQuery(stmntStr, false);

    if (pageTokens[0] == undefined){
        hideNextButton();
    }
}


function getResultsTable(q, isSummary){ // this function gets triggered when the user presses search
  resetPagination();
    var isFirst = true;

  $("#download_button").hide();
  $("#save_sql_button").hide();
  hideResultCount();
  hidePrevButton();
  hideNextButton();
  hidePageNum();
  hideResultsTable();
  showResultsLoadingSpinner();
  queryForCSVdownload = q;

  try {
    console.log("START QUERY HERE");
    console.log("USER IS: ", cognitoUser);
    let userId = localStorage.getItem("lookup");

    let body = {
      lookupId: userId,
      organization: org,
      email: emailId
    }

    let stmntStr = JSON.stringify(body);
    $.ajax({
        type: 'POST',
        url: 'https://y579rh6v24.execute-api.us-east-2.amazonaws.com/dev',
        data: stmntStr,
        crossDomain: true,
        contentType: 'application/json',
        beforeSend: function (xhr) {
           xhr.setRequestHeader('Authorization', authorizer);
        },
        success: function(data, status) {
          if(data.statusCode === "200") {
            console.log("USER IS STILL LOGGED IN");
            let body = {
              userName: cognitoUser,
              statement: q,
              organization: org,
              email: emailId
            }
            let stmntStr = JSON.stringify(body);
            //CHECK IF PID EXISTS
            $.ajax({
                type: 'POST',
                url: 'https://4wkqsc3q5i.execute-api.us-east-2.amazonaws.com/dev',
                data: stmntStr,
                crossDomain: true,
                contentType: 'application/json',
                beforeSend: function (xhr) {
                   xhr.setRequestHeader('Authorization', authorizer);
                },
                success: function(data, status) {
                  if(data != null && data != '') {
                    // console.log("PID FOUND, CANCELLING EXISTING QUERY");
                    cancelPid(data);
                  }
                  dropTable(stmntStr, isSummary);
                }, error: function(err) {
                  console.log(err);
                }
            });
          } else {
              window.location.href = "/";
          }
        }, error: function(err) {
          if(err.status === 0) {
            window.location.href = "/";
          }
        }
      });

  } catch (e) {
        var err = 'Query Error: ' + e;
        alert(err);
        hideResultsLoadingSpinner();
        document.getElementById('disablingDiv').style.display='none';
        enableButtons();
        $('#download_button').hide();
        $("#submit_button").text('Search');
  }
}

function cancelPid(pidData) {
  let body = pidData;
  let stmntStr = JSON.stringify(body);
  $.ajax({
      type: 'POST',
      url: 'https://59qfvibt3f.execute-api.us-east-2.amazonaws.com/dev',
      data: stmntStr,
      crossDomain: true,
      contentType: 'application/json',
      beforeSend: function (xhr) {
         xhr.setRequestHeader('Authorization', authorizer);
      },
      success: function(data, status) {
        console.log("PID CANCELLED");
      }, error: function(err) {
        console.log(err);
      }
  });
}

function dropTable(stmntStr, isSummary) {
  console.log("DROPPING TABLE");
  $.ajax({
      type: 'POST',
      url: 'https://ht3jtj4qr8.execute-api.us-east-2.amazonaws.com/dev',
      data: stmntStr,
      crossDomain: true,
      contentType: 'application/json',
      beforeSend: function (xhr) {
         xhr.setRequestHeader('Authorization', authorizer);
      },
      success: function(data, status) {
        pollTable(stmntStr, isSummary);
        createTable(stmntStr);
      }, error: function(err) {
        console.log(err);
      }
  });
}

function createTable(stmntStr) {
  console.log("CREATING TABLE");
  $.ajax({
      type: 'POST',
      url: 'https://4zax7ig737.execute-api.us-east-2.amazonaws.com/dev',
      data: stmntStr,
      crossDomain: true,
      contentType: 'application/json',
      beforeSend: function (xhr) {
         xhr.setRequestHeader('Authorization', authorizer);
      },
      success: function(data, status) {
        if(data.length > 0) {
          console.log('TABLE CREATED SUCCESSFULLY', data);
        } else if(data.errorMessage && data.errorMessage.includes('cancelled')) {
          console.log("CANCELLED SEARCH");
        } else if(data.errorMessage) {
          alert("Internal Query Builder Error: " + data.errorMessage + "\n Please take a screen shot of your inputs and contact support team");
          cancelSearch();
        }
      }, error: function(err) {
        if(err.readyState === 0) {
          console.log("API TIMEOUT");
        } else {
          console.log(err);
        }
      }
  });
}

function pollTable(stmntStr, isSummary) {
  console.log("POLLING TABLE");
  interval = setInterval(() => {
    $.ajax({
        type: 'POST',
        url: 'https://290wk2u3j4.execute-api.us-east-2.amazonaws.com/dev',
        data: stmntStr,
        crossDomain: true,
        contentType: 'application/json',
        beforeSend: function (xhr) {
           xhr.setRequestHeader('Authorization', authorizer);
        },
        success: function(data, status) {
          console.log("POLLING DATA IS: ", data);
          if(data == null) {

          } else if(data.statusCode === 200) {
            clearInterval(interval);
            userTableQuery(stmntStr, isSummary);
          }
        }, error: function(err) {
          console.log(err);
        }
    });
  }, 5000);
}

function userTableQuery(stmntStr, isSummary) {
  resetPagination();
  console.log("RUNNING QUERY AGAINST USER TABLE");
  $.ajax({
      type: 'POST',
      url: 'https://0jcxn43xw4.execute-api.us-east-2.amazonaws.com/dev',
      data: stmntStr,
      crossDomain: true,
      contentType: 'application/json',
      beforeSend: function (xhr) {
         xhr.setRequestHeader('Authorization', authorizer);
      },
      success: function(data, status) {
        if(isSummary) {
          console.log("SUMMARY BUTTON CLICKED");
          createCountQuery(data);
        } else {
          paginationCounter(stmntStr, data);
        }
      }, error: function(err) {
        console.log(err);
      }
  });
}

function paginationCounter(stmntStr, data) {
  $.ajax({
      type: 'POST',
      url: 'https://4cpqazfpyb.execute-api.us-east-2.amazonaws.com/dev',
      data: stmntStr,
      crossDomain: true,
      contentType: 'application/json',
      beforeSend: function (xhr) {
         xhr.setRequestHeader('Authorization', authorizer);
      },
      success: function(results, status) {
        let cnt = JSON.stringify(results);
        let parsed = JSON.parse(cnt);
        let paginationCount = parsed[0].count;
        console.log("RECORDS FOUND: ", paginationCount);
        hideResultsLoadingSpinner();
        document.getElementById('disablingDiv').style.display='none';
        enableButtons();
        $('#download_button').hide();
        $("#submit_button").text('Search');
        processQueryResults(paginationCount, data);
      }, error: function(err) {
        console.log(err);
      }
  });
}

function nextPage(){ // this function gets the next page of results when the user hits next

    hideResultsTable(); // hide table and show spinner until the next page comes back
    showResultsLoadingSpinner();


    $('#pageNum').empty();
    $('#prev_button').after('<label id="pageNum">'+ '&nbsp;&nbsp; Page ' + ++pageNum + ' of ' + totalPageCount + '</label>');

    if (pageNum > 1) showPrevButton();
    if (pageNum == totalPageCount) hideNextButton();

    let bd = "";
    console.log("SORT ON VALUE: ", sortOn);
    if(sortOn) {
      bd = {
        userName: cognitoUser,
        offset: pageNum,
        sortColumn: sortOn,
        order: orderBy,
        organization: org,
        email: emailId
      }
    } else {
      bd = {
        userName: cognitoUser,
        offset: pageNum,
        organization: org,
        email: emailId
      }
    }

    let userId = localStorage.getItem("lookup");

    let body = {
      lookupId: userId,
      organization: org,
      email: emailId
    }

    let stmntStr = JSON.stringify(body);
    $.ajax({
        type: 'POST',
        url: 'https://y579rh6v24.execute-api.us-east-2.amazonaws.com/dev',
        data: stmntStr,
        crossDomain: true,
        contentType: 'application/json',
        beforeSend: function (xhr) {
           xhr.setRequestHeader('Authorization', authorizer);
        },
        success: function(data, status) {
          if(data.statusCode === "200") {
            console.log("USER IS STILL LOGGED IN");
            let stmntStr = JSON.stringify(bd);

            $.ajax({
                type: 'POST',
                url: 'https://1aslwwv0cc.execute-api.us-east-2.amazonaws.com/dev',
                data: stmntStr,
                crossDomain: true,
                contentType: 'application/json',
                beforeSend: function (xhr) {
                   xhr.setRequestHeader('Authorization', authorizer);
                },
                success: function(data, status) {
                  var qryresults = $("#query_results").empty();
                  var tableContainer = $('<div id="container" class="table-container">').appendTo(qryresults);
                  var table = $("<table>", {'id':'results_table'}).appendTo(tableContainer);
                  var thead = $("<thead>").appendTo(table)
                  var header = $("<tr>").appendTo(thead);
                  var tbody = $("<tbody>").appendTo(table);
                  pageidx = pageidx + 1;
                  $.each(data, function(i, columnRow) {
                    if(i < 1) {
                      $.each(columnRow, function(field, val) {
                        $("<th>").text(field).on('click', {col: field}, sortCol).appendTo(header);
                      });
                    }
                    // if(field.type=="TIMESTAMP"){ //retrieved all field index that has type as timestamp
                    //   timestampIndexArr.push(i);
                    // }
                  });
                  // console.log("starting to parse results table for json.rows: ", json.rows);
                  $.each(data, function(i, rowData) {
                    var row = $("<tr>").appendTo(tbody);
                    $.each(rowData, function(i, field) {
                        //if row data index is timestamp, convert it to a readable date before displaying
                        // if($.inArray(i,timestampIndexArr) != -1){
                        //   var dateFormatter = moment.utc(field.v*1000); //UTC format
                        //   var toUTC = dateFormatter.format("YYYY-MM-DD hh:mm:ss");
                        //    $("<td>").text(toUTC).appendTo(row);
                        // }else{
                          $("<td>").text(field).appendTo(row);
                        // }
                    });
                    hideResultsLoadingSpinner();
                    showResultsTable();
                    $("table").stickyTableHeaders({container: "#container"});
                  });

                }, error: function(err) {
                  console.log(err);
                }
            });
        } else {
            window.location.href = "/";
        }
      }, error: function(err) {
        if(err.status === 0) {
          window.location.href = "/";
        }
      }
    });
}

function prevPage(){ // goes back a page in the results if the user hits previous

    hideResultsTable();
    showResultsLoadingSpinner();

    if (pageidx > 0){
        pageidx = pageidx - 1;
    }
    else{
        pageidx = 0;
    }

    $('#pageNum').empty();
    $('#prev_button').after('<label id="pageNum">'+ '&nbsp;&nbsp; Page ' + --pageNum + ' of ' + totalPageCount + '</label>');


    if (pageNum == 1) hidePrevButton();
        // var request = gapi.client.bigquery.jobs.getQueryResults({
        //    'projectId': project_id,
        //    'jobId': jobId,
        //    'maxResults': pageSize,
        // });

    // else{
    //     var request = gapi.client.bigquery.jobs.getQueryResults({
    //        'projectId': project_id,
    //        'jobId': jobId,
    //        'maxResults': pageSize,
    //        'pageToken': pageTokens[pageidx-1]
    //     });
    // }
    // Return the previous page of data in Table

    let bd = "";
    if(sortOn) {
      bd = {
        userName: cognitoUser,
        offset: pageNum,
        sortColumn: sortOn,
        order: orderBy,
        organization: org,
        email: emailId
      }
    } else {
      bd = {
        userName: cognitoUser,
        offset: pageNum,
        organization: org,
        email: emailId
      }
    }

    let userId = localStorage.getItem("lookup");

    let body = {
      lookupId: userId,
      organization: org,
      email: emailId
    }

    let stmntStr = JSON.stringify(body);
    $.ajax({
        type: 'POST',
        url: 'https://y579rh6v24.execute-api.us-east-2.amazonaws.com/dev',
        data: stmntStr,
        crossDomain: true,
        contentType: 'application/json',
        beforeSend: function (xhr) {
           xhr.setRequestHeader('Authorization', authorizer);
        },
        success: function(data, status) {
          if(data.statusCode === "200") {
            console.log("USER IS STILL LOGGED IN");

            let stmntStr = JSON.stringify(bd);

            $.ajax({
                type: 'POST',
                url: 'https://1aslwwv0cc.execute-api.us-east-2.amazonaws.com/dev',
                data: stmntStr,
                crossDomain: true,
                contentType: 'application/json',
                beforeSend: function (xhr) {
                   xhr.setRequestHeader('Authorization', authorizer);
                },
                success: function(data, status) {

                  var qryresults = $("#query_results").empty();
                  var tableContainer = $('<div id="container" class="table-container">').appendTo(qryresults);
                  var table = $("<table>", {'id':'results_table'}).appendTo(tableContainer);
                  var thead = $("<thead>").appendTo(table)
                  var header = $("<tr>").appendTo(thead);
                  var tbody = $("<tbody>").appendTo(table);
                  $('#filter_results').html('');
                  // need to set these globals for pagination
                  showNextButton();

                  $.each(data, function(i, columnRow) {
                    if(i < 1) {
                      $.each(columnRow, function(field, val) {
                        $("<th>").text(field).on('click', {col: field}, sortCol).appendTo(header);
                      });
                    }
                    // if(field.type=="TIMESTAMP"){ //retrieved all field index that has type as timestamp
                    //   timestampIndexArr.push(i);
                    // }
                  });
                  // console.log("starting to parse results table for json.rows: ", json.rows);
                  $.each(data, function(i, rowData) {
                    var row = $("<tr>").appendTo(tbody);
                    $.each(rowData, function(i, field) {
                        //if row data index is timestamp, convert it to a readable date before displaying
                        // if($.inArray(i,timestampIndexArr) != -1){
                        //   var dateFormatter = moment.utc(field.v*1000); //UTC format
                        //   var toUTC = dateFormatter.format("YYYY-MM-DD hh:mm:ss");
                        //    $("<td>").text(toUTC).appendTo(row);
                        // }else{
                          $("<td>").text(field).appendTo(row);
                        // }
                    });
                    hideResultsLoadingSpinner();
                    showResultsTable();
                    $("table").stickyTableHeaders({container: "#container"});
                  });

            }, error: function(err) {
              console.log(err);
            }
          });
        } else {
            window.location.href = "/";
        }
      }, error: function(err) {
        if(err.status === 0) {
          window.location.href = "/";
        }
      }
    });
}

function cancelSearch() {

   hideResultsLoadingSpinner();
   document.getElementById('disablingDiv').style.display='none';
   if(isDownload) {
       $('#searchModal').modal('hide');
   } else {

     let body = {
       userName: cognitoUser,
       organization: org,
       email: emailId
     }
     let stmntStr = JSON.stringify(body);
     $.ajax({
         type: 'POST',
         url: 'https://4wkqsc3q5i.execute-api.us-east-2.amazonaws.com/dev',
         data: stmntStr,
         crossDomain: true,
         contentType: 'application/json',
         beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', authorizer);
         },
         success: function(data, status) {
           if(data != null && data != '') {
             console.log("PID FOUND, CANCELLING EXISTING QUERY");
             cancelPid(data);
           }
           setTimeout(function() {
             console.log("ATTEMPTING ANOTHER PID CANCELLATION");
             $.ajax({
                 type: 'POST',
                 url: 'https://4wkqsc3q5i.execute-api.us-east-2.amazonaws.com/dev',
                 data: stmntStr,
                 crossDomain: true,
                 contentType: 'application/json',
                 beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', authorizer);
                 },
                 success: function(data, status) {
                   if(data != null && data != '') {
                     console.log("PID FOUND, CANCELLING EXISTING QUERY");
                     cancelPid(data);
                   }
                   clearInterval(interval);
                 }, error: function(err) {
                   console.log(err);
                 }
             });
           }, 2000);
           clearInterval(interval);
           enableButtons();
           $("#submit_button").text("Search");
           $("#summary_button").text("View Summary");
           $("#cancel_button").hide();
           $("#download_button").hide();
           $("#save_sql_button").hide();
           $('.hr2').hide();
           $('.searchresheader').hide();
         }, error: function(err) {
           console.log(err);
         }
     });
   }
}

function getSummaryTable(){ // this funciton is triggered when the user hits View Summary
    isSummary = true;
    hideSummaryTable();
    showResultsLoadingSpinner();

    document.getElementById('disablingDiv').style.display='block';
    c = wrapper.find(':selected').map(function(){
        return $(this).text();
    });
    pref = jQuery.extend(true,[],c);

      is_summ = true;
      if (selected_category === "Correlate Data"){
        bqView = selected_datamartView
      }
      else{
        bqView = selected_datamartView.toString();
      }
      if (selected_category === 'Correlate Data' && bqView.length > 1){// if correlate data call the custom wrapper for writeQuery
        q = writeUnifiedQuery(bqView);
      }
      else{
        q = writeQuery(bqView,false);
      }
      $('#queryTxtArea').val(q); // set the query in query builder textarea
      getResultsTable(q, isSummary);
}

function exportCSV() { // this function supports the download functionality by calling the ws appengine to do the heavy lifting
    console.log('export CSV triggered');

    showResultsLoadingSpinner();
    document.getElementById('disablingDiv').style.display='block';

    var d = new Date();
    var strDate = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate()+"-"+d.getHours()+""+d.getMinutes()+""+d.getMilliseconds();

    let files = selected_category + "_" + strDate;
    let fileName = files.split(' ').join('_');

    let userId = localStorage.getItem("lookup");

    let body = {
      lookupId: userId,
      organization: org,
      email: emailId
    }

    let stmntStr = JSON.stringify(body);
    $.ajax({
        type: 'POST',
        url: 'https://y579rh6v24.execute-api.us-east-2.amazonaws.com/dev',
        data: stmntStr,
        crossDomain: true,
        contentType: 'application/json',
        beforeSend: function (xhr) {
           xhr.setRequestHeader('Authorization', authorizer);
        },
        success: function(data, status) {
          if(data.statusCode === "200") {
            console.log("USER IS STILL LOGGED IN");

            let body = {
              userName: cognitoUser,
              file: fileName,
              organization: org,
              email: emailId,
              sortColumn: sortOn,
              order: orderBy
            }
            let stmntStr = JSON.stringify(body);
            $.ajax({
                type: 'POST',
                url: 'https://ed0jz3jovf.execute-api.us-east-2.amazonaws.com/dev',
                data: stmntStr,
                crossDomain: true,
                contentType: 'application/json',
                beforeSend: function (xhr) {
                   xhr.setRequestHeader('Authorization', authorizer);
                },
                success: function(data, status) {
                  if(data) {
                    console.log("GOT COLUMN HEADERS: ", data);
                    let body = data;
                    let stmntStr = JSON.stringify(body);
                    $.ajax({
                        type: 'POST',
                        url: 'https://91hargd5gf.execute-api.us-east-2.amazonaws.com/dev',
                        data: stmntStr,
                        crossDomain: true,
                        contentType: 'application/json',
                        beforeSend: function (xhr) {
                           xhr.setRequestHeader('Authorization', authorizer);
                        },
                        success: function(data, status) {
                          if(data.statusCode === "200") {
                            getSignedUrl(fileName);
                          }
                        }, error: function(err) {
                          console.log(err);
                        }
                    });
                  }
                }, error: function(err) {
                  console.log(err);
                }
            });
          } else {
              window.location.href = "/";
          }
        }, error: function(err) {
          if(err.status === 0) {
            window.location.href = "/";
          }
        }
      });
}

function getSignedUrl(fileName) {
  console.log("ATTEMPING TO GET S3 DOWNLOAD URL");
  let body = {
    userName: cognitoUser,
    file: fileName,
    organization: org,
    email: emailId
  }
  let stmntStr = JSON.stringify(body);
  $.ajax({
      type: 'POST',
      url: 'https://eipz8bark0.execute-api.us-east-2.amazonaws.com/dev',
      data: stmntStr,
      crossDomain: true,
      contentType: 'application/json',
      beforeSend: function (xhr) {
         xhr.setRequestHeader('Authorization', authorizer);
      },
      success: function(data, status) {
        console.log("S3 DOWNLOAD URL: ", data);
        $("#download_button").prop('disabled', false);
        $("#save_sql_button").prop('disabled', false);
        $("#download_button").text('Download');
        hideResultsLoadingSpinner();
        document.getElementById('disablingDiv').style.display='none';
        $(location).attr('href', data);
      }, error: function(err) {
        console.log(err);
      }
  });
}

function queryCategories() { // query to populate layer 1
    return "SELECT category, category_description FROM merck_dev_access_control.v_categories GROUP BY category_description, category";
}

function queryViews(category) {  // query to populate layer 2 (based on layer 1 selections
    return "SELECT view, view_description, display_view FROM merck_dev_access_control.v_categories WHERE category = '" + category + "' ORDER BY view ASC";
}

function querySubCategories(views, assays, programs) { //query to populate layer 5 based on selected programs/assays

    // these conditionals handle null programs or null assays
    console.log("PROGRAM ASSAYS: ", programs, assays);
    if(assays === "" && programs === ""){ // empty assay/program
        return "SELECT DISTINCT subcategory, assay FROM merck_dev_access_control.v_pro_assay_sub WHERE view IN ('') AND assay in ('') AND pro in ('') ORDER BY subcategory DESC";
    }
    else {
        if(assays.indexOf(null) > -1){
            if (programs.indexOf(null) > -1){ //both null
                console.log("both null");
                try{ //handles list of views
                    return "SELECT DISTINCT subcategory, assay FROM merck_dev_access_control.v_pro_assay_sub WHERE view IN ('" + views.join("', '") + "') AND assay is null AND pro is null ORDER BY subcategory DESC";
                }
                catch (e){ // handles single view
                    return "SELECT DISTINCT subcategory, assay FROM merck_dev_access_control.v_pro_assay_sub WHERE view IN ('" + [views].join("', '") + "') AND assay is null AND pro is null ORDER BY subcategory DESC";
                }
            }
            else{ // assay only null
                console.log("assay null");
                try{ // handles list of views
                    return "SELECT DISTINCT subcategory, assay FROM merck_dev_access_control.v_pro_assay_sub WHERE view IN ('" + views.join("', '") + "') AND assay is null AND pro IN (\'" + programs.join("\', \'") + "\') ORDER BY subcategory DESC";
                }
                catch (e){ //handles single view
                    return "SELECT DISTINCT subcategory, assay  FROM merck_dev_access_control.v_pro_assay_sub WHERE view IN ('" + [views].join("', '") + "') AND assay is null AND pro IN (\'" + programs.join("\', \'") + "\') ORDER BY subcategory DESC";
                }
            }
        }
        else{
            if (programs.indexOf(null) > -1){ //both null
                console.log("program null");
                try{ // handles list of views
                    return "SELECT DISTINCT subcategory, assay FROM merck_dev_access_control.v_pro_assay_sub WHERE view IN ('" + views.join("', '") + "') AND assay IN(\'"+assays.join("\', \'")+"\') AND pro is null ORDER BY subcategory DESC";
                }
                catch (e){ // handles single view
                    return "SELECT DISTINCT subcategory, assay FROM merck_dev_access_control.v_pro_assay_sub WHERE view IN ('" + [views].join("', '") + "') AND assay IN(\'"+assays.join("\', \'")+"\') AND pro is null ORDER BY subcategory DESC";
                }
            }
            else{ // neither null
                console.log("neither null");
                try{ // handles list of views
                    return "SELECT DISTINCT subcategory, assay FROM merck_dev_access_control.v_pro_assay_sub WHERE view IN ('" + views.join("', '") + "') AND assay IN(\'"+assays.join("\', \'")+"\') AND pro IN (\'" + programs.join("\', \'") + "\') ORDER BY subcategory DESC";
                }
                catch (e){ // handles single view
                    return "SELECT DISTINCT subcategory, assay FROM merck_dev_access_control.v_pro_assay_sub WHERE view IN ('" + [views].join("', '") + "') AND assay IN(\'"+assays.join("\', \'")+"\') AND pro IN (\'" + programs.join("\', \'") + "\') ORDER BY subcategory DESC";
                }
            }
        }
    }
}

function queryFields(sub_categories) { // query fields based on datatype selections in layer 5
    if (typeof sub_categories === "string"){
        sub_categories = [sub_categories];
    }
    return "SELECT DISTINCT field_name, is_pivoted, datatype, subcategory FROM merck_dev_access_control.v_sub_categories6 WHERE subcategory IN (\'" + sub_categories.join("\', \'") + "\') ORDER BY field_name DESC";
}

function queryPrograms(views) { // query for programs layer 3 based on view selections in layer 2
        return "SELECT DISTINCT pro FROM merck_dev_access_control.v_view_pro WHERE view IN (" + views + ") ORDER BY pro DESC";
}

function queryAssays(programs) { // query for assays based on program selections
    // will need to add some filtering by view
    var view = selected_datamartView;
    programs = programs.trim();
    console.log("PROGRAMS: ", programs);

    if(programs === "null"){ // single null program
        if (selected_category === "Correlate Data"){ //multiple views
            return "SELECT DISTINCT assay, pro FROM merck_dev_access_control.v_pro_assay_sub WHERE pro is '" + programs + "' AND view in ('" + view.join("', '") + "') ORDER BY assay DESC".replace(/\n/g, "");
        }
        else { // single view
            return "SELECT DISTINCT assay, pro FROM merck_dev_access_control.v_pro_assay_sub WHERE pro is '" + programs + "' AND view = '" + view + "' ORDER BY assay DESC".replace(/\n+/g,"");
        }
    }
    else{ // actual programs
        if (selected_category === "Correlate Data"){ // multiple views
            return "SELECT DISTINCT assay, pro FROM merck_dev_access_control.v_pro_assay_sub WHERE pro IN (" + programs + ") AND view in ('" + view.join("', '") + "') ORDER BY assay DESC".replace(/\n+/g,'');
        }
        else { // single view
            return "SELECT DISTINCT assay, pro FROM merck_dev_access_control.v_pro_assay_sub WHERE pro IN (" + programs + ") AND view = '" + view + "' ORDER BY assay DESC".replace(/\n+/g,'');
        }
    }
}

function composeQueryOptions(query) { // helper funciton for the above query constructors for populating the 6 layers
    // console.log(query);
    // reauth();
    // return gapi.client.bigquery.jobs.query({
    //     'projectId': project_id,
    //     'query': query,
    //     'useLegacySql': false,
    //     //'maxResults': 1//,
    //     'timeoutMs': timeout
    //
    // });
}

function populateCategories() {

    showCategoriesLoadingSpinner();
    var stmnt = queryCategories();
    // console.log("QUERY: ", stmnt);
    // we clear all the list containers
    selected_fields = [];
    tempFieldsObjList = []
    selected_datatypes = [];
    tempselectedProg = [];
    tempselectedassays = [];
    tempSelectedFields = [];
    pCol = [];
    col = [];

    let bd = {
      statement: stmnt,
      organization: org,
      email: emailId
    }
    let stmntStr = JSON.stringify(bd);
    $.ajax({
        type: 'POST',
        url: 'https://dl70eg51ke.execute-api.us-east-2.amazonaws.com/dev',
        data: stmntStr,
        crossDomain: true,
        contentType: 'application/json',
        beforeSend: function (xhr) {
           xhr.setRequestHeader('Authorization', authorizer);
        },
        success: function(data, status) {
          // console.log("Data: ", data);
          var catList = data;
          var categoriesDiv = $(".categories").empty();

          for (var i=0; i < catList.length; i++) {
              var cat = catList[i].category.trim(); // get the category names
              var catDescription = catList[i].category_description.trim(); // get category description
              categoriesDiv.append('<div class="form-check"> \
                                      <label class="form-check-label"> \
                                          <input type="radio" name="categoryRdoBtn" class="form-check-input category" \
                                              id="category" onchange="getSelectedCat()" value="' + cat + '"> \
                                              <span data-toggle="tooltip" title="<b>' + catDescription +'<b>">' + cat + '</span> \
                                      </label> \
                                    </div>');
          }
          // tooltip configurations to display the item description upon hover
          $('[data-toggle="tooltip"]').tooltip({
              'delay': 400,
              'placement' : 'right',
              'html': true
          });
          hideCategoriesLoadingSpinner();
      // });
    }, error: function(err) {
      console.log(err);
    }
  });
}

function populateDatamartsViews(category) { // populates layer 2 based on category seleciton in layer 1
    var DMViewsList = []; //list of views to be populated

    // we clear the temp lists
    selected_fields = [];
    tempselectedProg = [];
    tempselectedassays = [];
    tempFieldsObjList = [];
    tempSelectedFields = [];
    SelectedProgObjList = [];
    SelectedAssayObjList = [];
    SelectedDTObjList = [];
    //clear the ui elements concerning selections in layers 3-6
    $(".multiselect-native-select").empty();
    $("#dtSubCategories").empty();
    $("#selectedProgramsDiv").empty();
    $("#selectedassaysDiv").empty();
    $("#selectedDTDiv").empty();
    $("#selectedFieldDiv").empty();


    showViewsLoadingSpinner();
    // var request = composeQueryOptions(queryViews(category));
    // request.execute(function(json, raw) {

    let stmnt = queryViews(category);
    let bd = {
      statement: stmnt,
      organization: org,
      email: emailId
    }
    let stmntStr = JSON.stringify(bd);

    $.ajax({
        type: 'POST',
        url: 'https://dl70eg51ke.execute-api.us-east-2.amazonaws.com/dev',
        data: stmntStr,
        crossDomain: true,
        contentType: 'application/json',
        beforeSend: function (xhr) {
           xhr.setRequestHeader('Authorization', authorizer);
        },
        success: function(data, status) {
          // console.log("Data: ", data);

          DMViewsList = data;
          var datamartViewDiv = $(".datamarts-views").empty();
          viewsInputType = (category === 'Correlate Data') ? 'checkbox' : 'radio';
          for (var i=0; i < DMViewsList.length; i++) {
              var dmview = DMViewsList[i].view.trim();
              // var dmviewDescription = DMViewsList[i].f[1].v.trim();
              // var dmDisplayView = DMViewsList[i].f[2].v.trim();
              var dmviewDescription = DMViewsList[i].view_description.trim();
              var dmDisplayView = DMViewsList[i].display_view.trim();
              datamartViewDiv.append('<div class="form-check"> \
                                      <label class="form-check-label"> \
                                          <input type="' + viewsInputType +'" name="viewsRdoBtn" class="form-check-input datamart-view" \
                                              id="datamart-view" value="' + dmview + '"> \
                                              <span data-toggle="tooltip" title="<b>' + dmviewDescription +'<b>">' + dmDisplayView + '</span> \
                                      </label> \
                                    </div>');
          }
          // tooltip configurations to display the item description upon hover
          $('[data-toggle="tooltip"]').tooltip({
              'delay': 400,
              'placement' : 'right',
              'html': true
          });
          hideViewsLoadingSpinner();

          // if the view options are checkboxes. This is the logic that gets the selected views in layer 2
          // specifically if the selected category is 'Correlate Data'
          if (viewsInputType === 'checkbox') {
              var view_options = $('input.datamart-view');
              for (var i=0; i < view_options.length; i++) {
                  view_options[i].onclick = function() {
                      lastSelectedView = "'" + this.value + "'";
                      getSelectedDatamartView(this.value, this.checked);
                  }
              }
          }
          // if options are radio buttons
          else {
              $('input.datamart-view').change(function() {
                  lastSelectedView = "'" + this.value + "'";
                  getSelectedDatamartView(null, null); });
          }

          if (DMViewsList.length === 1){ // this handles PPDM, ChemProp and MetID autoselection of view
              $("input.datamart-view").prop("checked",true);
              $("input.datamart-view").change();
          }
    // });
    }, error: function(err) {
      console.log(err);
    }
  });
}

function populatePrograms(view){ // populates programs in layer 3 based on selection of view in layer 2
    showProgramsLoadingSpinner();
    // var request = composeQueryOptions(queryPrograms(view));

    if (selected_category !== 'Correlate Data') { // wipe everything if not correlate data
        // clears the containers
        selected_fields = [];
        selected_assays = [];
        SelectedAssayObjList = [];
        selected_datatypes = [];
        SelectedDTObjList = [];
        selected_Programs = [];
        SelectedProgObjList =  [];
        tempselectedProg = [];
        tempselectedassays = [];
        // tempFieldsObjList = [];
        tempSelectedFields = [];
    }

    // request.execute(function(json, raw) {

    let stmnt = queryPrograms(view);
    let bd = {
      statement: stmnt,
      organization: org,
      email: emailId
    }
    let stmntStr = JSON.stringify(bd);

    $.ajax({
        type: 'POST',
        url: 'https://dl70eg51ke.execute-api.us-east-2.amazonaws.com/dev',
        data: stmntStr,
        crossDomain: true,
        contentType: 'application/json',
        beforeSend: function (xhr) {
           xhr.setRequestHeader('Authorization', authorizer);
        },
        success: function(data, status) {
          // console.log("PROGRAMS LIST: ", data);

          ProgramsList = data;

          var ProgramsDiv = $(".programs").empty();
          var programSelect = $("<select>", {'id': 'Programs', 'multiple': 'multiple'}).appendTo(ProgramsDiv);
          var selectedProgramsDiv = $("<div>", {'id': 'selectedProgramsDiv'}).appendTo(ProgramsDiv);
          var pro = '';
          var proObj = {};
          if (ProgramsList.length > 0) {
              // store all the newly retrieved items from BQ to temp list
              for (var i=0; i < ProgramsList.length; i++) {
                  pro = ProgramsList[i].pro.trim();
                  proObj = { 'view': lastSelectedView, 'pro': pro.replace(/\n{1,2}/g," ")};
                  tempselectedProg.push(proObj);

              }
          } else {
              // scenario when user deselect a view in layer 2, we remove the corresponding programs in layer 3
              // we get only all of the previously selected subcategories that are not under the deselected view
              var pros2remove = [];
              tempselectedProg.forEach(obj => {
                  if(obj.view === lastSelectedView){
                      pros2remove.push(obj.pro);
                  }
              });
              tempselectedProg = tempselectedProg.filter(obj => obj.view !== lastSelectedView);
              selected_Programs.forEach(pro => {
                  if(pros2remove.indexOf(pro) > -1){
                      deSelectProgram(pro);
                  }
              });
              selected_Programs = selected_Programs.filter(pro => pros2remove.indexOf(pro) < 0);

              getSelectedPrograms(lastSelectedView.replace(/\'/g,""), false, selectedProgramsDiv, false, false); // function takes care of backend containers of selcted programs and populating selected programs checklist

          }

          // dynamically append to multiselect options
          for (var i=0; i < tempselectedProg.length; i++) {
              programSelect.prepend($("<option>", { 'value' : tempselectedProg[i].pro }).html(tempselectedProg[i].pro));

          }

          // initialize the bootstrap multiselect
          // $('#Programs').multiselect('destory'); // we have to do this for other layers no necessary here
          $('#Programs').multiselect(
              {
                  buttonWidth: '243px',
                  maxHeight: 400,
                  includeSelectAllOption: true,
                  enableFiltering: true,
                  enableCaseInsensitiveFiltering: true,
                  filterPlaceholder: 'Search for Sub-Category...',
                  // Event when user select a single item
                  onChange: function(element, checked) {
                      var sel_program = element.val(); //.trim();
                      // we set the timeouts before executing the getSelectedPrograms()
                      // to avoid the incorrect rendering of the bootstrap multi-select
                      $('.multiselect-container').find('input').attr("disabled", true);
                      setTimeout(function(){
                          $('.multiselect-container').find('input').removeAttr('disabled');
                      }, 1300);

                      getSelectedPrograms(sel_program, checked, selectedProgramsDiv, false, false);
                  },
                  // Event when the user click on the Select All Option
                  onSelectAll: function() {
                      showProgramsLoadingSpinner();
                      // var selectedItems = [];
                      var selectedItems = selected_Programs;
                      $('#Programs option:selected').each(function(index, item){
                          if (selectedItems.indexOf($(this).val()) < 0){
                              selectedItems.push($(this).val());
                          }
                      });
                      // console.log('selectedItems =======> ', selectedItems);
                      getSelectedPrograms(selectedItems, false, selectedProgramsDiv, true, false);
                      hideProgramsLoadingSpinner();
                  },
                  // Event when the user click on the De-Select All Option
                  onDeselectAll: function() {
                      getSelectedPrograms([], false, selectedProgramsDiv, false, true);
                  },
              }
          );
          //create tool tip to explain (null) option for programs
          if( tempselectedProg.filter(obj => obj.pro.indexOf("(null)") > -1).length > 0){
              $('.multiselect-container').tooltip({
                  title: 'Records with no Program can be accessed by choosing the (null) option for the desired view.',
                  trigger: 'hover'
              });
          }
          if(ProgramsList){
              if(ProgramsList.length === 1){ // handles MetID autoselection of program
                  if (SelectedProgObjList.filter(obj => obj.pro === pro).length < 1){
                      selectProgram(lastSelectedView,pro);
                      selected_Programs = SelectedProgObjList.map(obj => obj.pro);
                  }
              }
          }
          $('#Programs').multiselect('select', selected_Programs, true);
          // retain the previously selected Programs when selected Category is 'Correlate Data'
          if (selected_Programs.length > 0 && selected_category === "Correlate Data") {
              if(ProgramsList){
                  if (ProgramsList.length > 1){
                      for (var i=1; i < selected_Programs.length; i++) {
                          // append the selected items and display to layer 4
                          prependToSelectedprogramDiv(selectedProgramsDiv, selected_Programs[i]);
                      }
                  }
              }
          }

          hideProgramsLoadingSpinner();
        }, error: function(err) {
          console.log(err);
        }
      });
    // });
    $("#selectedassaysDiv").empty();
}

function populateAssays(quotedStrings,sel_program){
    showAssaysLoadingSpinner();

    if (quotedStrings !== "''"){
        let stmnt = queryAssays(quotedStrings);
        console.log("STATEMENT: ", stmnt);
        let bd = {
          statement: stmnt,
          organization: org,
          email: emailId
        }
        let stmntStr = JSON.stringify(bd);

        $.ajax({
            type: 'POST',
            url: 'https://dl70eg51ke.execute-api.us-east-2.amazonaws.com/dev',
            data: stmntStr,
            crossDomain: true,
            contentType: 'application/json',
            beforeSend: function (xhr) {
               xhr.setRequestHeader('Authorization', authorizer);
            },
            success: function(data, status) {
              // console.log("ASSAYS LIST: ", data);

            assaysList = data;
            // console.log('SUB-CATEGORIES ===> ', assaysList);
            if($(".assays").is(":empty") ){ // if .assays has been created yet make it empty then append the multiselct and list of selected fields as children
              var assaysDiv = $(".assays").empty();
              var assaySelect = $("<select>", {'id': 'assays', 'multiple': 'multiple'}).appendTo(assaysDiv);
              var selectedassaysDiv = $("<div>", {'id': 'selectedassaysDiv'}).appendTo(assaysDiv);
            }

            var assaysDiv = $(".assays");
            var assaySelect = $("#assays");
            var selectedassaysDiv = $("#selectedassaysDiv").empty();
            var assay = '';
            var pro = '';
            var assayObj = {};
            if (assaysList) { // results came back for the search (not a deselection)
                // store all the newly retrieved items from BQ to temp list
                for (var i=0; i < assaysList.length; i++) {
                    assay = assaysList[i].assay.trim().replace(/'/, "\\'"); //fix assay single quoted items
                    pro = assaysList[i].pro.trim();
                    assayObj = { 'pro':pro.replace(/\n{1,2}/g," "), 'assay': assay.replace(/\n{1,2}/g," ")}; // this handles a data quality issue with new lines
                    // if (tempselectedassays.filter(obj => obj.assay === assay).length  < 1){ // only add assay if not already there
                    if (tempselectedassays.filter(obj => obj.assay === assay).length  < 1){ // only add assay if not already there
                        tempselectedassays.push(assayObj);
                    }
                    //add this assay as a child of the appropriate selcted programs
                    SelectedProgObjList.forEach(obj => {
                        if (obj.pro === pro){
                            obj.child_assays.push(assay);
                        }
                    });
                    //add the selected program as a parent of the assay
                    SelectedAssayObjList.forEach(obj => {
                        if(obj.assay === assay){
                            obj.parent_pros.push(pro);
                        }
                    });
                    if(autoSelectAssays.indexOf(assay) > -1){ // handles autoselection of specified datatypes like Metadata_DLCsum
                        var my_parents = SelectedProgObjList.filter(obj => obj.child_assays.indexOf(assay) > -1).map(obj => obj.pro);
                        if (SelectedAssayObjList.filter(obj => obj.assay === assay).length < 1){
                            selectAssay(my_parents,assay);
                            selected_assays = SelectedAssayObjList.map(obj => obj.assay);
                            getSelectedAssays(selected_assays[selected_assays.length -1],true,selectedassaysDiv,false,false)
                            // $('#assays').multiselect('select', selected_assays[selected_assays.length -1], true);
                        }
                    }
                    else if(assaysList.length === 1){ // handles MetID auto selection of assay
                        if(SelectedAssayObjList.filter(obj => obj.assay === assay).length < 1){
                            var my_parents = SelectedProgObjList.filter(obj => obj.child_assays.indexOf(assay) > -1).map(obj => obj.pro);
                            selectAssay(my_parents,assay); // creates the assay object and adds it to SelectedAssayObjList
                            selected_assays = SelectedAssayObjList.map(obj => obj.assay); // maintains selected_assays for query builder
                            $('#assays').multiselect('select', selected_assays, true);
                            getSelectedAssays(selected_assays[selected_assays.length -1],true,selectedassaysDiv,false,false)
                            prependToSelectedAssayDiv(selectedassaysDiv, selected_assays[0]);
                        }
                }

                }

            }
            else {
                // scenario when user deselect a view in layer 2, we remove the corresponding subcategories in layer 3
                // we get only all of the previously selected subcategories that are not under the deselected view
                tempselectedassays = []
                SelectedProgObjList.forEach(obj => {
                    obj.child_assays.forEach(a =>{
                        tempselectedassays.push({pro: obj.pro, assay: a});
                    });
                });
                tempselectedassays = tempselectedassays.filter(onlyUnique);
                // we remove the datatype in the list of selected_assays and tempSelectedprogram
                // based on the de-selected datatype
                for (var x=0; x < selected_assays.length; x++) {
                    for (var i=0; i < tempselectedassays.length; i++) {
                        // if the datatype is present on both list, we go to the next item of the tempselectedProg
                        if (tempselectedassays[i].assay === selected_assays[x]) { break; }
                        else {
                            // we reach the end of the list so we know that it's not existing in the selected_assays
                            if (i == (tempselectedassays.length - 1)) {
                                lastDeSelectedAssay = selected_assays[x]; // we get the last deselected assay
                                selected_assays.splice(selected_assays.indexOf(lastDeSelectedAssay), 1); // we remove in the selected_assays
                                tempselectedassays.splice(tempSelectedprogram.indexOf(lastDeSelectedAssay), 1); // we remove in the tempSelectedprogram
                            }
                        }
                    }
                }
            }

            // dynamically append to multiselect options
            assaySelect.empty();
            for (var i=0; i < tempselectedassays.length; i++) {
                assaySelect.prepend($("<option>", { 'value' : tempselectedassays[i].assay }).html(tempselectedassays[i].assay));
            }

            // initialize the bootstrap multiselect
            console.log($('#assays'));
            $('#assays').multiselect('destroy');
            $('#assays').multiselect(
                {
                    buttonWidth: '243px',
                    maxHeight: 400,
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,
                    filterPlaceholder: 'Search for Sub-Category...',
                    // Event when user select a single item
                    onChange: function(element, checked) {
                        var sel_assay = element.val(); //trim();

                        // we set the timeouts before executing the getSelectedAssay()
                        // to avoid the incorrect rendering of the bootstrap multi-select
                        $('.multiselect-container').find('input').attr("disabled", true);
                        setTimeout(function(){
                            $('.multiselect-container').find('input').removeAttr('disabled');
                        }, 1300);
                        console.log("[sel_assay,checked]", [sel_assay, checked]);
                        getSelectedAssays(sel_assay, checked, selectedassaysDiv, false, false);
                    },
                    // Event when the user click on the Select All Option
                    onSelectAll: function() {
                        // var selectedItems = [];
                        var selectedItems = selected_assays;
                        showAssaysLoadingSpinner();
                        $('#assays option:selected').each(function(index, item){
                            if(selectedItems.indexOf($(this).val()) < 0){
                                selectedItems.push($(this).val());
                            }
                        });
                        getSelectedAssays(selectedItems, false, selectedassaysDiv, true, false);
                        hideAssaysLoadingSpinner();
                    },
                    // Event when the user click on the De-Select All Option
                    onDeselectAll: function() {
                        getSelectedAssays([], false, selectedassaysDiv, false, true);
                    }
                }
            );
            if (tempselectedassays.filter(obj => obj.assay.indexOf("(null)") >-1).length > 0){
                $('.multiselect-container').tooltip({
                    title: 'Records with no assay can be accessed by choosing the (null) option for the desired view',
                    trigger: 'hover'
                });
            }
            // retain the previously selected assays when selected Category is 'Correlate Data'
            console.log("SelectedAssayObjList: ",SelectedAssayObjList);
            selectedassaysDiv.empty()
            if (SelectedAssayObjList.length > 0 ) {
                // built-in functionality by bootstrap multi-select to manually select items. The 'true' param will trigger the onchange function
                // This is to fix the issue on bootstrap-multiselect getting messed up whenever we add additional item in layer 2.
                // We trigger the onchange event for the first item only to load the previously selected items in layer 4
                selected_assays = SelectedAssayObjList.map(obj => obj.assay);
                $('#assays').multiselect('select', selected_assays[0], true);
                // then we select again the previously selected items in layer 3
                // we ommit the 'true' param because we just need to just select again the previously selected items in layer 4
                $('#assays').multiselect('select', selected_assays);
                selectedassaysDiv.empty();
                for (var i=0; i < selected_assays.length; i++) {
                    // append the selected items and display to layer 4
                    prependToSelectedAssayDiv(selectedassaysDiv, selected_assays[i]);
                }
            }

            hideAssaysLoadingSpinner();
        }, error: function(err) {
          console.log(err);
        }
      });
    }
    else{
            if($(".assays").is(":empty") ){ // if .assays has been created yet make it empty then append the multiselct and list of selected fields as children
              var assaysDiv = $(".assays").empty();
              var assaySelect = $("<select>", {'id': 'assays', 'multiple': 'multiple'}).appendTo(assaysDiv);
              var selectedassaysDiv = $("<div>", {'id': 'selectedassaysDiv'}).appendTo(assaysDiv);
            }

            var assaysDiv = $(".assays");
            var assaySelect = $("#assays");
            var selectedassaysDiv = $("#selectedassaysDiv");
            var assay = '';
            var pro = '';
            var assayObj = {};
            // scenario when user deselect a view in layer 2, we remove the corresponding subcategories in layer 3
            // we get only all of the previously selected subcategories that are not under the deselected view
            tempselectedassays = []
            SelectedProgObjList.forEach(obj => {
                obj.child_assays.forEach(a =>{
                    tempselectedassays.push({pro: obj.pro, assay: a});
                });
            });
            tempselectedassays = tempselectedassays.filter(onlyUnique);
            // we remove the datatype in the list of selected_assays and tempSelectedprogram
            // based on the de-selected datatype
            for (var x=0; x < selected_assays.length; x++) {
                for (var i=0; i < tempselectedassays.length; i++) {
                    // if the datatype is present on both list, we go to the next item of the tempselectedProg
                    if (tempselectedassays[i].assay === selected_assays[x]) { break; }
                    else {
                        // we reach the end of the list so we know that it's not existing in the selected_assays
                        if (i == (tempselectedassays.length - 1)) {
                            lastDeSelectedAssay = selected_assays[x]; // we get the last deselected assay
                            selected_assays.splice(selected_assays.indexOf(lastDeSelectedAssay), 1); // we remove in the selected_assays
                            tempselectedassays.splice(tempSelectedprogram.indexOf(lastDeSelectedAssay), 1); // we remove in the tempSelectedprogram
                        }
                    }
                }
            }

        // dynamically append to multiselect options
        assaySelect.empty();
        for (var i=0; i < tempselectedassays.length; i++) {
            assaySelect.prepend($("<option>", { 'value' : tempselectedassays[i].assay }).html(tempselectedassays[i].assay));
        }

        // initialize the bootstrap multiselect
        console.log($('#assays'));
        $('#assays').multiselect('destroy');
        $('#assays').multiselect(
            {
                buttonWidth: '243px',
                maxHeight: 400,
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                filterPlaceholder: 'Search for Sub-Category...',
                // Event when user select a single item
                onChange: function(element, checked) {
                    var sel_assay = element.val(); //trim();
                    if (!(checked && SelectedAssayObjList.filter(obj => obj.assay === sel_assay).length > 0)){
                        // we set the timeouts before executing the getSelectedAssay()
                        // to avoid the incorrect rendering of the bootstrap multi-select
                        $('.multiselect-container').find('input').attr("disabled", true);
                        setTimeout(function(){
                            $('.multiselect-container').find('input').removeAttr('disabled');
                        }, 1300);
                        console.log("[sel_assay,checked]", [sel_assay, checked]);
                        getSelectedAssays(sel_assay, checked, selectedassaysDiv, false, false);
                    }
                },
                // Event when the user click on the Select All Option
                onSelectAll: function() {
                    var selectedItems = selected_assays;
                    showAssaysLoadingSpinner();
                    $('#assays option:selected').each(function(index, item){
                        if (selectedItems.indexOf($(this).val()) < 0){
                            selectedItems.push($(this).val());
                        }
                    });
                    getSelectedAssays(selectedItems, false, selectedassaysDiv, true, false);
                    hideAssaysLoadingSpinner();
                },
                // Event when the user click on the De-Select All Option
                onDeselectAll: function() {
                    getSelectedAssays([], false, selectedassaysDiv, false, true);
                }
            }
        );
        if (tempselectedassays.filter(obj => obj.assay.indexOf("(null)") >-1).length > 0){
            $('.multiselect-container').tooltip({
                title: 'Records with no assay can be accessed by choosing the (null) option for the desired view',
                trigger: 'hover'
            });
        }
        // retain the previously selected assays when selected Category is 'Correlate Data'
        console.log(SelectedAssayObjList);
        selectedassaysDiv.empty()
        if (SelectedAssayObjList.length > 0 ) {
            // built-in functionality by bootstrap multi-select to manually select items. The 'true' param will trigger the onchange function
            // This is to fix the issue on bootstrap-multiselect getting messed up whenever we add additional item in layer 2.
            // We trigger the onchange event for the first item only to load the previously selected items in layer 4
            selected_assays = SelectedAssayObjList.map(obj => obj.assay);
            $('#assays').multiselect('select', selected_assays[0], true);
            // then we select again the previously selected items in layer 3
            // we ommit the 'true' param because we just need to just select again the previously selected items in layer 4
            $('#assays').multiselect('select', selected_assays);
            selectedassaysDiv.empty();
            for (var i=0; i < selected_assays.length; i++) {
                // append the selected items and display to layer 4
                prependToSelectedAssayDiv(selectedassaysDiv, selected_assays[i]);
            }
        }

        hideAssaysLoadingSpinner();

    }
}


function populateDataTypesSubCategories(datamartView, assays, programs) {

        var lastSelectedAssay = assays[assays.length - 1];

        showDatatypesLoadingSpinner();
        var DTSubCategoriesList = []; // containers for the raw query results
        // var request = composeQueryOptions(querySubCategories(datamartView, assays, programs));
        if (selected_category != 'Correlate Data') {
            // clears the containers
            selected_datatypes = [];
            tempSelectedSubCat = [];
            // tempFieldsObjList = [];
            tempSelectedFields = [];
        }
        console.log("ASSAYS IN DATA MART: ", assays);
        if (assays !== "''" && assays !== ""){

          let stmnt = querySubCategories(datamartView, assays, programs);
          // console.log("STATEMENT: ", stmnt);
          let bd = {
            statement: stmnt,
            organization: org,
            email: emailId
          }
          let stmntStr = JSON.stringify(bd);

          $.ajax({
              type: 'POST',
              url: 'https://dl70eg51ke.execute-api.us-east-2.amazonaws.com/dev',
              data: stmntStr,
              crossDomain: true,
              contentType: 'application/json',
              beforeSend: function (xhr) {
                 xhr.setRequestHeader('Authorization', authorizer);
              },
              success: function(data, status) {
                // console.log("Data: ", data);
            // request.execute(function(json, raw) {
                DTSubCategoriesList = data;

                if($(".datatypes").is(":empty") ){
                  var datatypesDiv = $(".datatypes").empty();
                  var dtSubCatSelect = $("<select>", {'id': 'dtSubCategories', 'multiple': 'multiple'}).appendTo(datatypesDiv);
                  var selectedDTDiv = $("<div>", {'id': 'selectedDTDiv'}).appendTo(datatypesDiv);
                }

                var datatypesDiv = $(".datatypes");
                var dtSubCatSelect = $("#datatypes");
                var selectedDTDiv = $("#selectedDTDiv");
                var datatype = '';
                var assay = '';
                var dtObj = {};
                if (DTSubCategoriesList) {
                    // store all the newly retrieved items from BQ to temp list
                    // tempSelectedSubCat = [];
                    for (var i=0; i < DTSubCategoriesList.length; i++) {
                        datatype = DTSubCategoriesList[i].subcategory.trim();
                        assay = DTSubCategoriesList[i].assay.trim();
                        dtObj = { 'assay': assay, 'datatype': datatype};
                        tempSelectedSubCat.push(dtObj);
                        SelectedAssayObjList.forEach(obj => {
                        //add this datatype as a child of appropriate parents
                            if (obj.assay === assay && obj.child_datatypes.indexOf(datatype) < 0){
                                obj.child_datatypes.push(datatype);
                                SelectedDTObjList.forEach(dt =>{
                                    if(dt.datatype === datatype){
                                        dt.parent_assays.push(obj.assay);
                                    }
                                });
                            }
                            //handles adding the null assay as a parent
                            if(obj.assay === "" && lastSelectedAssay === null && obj.child_datatypes.indexOf(datatype) < 0){
                                obj.child_datatypes.push(datatype);
                                SelectedDTObjList.forEach(dt =>{
                                    if(dt.datatype === datatype){
                                        dt.parent_assays.push(obj.assay);
                                    }
                                });
                            }
                        });
                        if(autoSelectDatatypes.indexOf(datatype) > -1){ // handles autoselection of specified datatypes like Metadata_DLCsum
                            var my_parents = SelectedAssayObjList.filter(obj => obj.child_datatypes.indexOf(datatype) > -1).map(obj => obj.assay);
                            if (SelectedDTObjList.filter(obj => obj.datatype === datatype).length < 1){
                                selectDT(my_parents,datatype);
                                selected_datatypes = SelectedDTObjList.map(obj => obj.datatype);
                                populateFieldsResults(selected_datatypes,datatype);
                                $('#datatypes').multiselect('select', selected_datatypes, true);
                            }
                        }
                        else if(DTSubCategoriesList.length === 1){ // handles auto selection of a single datatype (ie ppdm assay)
                            if (SelectedDTObjList.filter(obj => obj.datatype === datatype).length < 1){
                                var my_parents = SelectedAssayObjList.filter(obj => obj.child_datatypes.indexOf(datatype) > -1).map(obj => obj.assay);
                                selectDT(my_parents,datatype);
                                selected_datatypes = SelectedDTObjList.map(obj => obj.datatype);
                                $('#datatypes').multiselect('select', selected_datatypes, true);
                                prependToSelectedDTDiv(selectedDTDiv, selected_datatypes[0]);
                            }
                        }

                    }
                }
                else {
                    // scenario when user deselect a view in layer 2, we remove the corresponding subcategories in layer 3
                    // we get only all of the previously selected subcategories that are not under the deselected view
                    tempSelectedSubCat = tempSelectedSubCat.filter(x => x !== lastSelectedAssay);
                    // we remove the datatype in the list of selected_datatypes and tempSelectedDT
                    // based on the de-selected datatype
                    for (var x=0; x < selected_datatypes.length; x++) {
                        for (var i=0; i < tempSelectedSubCat.length; i++) {
                            // if the datatype is present on both list, we go to the next item of the tempSelectedSubCat
                            if (tempSelectedSubCat[i].datatype === selected_datatypes[x]) { break; }
                            else {
                                // we reach the end of the list so we know that it's not existing in the selected_datatypes
                                if (i == (tempSelectedSubCat.length - 1)) {
                                    lastDeSelectedDatatype = selected_datatypes[x]; // we get the last selected datatype
                                    selected_datatypes.splice(selected_datatypes.indexOf(lastDeSelectedDatatype), 1); // we remove in the selected_datatypes
                                    tempSelectedSubCat.splice(tempSelectedSubCat.indexOf(lastDeSelectedDatatype), 1); // we remove in the tempSelectedDT
                                }
                            }
                        }
                    }
                }

                // dynamically append to multiselect options
                $("#dtSubCategories").empty();
                tempSelectedSubCat = [];

                SelectedAssayObjList.forEach(function(a) {
                    a.child_datatypes.forEach(function(d) {
                        if(tempSelectedSubCat.indexOf(d) < 0){
                            tempSelectedSubCat.push(d);
                        }
                    });
                });

                for (var i=0; i < tempSelectedSubCat.length; i++) {
                    $("#dtSubCategories").prepend($("<option>", { 'value' : tempSelectedSubCat[i] }).html(tempSelectedSubCat[i]));
                }

                // initialize the bootstrap multiselect
                $('#dtSubCategories').multiselect('destroy');
                $('#dtSubCategories').multiselect(
                    {
                        buttonWidth: '243px',
                        maxHeight: 400,
                        includeSelectAllOption: true,
                        enableFiltering: true,
                        enableCaseInsensitiveFiltering: true,
                        filterPlaceholder: 'Search for Sub-Category...',
                        // Event when user select a single item
                        onChange: function(element, checked) {
                            var sel_datatype = element.val().trim();

                            // we set the timeouts before executing the getSelectedDataTypes()
                            // to avoid the incorrect rendering of the bootstrap multi-select
                            $('.multiselect-container').find('input').attr("disabled", true);
                            setTimeout(function(){
                                $('.multiselect-container').find('input').removeAttr('disabled');
                            }, 1300);

                            getSelectedDataTypes(sel_datatype, checked, selectedDTDiv, false, false);
                        },
                        // Event when the user click on the Select All Option
                        onSelectAll: function() {
                            showDatatypesLoadingSpinner();
                            SelectedDTObjList = [];
                            var selectedItems = selected_datatypes;
                            $('#dtSubCategories option:selected').each(function(index, item){
                                if (selectedItems.indexOf($(this).val()) < 0){
                                    selectedItems.push($(this).val());
                                }
                            });
                            // conpsole.log('selectedItems =======> ', selectedItems);
                            getSelectedDataTypes(selectedItems, false, selectedDTDiv, true, false);
                            hideDatatypesLoadingSpinner();
                        },
                        // Event when the user click on the De-Select All Option
                        onDeselectAll: function() {
                            getSelectedDataTypes([], false, selectedDTDiv, false, true);
                        }
                    }
                );

                // retain the previously selected datatypes when selected Category is 'Correlate Data'
                selected_datatypes = SelectedDTObjList.map(obj => obj.datatype);
                selectedDTDiv.empty()
                if (selected_datatypes.length > 0){
                    // built-in functionality by bootstrap multi-select to manually select items. The 'true' param will trigger the onchange function
                    // This is to fix the issue on bootstrap-multiselect getting messed up whenever we add additional item in layer 2.
                    // We trigger the onchange event for the first item only to load the previously selected items in layer 4
                    $('#dtSubCategories').multiselect('select', selected_datatypes[0], true);
                    // then we select again the previously selected items in layer 3
                    // we ommit the 'true' param because we just need to just select again the previously selected items in layer 4
                    $('#dtSubCategories').multiselect('select', selected_datatypes);
                    selectedDTDiv.empty();
                    for (var i=0; i < selected_datatypes.length; i++) {
                        // append the selected items and display to layer 4
                            prependToSelectedDTDiv(selectedDTDiv, selected_datatypes[i]);
                    }
                }

                hideDatatypesLoadingSpinner();
            // });
          }, error: function(err) {
            console.log(err);
          }
        });

        }
        else{
                if($(".datatypes").is(":empty") ){
                  var datatypesDiv = $(".datatypes").empty();
                  var dtSubCatSelect = $("<select>", {'id': 'dtSubCategories', 'multiple': 'multiple'}).appendTo(datatypesDiv);
                  var selectedDTDiv = $("<div>", {'id': 'selectedDTDiv'}).appendTo(datatypesDiv);
                }

                var datatypesDiv = $(".datatypes");
                var dtSubCatSelect = $("#datatypes");
                var selectedDTDiv = $("#selectedDTDiv");
                var datatype = '';
                var assay = '';
                var dtObj = {};
                // scenario when user deselect a view in layer 2, we remove the corresponding subcategories in layer 3
                // we get only all of the previously selected subcategories that are not under the deselected view
                tempSelectedSubCat = tempSelectedSubCat.filter(x => x !== lastSelectedAssay);
                // we remove the datatype in the list of selected_datatypes and tempSelectedDT
                // based on the de-selected datatype
                for (var x=0; x < selected_datatypes.length; x++) {
                    for (var i=0; i < tempSelectedSubCat.length; i++) {
                        // if the datatype is present on both list, we go to the next item of the tempSelectedSubCat
                        if (tempSelectedSubCat[i] === selected_datatypes[x]) { break; }
                        // if (tempSelectedSubCat[i].datatype === selected_datatypes[x]) { break; }
                        else {
                            // we reach the end of the list so we know that it's not existing in the selected_datatypes
                            if (i == (tempSelectedSubCat.length - 1)) {
                                lastDeSelectedDatatype = selected_datatypes[x]; // we get the last selected datatype
                                selected_datatypes.splice(selected_datatypes.indexOf(lastDeSelectedDatatype), 1); // we remove in the selected_datatypes
                                tempSelectedSubCat.splice(tempSelectedSubCat.indexOf(lastDeSelectedDatatype), 1); // we remove in the tempSelectedDT
                            }
                        }
                    }
                }

            // dynamically append to multiselect options
            $("#dtSubCategories").empty();
            tempSelectedSubCat = [];

            SelectedAssayObjList.forEach(function(a) {
                a.child_datatypes.forEach(function(d) {
                    if(tempSelectedSubCat.indexOf(d) < 0){
                        tempSelectedSubCat.push(d);
                    }
                });
            });

            for (var i=0; i < tempSelectedSubCat.length; i++) {
                $("#dtSubCategories").prepend($("<option>", { 'value' : tempSelectedSubCat[i] }).html(tempSelectedSubCat[i]));
            }

            // initialize the bootstrap multiselect
            $('#dtSubCategories').multiselect('destroy');
            $('#dtSubCategories').multiselect(
                {
                    buttonWidth: '243px',
                    maxHeight: 400,
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,
                    filterPlaceholder: 'Search for Sub-Category...',
                    // Event when user select a single item
                    onChange: function(element, checked) {
                        var sel_datatype = element.val().trim();

                        // we set the timeouts before executing the getSelectedDataTypes()
                        // to avoid the incorrect rendering of the bootstrap multi-select
                        $('.multiselect-container').find('input').attr("disabled", true);
                        setTimeout(function(){
                            $('.multiselect-container').find('input').removeAttr('disabled');
                        }, 1300);

                        getSelectedDataTypes(sel_datatype, checked, selectedDTDiv, false, false);
                    },
                    // Event when the user click on the Select All Option
                    onSelectAll: function() {
                        showDatatypesLoadingSpinner();
                        SelectedDTObjList = [];
                        var selectedItems = selected_datatypes;
                        $('#dtSubCategories option:selected').each(function(index, item){
                            if(selectedItems.indexOf($(this).val()) < 0){
                                selectedItems.push($(this).val());
                            }
                        });
                        // conpsole.log('selectedItems =======> ', selectedItems);
                        getSelectedDataTypes(selectedItems, false, selectedDTDiv, true, false);
                        hideDatatypesLoadingSpinner();
                    },
                    // Event when the user click on the De-Select All Option
                    onDeselectAll: function() {
                        getSelectedDataTypes([], false, selectedDTDiv, false, true);
                    }
                }
            );

            // retain the previously selected datatypes when selected Category is 'Correlate Data'
            selected_datatypes = SelectedDTObjList.map(obj => obj.datatype);
            selectedDTDiv.empty()
            if (selected_datatypes.length > 0){
                // built-in functionality by bootstrap multi-select to manually select items. The 'true' param will trigger the onchange function
                // This is to fix the issue on bootstrap-multiselect getting messed up whenever we add additional item in layer 2.
                // We trigger the onchange event for the first item only to load the previously selected items in layer 4
                $('#dtSubCategories').multiselect('select', selected_datatypes[0], true);
                // then we select again the previously selected items in layer 3
                // we ommit the 'true' param because we just need to just select again the previously selected items in layer 4
                $('#dtSubCategories').multiselect('select', selected_datatypes);
                selectedDTDiv.empty();
                for (var i=0; i < selected_datatypes.length; i++) {
                    // append the selected items and display to layer 4
                        prependToSelectedDTDiv(selectedDTDiv, selected_datatypes[i]);
                }
            }

            hideDatatypesLoadingSpinner();

        }

}



function populateFieldsResults(subcategories, lastSelectedDatatype) {
    showFieldsLoadingSpinner();

    var FieldsResultsList = [];
    // var request = composeQueryOptions(queryFields(lastSelectedDatatype));

    let stmnt = queryFields(lastSelectedDatatype);
    // console.log("STATEMENT: ", stmnt);
    let bd = {
      statement: stmnt,
      organization: org,
      email: emailId
    }
    let stmntStr = JSON.stringify(bd);

    $.ajax({
        type: 'POST',
        url: 'https://dl70eg51ke.execute-api.us-east-2.amazonaws.com/dev',
        data: stmntStr,
        crossDomain: true,
        contentType: 'application/json',
        beforeSend: function (xhr) {
           xhr.setRequestHeader('Authorization', authorizer);
        },
        success: function(data, status) {
          console.log("FIELD RESULTS DATA: ", data);
    // request.execute(function(json, raw) {
        FieldsResultsList = data;
        $(".fields-results").empty();
        if($(".fields-results").is(":empty") ) {
            var fieldsDiv = $(".fields-results").empty();
            var fieldResSelect = $("<select>", {'id': 'fieldsResults', 'multiple': 'multiple'}).appendTo(fieldsDiv);
            var selectedFieldDiv = $("<div>", {'id': 'selectedFieldDiv'}).appendTo(fieldsDiv);
        }
        var fieldsDiv = $(".fields-results");
        var fieldResSelect = $("#fieldsResults");
        var selectedFieldDiv = $("#selectedFieldDiv");
        fieldResSelect.hide();
        var datatypeFieldObj = {};
        var selectedFieldsTobeRemovedUponDeselect = [];

        if (FieldsResultsList) { // there are results of the populating query (not a deselection)
            for (var i=0; i < FieldsResultsList.length; i++) {
                var fieldresult = FieldsResultsList[i].field_name.trim(); // fieldName
                var isPivoted = FieldsResultsList[i].is_pivoted; // isPivoted or Not
                var fieldDataType = FieldsResultsList[i].datatype.trim(); // field Datatype
                var fieldSubCat = FieldsResultsList[i].subcategory.trim();
                // we save both as object for checking of field if pivoted or not
                var fieldpivotObj = {'fieldName' : fieldresult, 'isPivoted' : isPivoted, 'fieldDataType' : fieldDataType};
                if (tempFieldsObjList.filter(obj => obj.fieldName === fieldresult).length === 0){
                    tempFieldsObjList.push(fieldpivotObj);
                    datatypeFieldObj = { 'datatype': fieldSubCat, 'field': fieldresult};

                    if (tempSelectedFields.indexOf(datatypeFieldObj) < 0){
                        tempSelectedFields.push(datatypeFieldObj);
                    }
                }
                for (var j = 0; j < autoSelectFields.length; j++){
                    // if (selected_category === "Correlate Data" && fieldresult.indexOf(autoSelectFields[j]) === 0){
                    if (fieldresult.indexOf(autoSelectFields[j]) === 0){
                        if (selected_fields.indexOf(fieldresult) < 0){
                            console.log("autoselecting filed", fieldresult);
                            selected_fields.push(fieldresult);
                        }
                    }
                }
                // for (var j = 0; j < autoSelectFields.length; j++){
                //     if (selected_category === "Correlate Data" && fieldresult.indexOf(autoSelectFields[j]) === 0){
                //         if (selected_fields.indexOf(fieldresult) < 0){
                //             console.log("autoselecting filed", fieldresult);
                //             selected_fields.push(fieldresult);
                //         }
                //     }
                // }
                SelectedDTObjList.forEach(obj => {
                    if (obj.datatype === fieldSubCat){
                        if (obj.child_fields.indexOf(fieldresult) < 0){ // add as child if new
                            obj.child_fields.push(fieldresult);
                        }
                    }
                });
            }

        }
        else {
            // scenario when user deselect a datatype in layer 3, we remove the corresponding fields in layer 4
            lastSelectedDatatype = (lastSelectedDatatype == '') ? lastDeSelectedDatatype : lastSelectedDatatype;
            lastSelectedDatatype = "'" + lastSelectedDatatype + "'";
            // we remove also items in the selected_fields, pCol and col lists
            selectedFieldsTobeRemovedUponDeselect = tempSelectedFields.filter(obj => obj.datatype === lastDeSelectedDatatype);
            for (var i=0; i<selectedFieldsTobeRemovedUponDeselect.length; i++) {
                var sfIdx = selected_fields.indexOf(selectedFieldsTobeRemovedUponDeselect[i].field);
                if (sfIdx > -1) { selected_fields.splice(sfIdx, 1); }

                var pColIdx = pCol.indexOf(selectedFieldsTobeRemovedUponDeselect[i].field);
                if (pColIdx > -1) { pCol.splice(pColIdx, 1); }

                var colIdx = col.indexOf(selectedFieldsTobeRemovedUponDeselect[i].field);
                if (colIdx > -1) { col.splice(colIdx, 1); }
            }
            tempSelectedFields = tempSelectedFields.filter(obj => obj.datatype !== lastDeSelectedDatatype);
        }

        fieldResSelect.empty();
        tempSelectedFields = [];
        SelectedDTObjList.forEach(dt =>{
            dt.child_fields.forEach( f => {
                var tempFieldObj = {datatype: dt, field: f};
                if (tempSelectedFields.indexOf(tempFieldObj) < 0){
                    tempSelectedFields.push(tempFieldObj);
                }
            })
        });
        var invivoPKPD_dts = ["PK","PD","PKPD"];
        for (var i=0; i < tempSelectedFields.length; i++) {
            if (invivoPKPD_dts.indexOf(tempSelectedFields[i].datatype.datatype) > -1){
                try{ // if first selected datatype then can't use each so will use catch
                    var doit = true;
                    $('#fieldsResults > option').each(function(){
                        if(this.text === tempSelectedFields[i].field){
                            doit = false;
                        }
                    });
                    if(doit){
                        fieldResSelect.prepend($("<option>", { 'value' : tempSelectedFields[i].field }).html(tempSelectedFields[i].field));
                    }
                }
                catch (e){
                    fieldResSelect.prepend($("<option>", { 'value' : tempSelectedFields[i].field }).html(tempSelectedFields[i].field));
                }
            }
            else{
                fieldResSelect.prepend($("<option>", { 'value' : tempSelectedFields[i].field }).html(tempSelectedFields[i].field));
            }
        }

        // initialize the bootstrap multiselect. This needs to be here since we are dynamically adding the options tags
        $('#fieldsResults').multiselect('destroy');
        $('#fieldsResults').multiselect(
            {
                buttonWidth: '243px',
                maxHeight: 400,
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                // Event when user select/deselect an item
                onChange: function(element, checked) {
                    var sel_field = element.val().trim();
                    console.log('Selected Field ===> ', sel_field);
                    console.log('isChecked ===========> ', checked);
                    getSelectedFieldsResults(sel_field, lastSelectedDatatype, checked, selectedFieldDiv, false, false);
                },
                // Event when user use the Select All option
                onSelectAll: function() {
                    var selectedItems = selected_fields;
                    $('#fieldsResults option:selected').each(function(index, item){
                        if (selectedItems.indexOf($(this).val()) < 0){
                            selectedItems.push($(this).val());
                        }
                    });
                    console.log('selectedItems =======> ', selectedItems);
                    getSelectedFieldsResults(selectedItems, lastSelectedDatatype, false, selectedFieldDiv, true, false);
                },
                // Event when user use the De-Select All option
                onDeselectAll: function() {
                    getSelectedFieldsResults([], lastSelectedDatatype, false, selectedFieldDiv, false, true);
                }
            }
        );

        // retain the previously selected fields upon 3rd layer select
        if (selected_fields.length > 0) {
            console.log('Retaining selected fields ======> ', selected_fields);
            $('#fieldsResults').multiselect('select', selected_fields);
            // add the previously selected items in the layer box
            selected_fields = selected_fields.filter(field =>
                tempSelectedFields.filter(obj => obj.field === field).length > 0); // this field still appears in the multiselect
            selected_fields.forEach(field => {
                prependToSelectedFieldDiv(selectedFieldDiv,field);
            });
            saveAndRetainFilterInputs();
        }

        hideFieldsLoadingSpinner();
    // });
    }, error: function(err) {
      console.log(err);
    }
  });
}

function getSelectedCat() {

    emptyQueryBox();
    selected_fields = []; // clears the selected fields
    selected_Programs = [];
    selected_assays = [];
    tempFieldsObjList = []; // we clear first the field object list
    selected_datatypes = [];
    tempselectedProg = [];
    tempSelectedFields = [];
    SelectedDTObjList = [];
    SelectedAssayObjList = [];
    SelectedProgObjList = [];
    pCol = [];
    col = [];
    isInitialSelect = true;
    clearFilterRows();

    //remove the searched buttons everything is new now
    $("#download_button").hide();
    $("#save_sql_button").hide();

    // We empty the 3 listboxes first
    $(".datamarts-views").empty();
    $(".datatypes").empty();
    $(".fields-results").empty();

    selected_category = $('input[name=categoryRdoBtn]:checked').val();
    populateDatamartsViews(selected_category);
    loadSelectedfields(1); // this is to empty the dropdown fields in the filter row

}

function getSelectedDatamartView(selView, isChecked) {

    emptyQueryBox();
    if (selected_category !== 'Correlate Data') $(".datatypes").empty(); // this is to refresh the 3rd layer including the selected subcategories
    $(".fields-results").empty(); // we empty the 4th Layer

    var view = '';

    // if the selection is checkbox (Scenario where selected category is 'Correlate Data')
    if (viewsInputType === 'checkbox') {
        var selected_views = $('input.datamart-view:checkbox:checked').map(function() { return this.value; }).get();
        var idx = tempSelectedViews.indexOf(selView);
        if (isChecked) {
            if (idx < 0) { // selected item is new
                tempSelectedViews.push(selView.trim());
            }
            view = "'" + selView + "'";
        }
        else {
            if (idx > -1) { // we remove the item in the templist
                tempSelectedViews.splice(idx, 1);
            }
            if (tempSelectedViews.length < 1) { // we empty the first filter row fields drop down if no views selected
                $('#select1').empty();
            }
            view = "''";
        }

        selected_datamartView = jQuery.extend(true, [], selected_views);

    }
    // if the selection is radio button
    else {
        view = "'" + $('input[name=viewsRdoBtn]:checked').val() + "'";
        $(".datatypes").empty(); // we clear first the 3rd layer
        selected_datamartView = view.toString().replace(/\'/g,"");
        tempSelectedprogram = []; // we clear the list that checks if the selected datatype is new since this is for radio buttons and every select is a new item
        selected_fields = [];
        pCol = [];
        col = [];
        isInitialSelect = true;
        clearFilterRows();

        // we clear the filter fields dropdown based in the number of the row
        for (var i=0; i <= x; i++) { // x here is the global variable for the total number of filter rows
            $('#select' + i).empty(); // we clear all of the fields drop down on each row
            getSymbol(i); // we set to default all of the symbols
            $('#searchPhraseCmpdID'+i).val(''); // we clear all of the text area on each filter rows
        }
    }
    $('.datamarts-views').find('input').attr("disabled", true);
    setTimeout(function(){
        $('.datamarts-views').find('input').removeAttr('disabled');
    }, 1500);
    // populateDataTypesSubCategories(view, selected_assays);
    $(".assays").empty();
    populatePrograms(view);
}

function getSelectedDataTypes(sel_datatype, isChecked, selectedDTDiv, isSelectAll, isDeSelectAll) {

    emptyQueryBox();

    //check first if the selected item is in the list before adding
    var index = selected_datatypes.indexOf(sel_datatype);

    if (isDeSelectAll) {
        console.log('isDeSelectAll ====> ', isDeSelectAll);
        selectedDTDiv.empty(); // clear the div that contains the selected datatypes in layer 3
        $('.fields-results').empty();// clear the field containers
        selected_datatypes = [];
        selected_fields = [];
        SelectedDTObjList =[];
        tempFieldsObjList = [];
        tempSelectedFields = [];
        return;
    }
    else if (isSelectAll) {
        console.log('isSelectAll ====> ', isSelectAll);
        selected_datatypes = []; // we clear the selected datatypes
        SelectedDTObjList = [];
        selected_datatypes = sel_datatype; // we set the all of the selected items from UI to selected_datatypes
        selectedDTDiv.empty(); // we clear the div that contains the selected datatypes in layer 3
        // then we display again in UI in layer 3
        for (var i=0; i < selected_datatypes.length; i++) {
            var mypassays = [];
            SelectedAssayObjList.forEach(a => { // get all parent assays
                if (a.child_datatypes.indexOf(selected_datatypes[i]) > -1){
                    mypassays.push(a.assay);
                }
            });
            selectDT(mypassays ,selected_datatypes[i]); //create datatype object and append to SelectedDTObjList
            prependToSelectedDTDiv(selectedDTDiv, selected_datatypes[i]); // add to selected DT list
        }

        // then we populate the 4th layer
        var quotedStrings = "'" + selected_datatypes.join("','") + "'";
        console.log('selected_datatypes ===> ', quotedStrings);
        populateFieldsResults(selected_datatypes,selected_datatypes);
        return;
    }
    else if (isChecked && (index < 0)) {
        // scenario where trigger is check and item is new

        selected_datatypes.push(sel_datatype);
        //get the list of program objects with this as a child
        var parentObjects = SelectedAssayObjList.filter( obj => obj.child_datatypes.indexOf(sel_datatype) > -1);
        var parents = parentObjects.map(obj => obj.assay); // get just the program names
        selectDT(parents, sel_datatype);
        selected_assays[selected_assays.indexOf("")] = null;
        prependToSelectedDTDiv(selectedDTDiv, sel_datatype);
    }
    else if (isChecked && (index > -1)) {
        // scenario where in trigger is check and items are existing
        // already in selected_datatypes list. This is to show/retain previously selected values
        prependToSelectedDTDiv(selectedDTDiv, sel_datatype);
    }
    else {
        // Scenario for de-select an item in 3rd layer. we remove the selected item in the list during de-select
        deSelectDT(sel_datatype);
        if (index > -1) selected_datatypes.splice(index, 1);
        tempSelectedSubCat.splice(tempSelectedSubCat.indexOf(sel_datatype), 1);
        // tempSelectedSubCat = tempSelectedSubCat.filter(obj => obj.datatype !== sel_datatype);
        var concatenated_sel_datatype = sel_datatype.toString().replace(/ +/g, "").replace("%","percent");
        $('div.' + concatenated_sel_datatype).remove();
        var quotedStrings = "''"; // we forcefully set this to empty so that the result is empty as well
        console.log('selected_datatypes upon remove ===> ', sel_datatype);
        // populateFieldsResults(quotedStrings, sel_datatype);
        lastDeSelectedDatatype = sel_datatype;
        selected_datatypes = SelectedDTObjList.map(dt => dt.datatype);
        populateFieldsResults(selected_datatypes,'');
        return;
    }

    var sel_field = '';
    var idx = -1;
    // Due to the nature of the checkboxes, this is the logic to check of what new item is selected.
    for (var i=0; i < selected_fields.length; i++) {
        idx = tempSelectedFields.indexOf(selected_fields[i]);
        if (idx < 0) { // selected item is new
            sel_field = selected_fields[i];
            tempSelectedFields.push(sel_field);
            break;
        }
    }

    // then we populate 4th layer based on the selected subcategory(s)
    var quotedStrings = "'" + sel_datatype + "'";
    console.log('selected_datatypes ===> ', selected_datatypes);
    console.log('tempSelectedprogram =======> ', tempSelectedprogram);
    console.log('quotedStrings ===> ', quotedStrings);
    // populateFieldsResults(sel_datatype, selected_datatypes[selected_datatypes.length - 1]);
    populateFieldsResults(sel_datatype, sel_datatype);
}

function getSelectedPrograms(sel_program, isChecked, selectedprogramDiv, isSelectAll, isDeSelectAll) {

    // If any of the program descriptions contain special characters, they must be escaped.
    // Currently escaping only apostrophe
    if(typeof sel_program == 'string'){ // one program (string) passed in
        sel_program = escapeString(sel_program);
    }
    if(typeof sel_program == 'object'){ // Array of programs (Array of String) passed in
        for(var i=0; i < sel_program.length; i++) {
            sel_program[i] = escapeString(sel_program[i]);
           }
    }
    // console.log("sel_program2: " + sel_program);

    sel_program[sel_program.indexOf("")] = "null";
    emptyQueryBox();

    // we check first if the selected item is in the list before adding
    var index = selected_Programs.indexOf(sel_program);

    if (isDeSelectAll) {
        console.log('isDeSelectAll ====> ', isDeSelectAll);
        selectedprogramDiv.empty(); // we clear the div that contains the selected programs in layer 3
        SelectedProgObjList = [];
        selected_Programs =[];
        SelectedAssayObjList = [];
        SelectedDTObjList = [];

        $(".assays").empty();
        $(".fields-results").empty(); // we clear the entire layer 4
        $(".datatypes").empty(); // we clear the entire layer 4
        // them we clear the field containers
        selected_fields = [];
        tempselectedassays = [];
        tempFieldsObjList = [];
        tempSelectedFields = [];
        return;
    }
    else if (isSelectAll) {
        console.log('isSelectAll ====> ', isSelectAll);
        console.log('sel_program: ',sel_program);
        selected_Programs = []; // we clear the selected programs
        SelectedProgObjList = [];
        selected_Programs = sel_program; // we set the all of the selected items from UI to selected_Programs
        selectedprogramDiv.empty(); // we clear the div that contains the selected programs in layer 3
        // then we display again in UI in layer 3
        for (var i=0; i < selected_Programs.length; i++) {
            var lp = selected_Programs[i].split("_");
            var pview = lp[lp.length - 1]; // view suffix for this program
           selectProgram(pview, selected_Programs[i]);
           prependToSelectedprogramDiv(selectedprogramDiv, selected_Programs[i]);
        }

        // then we populate the 4th layer

        //handling null program
        var nidx = selected_Programs.indexOf("");
        if(nidx === -1){
            nidx = selected_Programs.indexOf("null");
        }

        if(nidx > -1){
            selected_Programs.splice(nidx,1);
            var quotedStrings = "\'" + selected_Programs.join("\',\'") + "\' ) OR ((pro is null)";
        }
        else{
            var quotedStrings = "\'" + selected_Programs.join("\',\'") + "\'";
        }
        console.log('selected_Programs ===> ', quotedStrings);
        populateAssays(quotedStrings, sel_program);
        return;
    }
    else if (isChecked && (index < 0)) {
        // scenario where trigger is check and item is new
        selected_Programs.push(sel_program);
        var lp = sel_program.split("_");
        var pview = lp[lp.length - 1];
        selectProgram(pview, sel_program);
        selected_Programs[selected_Programs.indexOf("")] = null;
        console.log(sel_program)
        prependToSelectedprogramDiv(selectedprogramDiv, sel_program);
    }
    else if (isChecked && (index > -1)) {
        // scenario where in trigger is check and items are existing
        // already in selected_Programs list. This is to show/retain previously selected values
        prependToSelectedprogramDiv(selectedprogramDiv, sel_program);
    }
    else {
        // Scenario for de-select an item in 2nd layer. we remove the selected item in the list during de-select
        console.log(sel_program);
        if (selected_category === "Correlate Data"){
            var sel_view = sel_program; // for correlate data scenarios we pass view instead of program for deselection?
            SelectedProgObjList.forEach(p =>{
                if (p.parent_views.indexOf(sel_view) > -1){
                    sel_program = p.pro;
                }
            });

        }
        deSelectProgram(sel_program);
        if (index > -1) selected_Programs.splice(index, 1);
        tempSelectedprogram.splice(tempSelectedprogram.indexOf(sel_program), 1);
        var concatenated_sel_program = sel_program.toString().replace(/ +/g, "").replace(/[^a-zA-Z0-9\-_.:]/g,"");
        console.log(concatenated_sel_program);
        $('div.' + concatenated_sel_program).remove();
        var quotedStrings = "''"; // we forcefully set this to empty so that the result is empty as well
        console.log('selected_Programs upon remove ===> ', sel_program);
        populateAssays(quotedStrings, sel_program);
        return;
    }

    var selprogram = '';
    var idx = -1;
    // Due to the nature of the checkboxes, this is the logic to check of what new item is selected.
    for (var i=0; i < selected_Programs.length; i++) {
        idx = tempSelectedprogram.indexOf(selected_Programs[i]);
        if (idx < 0) { // selected item is new
            selprogram = selected_Programs[i];
            tempSelectedprogram.push(selprogram);
            break;
        }
    }
    var quotedStrings = "";
    if (sel_program !== ""){
        quotedStrings = "\'" + selprogram + "\'";
    }
    else{
        quotedStrings = "null";
    }
    populateAssays(quotedStrings, selprogram);
}

let escapeString = (str) => { return str.replace(/'/, "\\'"); }; // escape apostrophies, as in Alzheimer's

function getSelectedAssays(sel_assay, isChecked, selectedassayDiv, isSelectAll, isDeSelectAll) {

    emptyQueryBox();

    // we check first if the selected item is in the list before adding
    var index = selected_assays.indexOf(sel_assay);

    if (isDeSelectAll) {
        selectedassayDiv.empty(); // we clear the div that contains the selected assays in layer 3
        $(".fields-results").empty(); // we clear the entire layer 4
        $(".datatypes").empty();
        // them we clear the field containers
        SelectedAssayObjList = [];
        SelectedDTObjList = [];
        selected_fields = [];
        tempFieldsObjList = [];
        tempSelectedFields = [];
        return;
    }
    else if (isSelectAll) {
        console.log('isSelectAll ====> ', isSelectAll);
        selected_assays = []; // we clear the selected assays
        SelectedAssayObjList = [];
        selected_assays = sel_assay; // we set the all of the selected items from UI to selected_assays
        selectedassayDiv.empty(); // we clear the div that contains the selected assays in layer 3
        // then we display again in UI in layer 3
        for (var i=0; i < selected_assays.length; i++) {
            var parentObjects = SelectedProgObjList.filter( obj => obj.child_assays.indexOf(selected_assays[i]) > -1);
            var parents = parentObjects.map(obj => obj.pro); // get just the program names of the parents
            selectAssay(parents, selected_assays[i]);
            selected_assays[selected_assays.indexOf("")] = null;
            prependToSelectedAssayDiv(selectedassayDiv, selected_assays[i]);
        }

        // then we populate the 4th layer
        var nidx = selected_assays.indexOf(null);
        if(nidx === -1){
            nidx = selected_assays.indexOf("null");
        }

        if(nidx > -1){
            selected_assays.splice(nidx,1);
            var quotedStrings = "\'" + selected_assays.join("\',\'") + "\' ) OR ((assay is null)";
        }
        else{
            var quotedStrings = "\'" + selected_assays.join("\',\'") + "\'";
        }
        console.log('selected_assays ===> ', quotedStrings);
        populateDataTypesSubCategories(selected_datamartView,selected_assays, selected_Programs);
        return;
    }
    else if (isChecked && (index < 0)) {
        // scenario where trigger is check and item is new
        selected_assays.push(sel_assay);
        //get the list of program objects with this as a child
        var parentObjects = SelectedProgObjList.filter( obj => obj.child_assays.indexOf(sel_assay) > -1);
        var parents = parentObjects.map(obj => obj.pro); // get just the program names
        selectAssay(parents, sel_assay);
        selected_assays[selected_assays.indexOf("")] = null;
        prependToSelectedAssayDiv(selectedassayDiv, sel_assay);
    }
    else if (isChecked && (index > -1)) {
        // scenario where in trigger is check and items are existing
        // already in selected_assays list. This is to show/retain previously selected values
        prependToSelectedAssayDiv(selectedassayDiv, sel_assay);
    }
    else {
        // Scenario for de-select an item in 3rd layer. we remove the selected item in the list during de-select
        deSelectAssay(sel_assay);
        if (index > -1) selected_assays.splice(index, 1);
        tempselectedassays.splice(tempselectedassays.indexOf(sel_assay), 1);
        var concatenated_sel_assay = sel_assay.toString().replace(/ +/g, "").replace(/[^a-zA-Z0-9\-_.:]/g,"");
        if(selected_category !== "Correlate Data"){
            $('div.' + concatenated_sel_assay).remove();
        }
        console.log("concatenated_sel_assay: ", concatenated_sel_assay);

        var quotedStrings = "''"; // we forcefully set this to empty so that the result is empty as well
        console.log('selected_assays upon remove ===> ', sel_assay);
        // populateAssays(quotedStrings, sel_assay);
        populateDataTypesSubCategories(selected_datamartView,"","")
        return;
    }

    var selassay = '';
    var idx = -1;
    // Due to the nature of the checkboxes, this is the logic to check of what new item is selected.
    for (var i=0; i < selected_assays.length; i++) {
        idx = tempselectedassays.indexOf(tempselectedassays.find(obj => obj.assay === selected_assays[i]));
        if (idx < 0) { // selected item is new
            selassay = selected_assays[i];
            var selassayObj = SelectedAssayObjList.filter(obj => obj.assay === selassay)[0];
            for (var j in selassayObj.parent_pros){
                var tempObj = {pro: selassayObj.parent_pros[j], assay: selassayObj.assay};
                if (tempselectedassays.indexOf(tempObj) < 0){
                    // tempselectedassays.push(tempObj);
                }
            }
            tempselectedassays.filter(onlyUnique);
            break;
        }
    }

    // then we populate 4th layer based on the selected subcategory(s)
    var quotedStrings = "'" + sel_assay + "'";
    console.log('selected_assays ===> ', selected_assays);
    console.log('tempselectedassays =======> ', tempselectedassays);
    console.log('quotedStrings ===> ', quotedStrings);
    // populateDataTypesSubCategories(selected_datamartView, selected_assays, selected_Programs);
    populateDataTypesSubCategories(selected_datamartView, [sel_assay], selected_Programs);
}

function getSelectedFieldsResults(sel_field, lastSelectedDatatype, isChecked, selectedFieldDiv, isSelectAll, isDeSelectAll) {

    // we check first if the selected item is in the list before adding
    var index = selected_fields.indexOf(sel_field);
    if (isDeSelectAll) {
        console.log('isDeSelectAll ====> ', isDeSelectAll);
        var typeaheadFieldVal = $('.fields-results').find('.multiselect-container').find('.multiselect-filter').find('.multiselect-search').val();
        // we check if the typeahead field is empty or not
        // if empty, we deselect all and clear the 4th layer
        if (typeaheadFieldVal === '') {
            selectedFieldDiv.empty();
            clearFilters();
            pCol = [];
            col = [];
            return;
        }
        // if not empty, we get all of the selected fields which is under that filtered typeahead
        // and remove that to the 4th layer and the selected_fields, pcol and col
        else {
            $('.fields-results').find('.multiselect-container').each(function(){
                $(this).find('li').each(function() {
                    if (!$(this).hasClass('multiselect-item') && !$(this).hasClass('multiselect-filter-hidden')) {
                        var deselectedItem = $(this).find('input').val();
                        // we remove the selected item in the list
                        if (selected_fields.indexOf(deselectedItem) > -1) selected_fields.splice(selected_fields.indexOf(deselectedItem), 1);
                        if (pCol.indexOf(deselectedItem) > -1) pCol.splice(pCol.indexOf(deselectedItem), 1);
                        if (col.indexOf(deselectedItem) > -1) col.splice(col.indexOf(deselectedItem), 1);
                        var replacedSpecialCharac = deselectedItem.replace(/\|/g, "_").replace(/%/g,"_");
                        $('div.' + replacedSpecialCharac).remove();
                    }
                });
            });
        }
    }
    else if (isSelectAll) {
        console.log('isSelectAll ====> ', isSelectAll);
        // selected_fields = []; // we clear first the selected_fields
        selected_fields = sel_field; // then we assign the sel_field (this contains all of the checked items in layer 4)
        selectedFieldDiv.empty(); // we clear the div that contains the selected fields in 4th layer
        for (var i=0; i < selected_fields.length; i++) {
            // we segregate the pivoted and not
            filterPivotedAndNot(selected_fields[i]);
            // console.log("col: ", col);
            // console.log("pCol: ",pCol);
            // console.log(selected_fields[i])
            // then we display the selected field in 4th layer
            prependToSelectedFieldDiv(selectedFieldDiv, selected_fields[i]);
        }
    }
    else if (isChecked && (index < 0)) {
        selected_fields.push(sel_field.toString());
        // segregate the pivoted fields and not
        filterPivotedAndNot(sel_field);
        // then we display the selected field in 4th layer
        prependToSelectedFieldDiv(selectedFieldDiv, sel_field);
    }
    else {
        // we remove the selected item in the list upon de-selection on the 4th layer
        if (index > -1) selected_fields.splice(index, 1);
        if (pCol.indexOf(sel_field) > -1) pCol.splice(pCol.indexOf(sel_field), 1);
        if (col.indexOf(sel_field) > -1) col.splice(col.indexOf(sel_field), 1);
        var replacedSpecialCharac = sel_field.replace(/\|/g, "_").replace(/%/g,"_");
        console.log(replacedSpecialCharac);
        $('div.' + replacedSpecialCharac).remove();
    }
    console.log('SELECTED =======> ', selected_fields);
    console.log('PCOL     =======> ', pCol);
    console.log('COL      =======> ', col);

    saveAndRetainFilterInputs();
    emptyQueryBox();
}

function saveAndRetainFilterInputs() {

    saveFilterInputs();

    // we updated the dropdown based on the selected fields from layer 4
    // x here is the global variable for the number of filter rows
    for (var i=1; i <= x; i++) {
        loadSelectedfields(i);
        setDomIDIndex('select' + i); // trigger the updating of symbols based on loaded fields
        getOperator(i); // we re-add the operators upon field select
    }

    reApplyFilterInputs();
}

function saveFilterInputs() {
    // we save first the previously selected and inputted filters
    for (var i=1; i <= x; i++) { // x here is the global variable for the total number of filter rows
        savedFilterInputObj = { 'filterRow': i,
                                'operator': (i==1) ? '': $("div#operator" + i + " select").val(),
                                'selectedField': $("div#tableCols" + i + " select").val(),
                                'symbol': $("div#symbol" + i + " select").val(),
                                'inputtedText': $("textarea#searchPhraseCmpdID" + i).val(),
                                'inputtedDateA': $('#inputprogrampicker'+ i +'a').val(),
                                'inputtedDateB': $('#inputprogrampicker'+ i +'b').val()
                              };
        // we check first if this is existing to the savedFilterInputs list
        var index = savedFilterInputs.findIndex(item => item.filterRow == i);
        if (index > -1) {
            // if existing, we remove the object to overwrite
            savedFilterInputs.splice(index, 1);
        }
        // then we save in the savedFilterInputs list
        savedFilterInputs.push(savedFilterInputObj);
    }

    console.log('savedFilterInputs ========> ', savedFilterInputs);
}

function reApplyFilterInputs() {
    // then we re-apply those previously saved on the UI
    if (!isInitialSelect) {
        for (var i=1; i <= x; i++) { // x here is the global variable for the total number of filter rows
            try{
            $("div#operator" + i + " select").val(savedFilterInputs[i - 1].operator);//.trigger('change');
            $("div#tableCols" + i + " select").val(savedFilterInputs[i - 1].selectedField).trigger('change');
            $("div#symbol" + i + " select").val(savedFilterInputs[i - 1].symbol).trigger('change');
            $("textarea#searchPhraseCmpdID" + i).val(savedFilterInputs[i - 1].inputtedText);
            $('#inputprogrampicker'+ i +'a').val(savedFilterInputs[i - 1].inputtedDateA);
            $('#inputprogrampicker'+ i +'b').val(savedFilterInputs[i - 1].inputtedDateB);
            }
            catch(e){}
        }
    }
    isInitialSelect = false;
}

function clearFilterRows() {
    savedFilterInputs = [];
    for (var i=1; i <= x; i++) { // x here is the global variable for the total number of filter rows
        $("div#operator" + i + " select").val('');
        $("div#tableCols" + i + " select").val('');
        $("div#symbol" + i + " select").val('');
        $("textarea#searchPhraseCmpdID" + i).val('');
        $('#inputprogrampicker'+ i +'a').val('');
        $('#inputprogrampicker'+ i +'b').val('');
    }
}

function emptyQueryBox() {
    $('#queryTxtArea').prop('disabled', true);
    $('#queryTxtArea').val('');
    isEditQuery = false;
     $('#summary_button').removeClass('disabled');
}
function filterPivotedAndNot(fieldResult) { // populates col with non-pivoted fields and pCol with pivoted fields when given a result
    for (var item in tempFieldsObjList) {

        if (fieldResult == tempFieldsObjList[item].fieldName) {
            if (tempFieldsObjList[item].isPivoted) {
                if (pCol.indexOf(fieldResult) < 0) pCol.push(fieldResult);
            }
            else {
                if (col.indexOf(fieldResult) < 0) col.push(fieldResult);
            }
            break;
        }
    }


}
//functions for prepending to selected lists
function prependToSelectedprogramDiv(selectedprogramDiv, sel_program) {
    var concatenated_program = sel_program.toString().replace(/ +/g, "").replace(/[^a-zA-Z0-9\-_.:]/g,"");
    if ($("."+concatenated_program).length < 1){
    selectedprogramDiv.prepend('<div class="form-check ' + concatenated_program +'"> \
                            <label class="form-check-label"> \
                                <input type="checkbox" checked disabled class="form-check-input">' + sel_program + ' \
                            </label> \
                         </div>');
    }
}
function prependToSelectedAssayDiv(selectedassayDiv, sel_assay) {
    if (!sel_assay){
        sel_assay = "(null)" + lastSelectedView;
    }
    var concatenated_sel_assay = sel_assay.toString().replace(/ +/g, "").replace(/[^a-zA-Z0-9\-_.:]/g,"");
    selectedassayDiv.prepend('<div class="form-check ' + concatenated_sel_assay +'"> \
                            <label class="form-check-label"> \
                                <input type="checkbox" checked disabled class="form-check-input">' + sel_assay + ' \
                            </label> \
                         </div>');
}

function prependToSelectedDTDiv(selectedDTDiv, sel_datatype) {
    var concatenated_sel_datatype = sel_datatype.toString().replace(/ +/g, "").replace("%","percent");
    selectedDTDiv.prepend('<div class="form-check ' + concatenated_sel_datatype +'"> \
                            <label class="form-check-label"> \
                                <input type="checkbox" checked disabled class="form-check-input">' + sel_datatype + ' \
                            </label> \
                         </div>');
}

function prependToSelectedFieldDiv(selectedFieldDiv, sel_field) {
    var replacedSpecialCharac = sel_field.replace(/\|/g, "_").replace(/%/g,"_"); // we remove the "|" and "%" and replace with "_"
    selectedFieldDiv.prepend('<div class="form-check ' + replacedSpecialCharac +'"> \
                                <label class="form-check-label"> \
                                    <input type="checkbox" checked disabled class="form-check-input">' + sel_field + ' \
                                </label> \
                             </div>');
}

function clearSixLayers() { //wipes the 6-layers
    $(".datamarts-views").empty();
    $(".datatypes").empty();
    $(".programs").empty();
    $(".assays").empty();
    $(".fields-results").empty();
    populateCategories();
}

function clearFilters() { // wipe the filter rows
    selected_fields = [];
    filterRows = [];
    wrapper.empty();
    x = 0; //reset the filter row counter
    addFilterRow(0);
}

function hideSummaryTable(){
    $("#summary_results").hide();
}

function showSummaryTable(){
    $("#summary_results").show();
}

function hideResultsTable(){
    $("#query_results").hide();
}

function showResultsTable(){
    $("#query_results").show();
}

function hideResultsLoadingSpinner() {
    $('#resultLoadingSpinner').hide();
    $('#cancel_button').hide();
}

function showResultsLoadingSpinner() {
    $('#resultLoadingSpinner').show();
    $('#cancel_button').show();
    $('#cancel_button').prop('disabled', false);
}
function showCategoriesLoadingSpinner() {
    $('#categoriesLoadingSpinner').show();
}

function hideCategoriesLoadingSpinner() {
    $('#categoriesLoadingSpinner').hide();
}

function showProgramsLoadingSpinner() {
    $('#programsLoadingSpinner').show();
}

function hideProgramsLoadingSpinner() {
    $('#programsLoadingSpinner').hide();
}

function showAssaysLoadingSpinner() {
    $('#assaysLoadingSpinner').show();
}

function hideAssaysLoadingSpinner() {
    $('#assaysLoadingSpinner').hide();
}

function showViewsLoadingSpinner() {
    $('#viewsLoadingSpinner').show();
}

function hideViewsLoadingSpinner() {
    $('#viewsLoadingSpinner').hide();
}

function showDatatypesLoadingSpinner() {
    $('#datatypesLoadingSpinner').show();
}

function hideDatatypesLoadingSpinner() {
    $('#datatypesLoadingSpinner').hide();
}

function showFieldsLoadingSpinner() {
    $('#fieldsLoadingSpinner').show();
}

function hideFieldsLoadingSpinner() {
    $('#fieldsLoadingSpinner').hide();
}

function showResultCount() {
    $('.resultCount').show();
}

function hideResultCount() {
    $('.resultCount').hide();
}

function showPrevButton() {
    $('#prev_button').show();
}

function hidePrevButton() {
    $('#prev_button').hide();
}

function showNextButton() {
    $('#next_button').show();
}

function hideNextButton() {
    $('#next_button').hide();
}

function showPageNum() {
    $('#pageNum').show();
}

function hidePageNum() {
    $('#pageNum').hide();
}

function enableButtons() {
    $("#summary_button").prop('disabled', false);
    $("#download_button").prop('disabled', false);
    $("#save_sql_button").prop('disabled', false);
    $("#submit_button").prop('disabled', false);
    $("#cancel_button").prop('disabled', false);
}

function disableDownloadButton() {
    $("#download_button").prop('disabled', true);
    $("#save_sql_button").prop('disabled', true);
}


function processQueryResults(paginationCount, data) { //creates results table
    hideResultsLoadingSpinner();
    $('.searchresheaderh5').text('Preview of Search Results');
    $('#filter_results').html('');
    var qryresults = $("#query_results").empty();
    var tableContainer = $('<div id="container" class="table-container">').appendTo(qryresults);
    var table = $("<table>", {'id':'results_table'}).appendTo(tableContainer);
    var thead = $("<thead>").appendTo(table)
    var header = $("<tr>").appendTo(thead);
    var tbody = $("<tbody>").appendTo(table);
    resultCount = paginationCount;

    try{
        console.log(data);
    }
    catch (e) {
        throw("Query failed " + e);
    }
    pageidx = 0;

    $('.resultCount').empty();
    $('.resultCount').append('<label>' + resultCount + ' record(s) found </label>');
    $('.resultCount').show();

    // Display the page number
    totalPageCount = resultCount / pageSize;
    totalPageCount = (Number.isInteger(totalPageCount)) ? totalPageCount : Math.trunc(totalPageCount) + 1;
    $('#pageNum').empty();
    pageNum = pageidx + 1;
    $('#prev_button').after('<label id="pageNum">'+ '&nbsp;&nbsp; Page ' + pageNum + ' of ' + totalPageCount + '</label>');

    try{
        // jobId = json.jobReference.jobId;
        // jobReferenceId = json.jobReference.jobId;
        if (paginationCount == 0){
          alert("The query returned no results");
          $("#download_button").hide();
          $("#save_sql_button").hide();
        }
        else{
          $("#download_button").show();
          $("#save_sql_button").show();
          if (paginationCount > pageSize){
            showNextButton();
          }
          var timestampIndexArr = [];

          $.each(data, function(i, columnRow) {
            if(i < 1) {
              $.each(columnRow, function(field, val) {
                $("<th>").text(field).on('click', {col: field}, sortCol).appendTo(header);
              });
            }
            // if(field.type=="TIMESTAMP"){ //retrieved all field index that has type as timestamp
            //   timestampIndexArr.push(i);
            // }
          });
          // console.log("starting to parse results table for json.rows: ", json.rows);
          $.each(data, function(i, rowData) {
            var row = $("<tr>").appendTo(tbody);
            $.each(rowData, function(i, field) {
                //if row data index is timestamp, convert it to a readable date before displaying
                // if($.inArray(i,timestampIndexArr) != -1){
                //   var dateFormatter = moment.utc(field.v*1000); //UTC format
                //   var toUTC = dateFormatter.format("YYYY-MM-DD hh:mm:ss");
                //    $("<td>").text(toUTC).appendTo(row);
                // }else{
                  $("<td>").text(field).appendTo(row);
                // }
            });
          });
        }
        console.log("finished parsing results table");
        console.log("calling showResultCount...")
        showResultCount();
        if (paginationCount > pageSize) {
            //showPrevButton();
            showNextButton();
        }
        showPageNum();
        console.log("calling showResultsTable...");
        showResultsTable();
        $('.hr2').show();
        $('.searchresheader').show();
        $("table").stickyTableHeaders({container: "#container"});
        if (pageNum == 1) hidePrevButton();
    }
    catch (e){
        throw(e);
        alert("ERROR: malformed query.");
        $('#download_button').hide();
        $("#submit_button").text('Search');
    }
    hideResultsLoadingSpinner();
    $("#submit_button").text('Search');
    enableButtons();
    document.getElementById('disablingDiv').style.display='none';
}

function processSummaryQueryResults(data){ //populates summary table
  var summaryResults = $("#summary_results").empty();
  var table = $("<table>", {'id':'table'}).appendTo(summaryResults);
  var header = $("<tr>").appendTo(table);
  // console.log(data);

  $("<th>").text("Field ").appendTo(header);
  $("<th>").text("Count ").appendTo(header);

  $.each(data, function(i, rowData) {
    $.each(rowData, function(i, field) {
      var row = $("<tr>").appendTo(table);
                $("<td>").text(i).appendTo(row);
                $("<td>").text(field).appendTo(row);
    });
  });
  hideResultsLoadingSpinner();
  document.getElementById('disablingDiv').style.display='none';
  showSummaryTable();
  $("#summary_button").text('View Summary');
  $('.searchresheaderh5').text('Preview of Summary Results');
  enableButtons();
}

function createCountQuery(data){ //for summary

    var fieldList = [];
    $.each(data,function(i, field){
      if(i < 1) {
        $.each(field, function(k, v) {
          fieldList.push(k);
        });
      }
    });


    var countQuery = "SELECT ";
    if (fieldList.length > 1){
      for (var i=0; i<fieldList.length-1; i++){
        countQuery = countQuery + "Count(DISTINCT " + fieldList[i] + ") as " + fieldList[i] + ", ";
      }
      countQuery =countQuery + "Count(DISTINCT " + fieldList[i] + ") as " + fieldList[i] + " ";
      countQuery = countQuery + "FROM (";
    }else if (fieldList.length ==1 ) {
      countQuery = countQuery + "Count(DISTINCT " + fieldList[0] + ") as " + fieldList[0] + " ";
      countQuery = countQuery + "FROM (";
    }
    countQuery = countQuery +"SELECT * FROM  merck_dev_temp." + cognitoUser +")";


    let bd = {
      statement: countQuery,
      organization: org,
      email: emailId
    }
    let stmntStr = JSON.stringify(bd);
    $.ajax({
        type: 'POST',
        url: 'https://dl70eg51ke.execute-api.us-east-2.amazonaws.com/dev',
        data: stmntStr,
        crossDomain: true,
        contentType: 'application/json',
        beforeSend: function (xhr) {
           xhr.setRequestHeader('Authorization', authorizer);
        },
        success: function(data, status) {
          processSummaryQueryResults(data);
        }, error: function(err) {
          console.log(err);
        }
    });
}
