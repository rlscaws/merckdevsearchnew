let accessToken;
let idToken;
let refreshToken;

function validateForm(formName, nextFunc) {
    let forms = document.getElementsByClassName(formName);
    let validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
      if(form.checkValidity() === true) {
        event.preventDefault();
        if(nextFunc === "login") {
          onLogin();
        } else if(nextFunc === "register") {
          onRegister();
        } else if(nextFunc === "confirm") {
          onConfirmRegistration();
        } else if(nextFunc === "forgot") {
          forgotPassword();
        } else if(nextFunc === 'verify') {
          confirmNewPassword();
        }
      }
    });
}

function onLogin() {

  $('#signupErrorMsg').hide();
  $("#signupSubmit").prop('disabled', true);
  $('#signupSubmit').text('Authenticating...');

  let emailId = $('#signupEmail').val().trim();
  let address = emailId.split('@');
  let org = address[1].split('.');

  let body = {
    email: emailId,
    password: $('#signupPassword').val().trim(),
    organization: org[0]
  };

  let stmntStr = JSON.stringify(body);
  $.ajax({
      type: 'POST',
      url: 'https://2a7ilmiaij.execute-api.us-east-2.amazonaws.com/dev/login',
      data: stmntStr,
      crossDomain: true,
      contentType: 'application/json',
      success: function(data, status) {
        if(data.message) {
          $('#signupSubmit').text('Login');
          $("#signupSubmit").prop('disabled', false);
          $('#signupErrorMsg').show();
          $('#signupErrorMsg').text(data.message);
        } else {
          accessToken = data.AuthenticationResult.AccessToken;
          idToken = data.AuthenticationResult.IdToken;
          refreshToken = data.AuthenticationResult.RefreshToken;
          userLookup(emailId, accessToken, idToken);
        }
      }, error: function(err) {
        console.log(err);
        $("#signupSubmit").prop('disabled', false);
        $('#signupErrorMsg').show();
        $('#signupErrorMsg').text('Please check your username and password or click on the Register link');
      }
    });
}

function userLookup(emailId, accessToken, idToken) {
  let address = emailId.split('@');
  let org = address[1].split('.');
  let body = {
    email: emailId,
    token: accessToken,
    id_token: idToken,
    user_org: org[0]
  };
  let stmntStr = JSON.stringify(body);
  $.ajax({
      type: 'POST',
      url: 'https://tyh2nkmxel.execute-api.us-east-2.amazonaws.com/dev',
      data: stmntStr,
      crossDomain: true,
      contentType: 'application/json',
      beforeSend: function (xhr) {
         xhr.setRequestHeader('Authorization', idToken);
      },
      success: function(res, status) {
        writeToDynamo(res);
      }, error: function(err) {
        console.log(err);
      }
    });
}

function writeToDynamo(res) {
  let body = res;
  let stmntStr = JSON.stringify(body);
  $.ajax({
      type: 'POST',
      url: 'https://4sdwnjwjh6.execute-api.us-east-2.amazonaws.com/dev',
      data: stmntStr,
      crossDomain: true,
      contentType: 'application/json',
      beforeSend: function (xhr) {
         xhr.setRequestHeader('Authorization', idToken);
      },
      success: function(data, status) {
        if(data.lookup_Id) {
          localStorage.setItem("lookup", data.lookup_Id);
          setTimeout(function() {
            window.location.href = "/search/index.html";
          }, 1250);
        }
      }, error: function(err) {
        console.log(err);
      }
    });
}

function onRegister() {

  $("#registrationSubmit").prop('disabled', true);
  $('#registrationSubmit').text('Creating account...');
  let body = {
    email: $('#signupEmail').val().trim(),
    password: $('#signupPassword').val().trim(),
    nickname: $('#signupName').val().trim(),
    phone: $('#signupPhone').val().trim()
  };

  let stmntStr = JSON.stringify(body);
  $.ajax({
      type: 'POST',
      url: 'https://2a7ilmiaij.execute-api.us-east-2.amazonaws.com/dev/registration',
      data: stmntStr,
      crossDomain: true,
      contentType: 'application/json',
      success: function(data, status) {
        if(data.message) {
          $('#registrationSubmit').text('Create your account');
          $("#registrationSubmit").prop('disabled', false);
          $('#registrationErrorMsg').show();
          $('#registrationErrorMsg').text(data.message);
        } else {
            window.location.href = "confirmRegistration.html";
        }
      }, error: function(err) {
        console.log(err);
        $('#registrationErrorMsg').show();
        $('#registrationErrorMsg').text(err.message);
      }
    });
}

function onConfirmRegistration() {

  $("#confirmRegistration").prop('disabled', true);
  $('#confirmRegistration').text('Checking confirmation code...');

  let body = {
    email: $('#email').val().trim(),
    confirmation_code: $('#confirmationCode').val().trim()
  };

  let stmntStr = JSON.stringify(body);
  $.ajax({
      type: 'POST',
      url: 'https://2a7ilmiaij.execute-api.us-east-2.amazonaws.com/dev/registrationconfirmation',
      data: stmntStr,
      crossDomain: true,
      contentType: 'application/json',
      success: function(data, status) {
        if(data.message) {
          $('#confirmRegistration').text('Confirm');
          $("#confirmRegistration").prop('disabled', false);
          $('#confirmRegErrorMsg').show();
          $('#confirmRegErrorMsg').text(data.message);
        } else {
            window.location.href = "login.html";
        }
      }, error: function(err) {
        console.log(err);
        $('#confirmRegErrorMsg').show();
        $('#confirmRegErrorMsg').text(err.message);
      }
  });
}

function forgotPassword() {

  $("#forgotPasswordSubmit").prop('disabled', true);
  $('#forgotPasswordSubmit').text('Requesting confirmation code...');

  let body = {
    email: $('#signupEmail').val().trim()
  };

  let stmntStr = JSON.stringify(body);
  $.ajax({
      type: 'POST',
      url: 'https://2a7ilmiaij.execute-api.us-east-2.amazonaws.com/dev/forgotpassword',
      data: stmntStr,
      crossDomain: true,
      contentType: 'application/json',
      success: function(data, status) {
        if(data.message) {
          $('#forgotPasswordSubmit').text('Send Confirmation Code');
          $("#forgotPasswordSubmit").prop('disabled', false);
          $('#forgotPassErrorMsg').show();
          $('#forgotPassErrorMsg').text("Please re-enter the values.");
        } else {
          window.location.href = "verifyPassword.html";
        }
      }, error: function(err) {
        console.log(err);
        $('#forgotPassErrorMsg').show();
        $('#forgotPassErrorMsg').text("Please re-enter the values.");
      }
  });
}

function confirmNewPassword() {

  $("#verifyPasswordSubmit").prop('disabled', true);
  $('#verifyPasswordSubmit').text('Confirming password reset...');

  let body = {
    email: $('#signupEmail').val().trim(),
    password: $('#newPassword').val().trim(),
    confirmation_code: $('#verificationCode').val().trim()
  };

  let stmntStr = JSON.stringify(body);
  $.ajax({
      type: 'POST',
      url: 'https://2a7ilmiaij.execute-api.us-east-2.amazonaws.com/dev/forgotpasswordconfirmation',
      data: stmntStr,
      crossDomain: true,
      contentType: 'application/json',
      success: function(data, status) {
        if(data.message) {
          $('#verifyPasswordSubmit').text('Reset Password');
          $("#verifyPasswordSubmit").prop('disabled', false);
          $('#verifyPasswordErrorMsg').show();
          $('#verifyPasswordErrorMsg').text("Please re-enter the values.");
        } else {
          window.location.href = "login.html";
        }
      }, error: function(err) {
        console.log(err);
        $('#verifyPasswordErrorMsg').show();
        $('#verifyPasswordErrorMsg').text("Please re-enter the values.");
      }
  });
}

function clearStorage() {
  localStorage.clear();
}
