// this file contains the objects and lists that create the directed graph datastructure that support the assay/program CR
var SelectedProgObjList =  [];
var SelectedAssayObjList = [];
var SelectedDTObjList = [];

// prototypes fora making new objects
function pro(view, pro_name) {
    this.parent_views = view; // string of the view containing this program
    this.pro = pro_name; // this string holds the pro name
    this.child_assays = []; // list of assay_objs for the assays that are part of this program
};

function assay(pros, assay_name) {
    this.parent_pros = pros; // list of pointers/references to programs objects containing this assay
    this.assay = assay_name; // this holds the assay name
    this.child_datatypes = []; // list of pointers/references to datatypes objects that are part of this assay
};

function datatype(assays, datatype_name) {
    this.parent_assays = assays; // list of pointers/references to assays objects containing this assay
    this.datatype = datatype_name;
    this.child_fields = []; // list of fileds (strings) which belong to this dataType
};

function selectProgram(view, program_name){  // when passed the selected string this creates the pro object
    var obj = new pro(view, program_name);
    SelectedProgObjList.push(obj);
}

function deSelectProgram(sel_program){
    // this is a deselection so only cascades forward
    var program = SelectedProgObjList.find(obj => obj.pro === sel_program);

    //remove this program as a parent of its children assays
    console.log('deSelectProgram', program)
    if (program){ // deselecting a program directly
        program.child_assays.forEach( a => {
            SelectedAssayObjList.forEach(obj => {
                if (obj.assay === a){
                    console.log("removing parent for assay", a);
                    obj.parent_pros = obj.parent_pros.filter(pro => pro !== program.pro);
                    if (obj.parent_pros.length < 1){ // remove this assay if there are no more parent programs
                        deSelectAssay(obj.assay);
                    }
                }
            });
        });
    }
    //remove this from the selected object list
    SelectedProgObjList.splice(SelectedProgObjList.indexOf(program),1);
}

function selectAssay(pros, assay_name){  // when passed the selected string this creates the assay object
    console.log("selectAssay",pros,assay_name);
    var obj = new assay(pros, assay_name);
    SelectedAssayObjList.push(obj);
    console.log(SelectedAssayObjList)
}

function deSelectAssay(sel_assay){
    // this is a deselection so only cascades forward
    var assay = SelectedAssayObjList.find(obj => obj.assay === sel_assay);
    console.log(assay);
    if(assay){ // remove the selected datatypes
        //remove this assay as a parent of its children datatypes
        assay.child_datatypes.forEach( d => {
            SelectedDTObjList.forEach(obj => {
                if (obj.datatype === d){
                    obj.parent_assays = obj.parent_assays.filter(a => a !== assay.assay);
                    if (obj.parent_assays.length < 1){ // remove this assay if there are no more parent assays
                        deSelectDT(obj.datatype);

                    }
                }
            });
        });
        //remove this from the selected object list
        SelectedAssayObjList.splice(SelectedAssayObjList.indexOf(assay),1);
        getSelectedAssays(sel_assay, false, $("#selectedassaysDiv"), false, false);
        populateDataTypesSubCategories(selected_datamartView,"","");
    }
}

function selectDT(assays, datatype_name){  // when passed the selected string this creates the assay object
    console.log("selectDT",assays,datatype_name);
    var obj = new datatype(assays, datatype_name);
    SelectedDTObjList.push(obj);
    console.log(SelectedDTObjList)
}

function deSelectDT(sel_datatype){
    // this is a deselection so only cascades forward
    var datatype = SelectedDTObjList.find(obj => obj.datatype === sel_datatype);
    console.log(datatype);
    if (datatype){
        datatype.child_fields.forEach(f =>{ //remove the child fields from all the apropriate globals
            col = col.filter(c => c !== f); pCol = pCol.filter(c => c !== f);
        });
        SelectedDTObjList.splice(SelectedDTObjList.indexOf(datatype),1);
        getSelectedDataTypes(sel_datatype, false, $("#selectedDTDiv"), false, false);
        populateFieldsResults(selected_datatypes, "");
    }

}
