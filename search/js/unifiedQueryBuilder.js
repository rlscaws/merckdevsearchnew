//Globals  which this function has dependency on to add
var vPriority = ["DLCsum", "DLCevent", "invivoPKPD",  "ChemProp","MetID", "PPDM","InvitroPharmSum","InvitroPharm"]; // this is a rough priority for the order in which to join the views
var views = ["DLCsum", "DLCevent", "invivoPKPD",  "ChemProp","MetID", "PPDM","InvitroPharmSum","InvitroPharm"]; // starts assuming needs all views
var joiners_dict = { // defines all columns required for joining or getting expected pivoting output
    "DLCsum": ["compound_id", "parent_id", "spvs_id"],
    "DLCevent": ["compound_id", "parent_id", "sample_id","result_id"], //,"spvs_id"],
    "MetID": ["parent_id_MetID"],
    "ChemProp": ["compound_id", "sample_id", "parent_id"],
    "invivoPKPD":  ["compound_id", "sample_id", "parent_id"],
    "InvitroPharm": ["compound_id"],
    "InvitroPharmSum": ["compound_id"],
    "PPDM": ["compound_id", "sample_id", "parent_id"]

}


var joinOn_dict = {
    "DLCevent":{
        "DLCsum": "DLCevent.parent_id=DLCsum.parent_id AND DLCevent.compound_id = DLCsum.compound_id",
        "MetID": "DLCevent.parent_id = MetID.parent_id_MetID AND SUBSTR(DLCevent.compound_id,1,11) = parent_id AND SUBSTR(DLCevent.sample_id,1,11) = parent_id",
        "ChemProp": "DLCevent.parent_id=ChemProp.parent_id AND DLCevent.compound_id=ChemProp.compound_id AND DLCevent.sample_id = ChemProp.sample_id",
        "invivoPKPD": "DLCevent.parent_id=invivoPKPD.parent_id AND DLCevent.compound_id=invivoPKPD.compound_id AND ((DLCevent.sample_id = invivoPKPD.sample_id) OR (DLCevent.sample_id = invivoPKPD.pk_analyte))",
        "PPDM": "DLCevent.parent_id=PPDM.parent_id AND DLCevent.compound_id = PPDM.compound_id AND DLCevent.sample_id = PPDM.sample_id"
    },
    "DLCsum":{
        "DLCevent": "DLCevent.parent_id=DLCsum.parent_id AND DLCevent.compound_id = DLCsum.compound_id",
        "MetID": "DLCsum.parent_id = MetID.parent_id_MetID AND SUBSTR(DLCsum.compound_id,1,11) = MetID.parent_id_MetID",
        "ChemProp": "DLCsum.parent_id=ChemProp.parent_id AND DLCsum.compound_id=ChemProp.compound_id",
        "invivoPKPD": "DLCsum.parent_id=invivoPKPD.parent_id AND DLCsum.compound_id=invivoPKPD.compound_id",
        "PPDM": "DLCsum.parent_id=PPDM.parent_id AND DLCevent.compound_id = PPDM.compound_id"
    },
    "MetID":{
        "DLCevent": "DLCevent.parent_id = MetID.parent_id_MetID AND SUBSTR(DLCevent.compound_id,1,11) = parent_id AND SUBSTR(DLCevent.sample_id,1,11) = parent_id",
        "DLCsum": "DLCsum.parent_id = MetID.parent_id_MetID AND SUBSTR(DLCsum.compound_id,1,11) = MetID.parent_id_MetID",
        "ChemProp": "MetID.parent_id_MetID=ChemProp.parent_id AND SUBSTR(ChemProp.compound_id,1,11)=MetID.parent_id_MetID",
        "invivoPKPD": "MetID.parent_id_MetID=invivoPKPD.parent_id AND MetID.parent_id_MetID=SUBSTR(invivoPKPD.compound_id,1,11) AND ((MetID.parent_id_MetID = SUBSTR(invivoPKPD.sample_id,1,11)) OR (MetID.parent_id_MetID = SUBSTR(invivoPKPD.pk_analyte,1,11)))",
        "PPDM": "MetID.parent_id_MetID=PPDM.parent_id AND MetID.parent_id_MetID=SUBSTR(PPDM.compound_id,1,11) AND MetID.parent_id_MetID = SUBSTR(PPDM.sample_id,1,11)"
    },
    "ChemProp":{
        "DLCevent": "DLCevent.parent_id=ChemProp.parent_id AND DLCevent.compound_id=ChemProp.compound_id AND DLCevent.sample_id = ChemProp.sample_id",
        "DLCsum": "DLCsum.parent_id=ChemProp.parent_id AND DLCsum.compound_id=ChemProp.compound_id",
        "MetID": "MetID.parent_id_MetID=ChemProp.parent_id AND SUBSTR(ChemProp.compound_id,1,11)=MetID.parent_id_MetID",
        "invivoPKPD": "ChemProp.parent_id=invivoPKPD.parent_id AND ChemProp.compound_id=invivoPKPD.compound_id AND ((ChemProp.sample_id = invivoPKPD.sample_id) OR (ChemProp.sample_id = invivoPKPD.pk_analyte))",
        "PPDM": "ChemProp.parent_id=PPDM.parent_id AND ChemProp.compound_id = PPDM.compound_id AND ChemProp.sample_id = PPDM.sample_id"
    },
    "invivoPKPD":{
        "DLCevent": "DLCevent.parent_id=invivoPKPD.parent_id AND DLCevent.compound_id=invivoPKPD.compound_id AND ((DLCevent.sample_id = invivoPKPD.sample_id) OR (DLCevent.sample_id = invivoPKPD.pk_analyte))",
        "DLCsum": "DLCsum.parent_id=invivoPKPD.parent_id AND DLCsum.compound_id=invivoPKPD.compound_id",
        "MetID": "MetID.parent_id_MetID=invivoPKPD.parent_id AND MetID.parent_id_MetID=SUBSTR(invivoPKPD.compound_id,1,11) AND ((MetID.parent_id_MetID = SUBSTR(invivoPKPD.sample_id,1,11)) OR (MetID.parent_id_MetID = SUBSTR(invivoPKPD.pk_analyte,1,11)))",
        "ChemProp": "ChemProp.parent_id=invivoPKPD.parent_id AND ChemProp.compound_id=invivoPKPD.compound_id AND ((ChemProp.sample_id = invivoPKPD.sample_id) OR (ChemProp.sample_id = invivoPKPD.pk_analyte))",
        "PPDM": "PPDM.parent_id=invivoPKPD.parent_id AND PPDM.compound_id=invivoPKPD.compound_id AND ((PPDM.sample_id = invivoPKPD.sample_id) OR (PPDM.sample_id = invivoPKPD.pk_analyte))"

    },
    "PPDM":{
        "DLCevent": "DLCevent.parent_id=PPDM.parent_id AND DLCevent.compound_id = PPDM.compound_id AND DLCevent.sample_id = PPDM.sample_id",
        "DLCsum": "DLCsum.parent_id=PPDM.parent_id AND DLCevent.compound_id = PPDM.compound_id",
        "MetID": "MetID.parent_id_MetID=PPDM.parent_id AND MetID.parent_id_MetID=SUBSTR(PPDM.compound_id,1,11) AND MetID.parent_id_MetID = SUBSTR(PPDM.sample_id,1,11)",
        "ChemProp": "ChemProp.parent_id=PPDM.parent_id AND ChemProp.compound_id = PPDM.compound_id AND ChemProp.sample_id = PPDM.sample_id",
        "invivoPKPD": "PPDM.parent_id=invivoPKPD.parent_id AND PPDM.compound_id=invivoPKPD.compound_id AND ((PPDM.sample_id = invivoPKPD.sample_id) OR (PPDM.sample_id = invivoPKPD.pk_analyte))",

    },
    "InvitroPharm":{
        "DLCevent": "DLCevent.parent_id=PPDM.parent_id",
        "DLCsum": "DLCsum.parent_id=PPDM.parent_id",
        "MetID": "MetID.parent_id_MetID=PPDM.parent_id",
        "ChemProp": "ChemProp.parent_id=PPDM.parent_id",
        "invivoPKPD": "PPDM.parent_id=invivoPKPD.parent_id",
        "InvitroPharmSum": "InvitroPharm.parent_id=InvitropharmSum.parent_id"
    }
}


function writeSubQuery(view){ // writes the sub-query for a single view
    q = writeQuery(view,false);
    return(q);
}

function getSourceColumn(c){ //returns a source column name given a display column nam
    var list = c.toString().split("_");
    var suffix = list.pop(); // remove suffix from field name indicating source view
    if (suffix === "id"){
        return(c);
    }
    if (suffix === "MetID"){
      return(c);
    }
    else{
        return(list.join("_"));
    }
}

function writeTempQuery(fv){ // writes temp query for generating initial compound list
    // var firstTable = fv; //Identify first table form tPriority (GLOBAL)
    var fq = "WITH temp AS (";
    fq = fq + writeQuery(fv,true);
    fq = fq + ") ";
    return(fq);
}


function getJoin(a,b){ // gets the join criteria based on Nilam's matrix
    console.log("getJoin", a, b)
    // var joinOn = " ON ";
    // joinOn = joinOn + joinOn_dict[a][b];
    if(b.indexOf("InvitroPharm") == 0){
        return(getJoin(b,a));
    }
    else{
        if(a.indexOf("InvitroPharm") < 0){
            var joinOn = " ON " + a + ".parent_id = " + b +".parent_id ";
        }
        else{
            var joinOn = " ON SUBSTR(" + a + ".compound_id,1,11) = " + b +".parent_id ";
        }
    return(joinOn);
    }
}

function joinQueries(views){ //a function for joining the views needed for correlate data
    // // console.log('joinQueries(', views);
    var q = "";
    for (var a = 0; a < views.length; a = a + 2) { // loop over views
        var b = a + 1; // the next view
        var joinOn = " ";
        if (a === 0 ){ // this uses the temp table created in the temp query for the first view
            q = q + " temp ";
        }
        else{
            q = q + "("
            q = q + writeSubQuery(views[a]); //write subquery for views[a]
        }
        if (a > 0 && a < views.length - 1) { // a is somewhere in the middle
            joinOn = getJoin(views[a-1],views[a]);
            q = q + ") AS " + views[a] + joinOn + " JOIN (";
            q = q + writeSubQuery(views[b]); + ") AS " + views[b];

        }
        else if(a == 0){ // a is first view
            // console.log("joining the first and second view")
            if (views.length === 1){ // bad use case for correlate data .. should already have been redirected to single view
                q = q + ")";
                alert("WARNING: You have selected only fields from a single datamart/view for your current search with Correlate Data.  Please restart your field selection process by using a specific category of interest.  Correlate Data category is intended for traversing multiple datamarts/views.")
            }
            else{ // a is not the only needed view to complete this query
                q = q + " AS " + views[a] + " JOIN (";
                q = q + writeSubQuery(views[b]); + ") AS " + views[b];
            }
        }
        else{ // a is last view (odd # of views)
            joinOn = getJoin(views[a-1],views[a]);
            q = q + ") AS " + views[a] + joinOn;
        }


        if (a < views.length - 1) { // a is not the last view (even # of views)
            joinOn = getJoin(views[a],views[b]);
            q = q + ") AS " + views[b] + " " + joinOn + " ";
        }

        if(b < views.length-1){ // b is not the last view (odd)
            q = q + " JOIN ";
        }
    }
    return(q);
}

function writeUnifiedQuery(view) {
    var views_needed = []; //base on selected fields NOT selected datamarts / views
        for (var i = 0; i < selected_fields.length; i++){
            vi = selected_fields[i].toString().split("_").pop();
            if (vi === "CalcChemProp" || vi === "MeasChemProp"){
                vi = "ChemProp";
            }
            for (var j = 0; j < vPriority.length; j++){
                if (vPriority[j] == vi){
                    views_needed.push(vi)
                }
            }
        }
        // at this point views_needed will have duplicates so lets filter on unique values
        function onlyUnique(value, index, self) {
            return self.indexOf(value) === index;
        }
        views_needed = views_needed.filter( onlyUnique );

        //order the views based on vPriority
        var views = [];
        for (var i = 0; i < vPriority.length; i++){
            for(var j = 0; j < views_needed.length; j++){
                if (vPriority[i] == views_needed[j]){
                    views.push(views_needed[j]);
                }
            }
        }
    // alter sorting of views based on # of filters for each view
    var view_filter_histogram = {}; // histogram object

    for (var i = 0; i < views.length; i++){ //initialize histogram object
        view_filter_histogram[views[i]] = 0;
    }

    for(var k = 0; k < views.length; k++){
        readFilters(views[k]);

        for (var n=0; n < filtersList.length; n++){
                var f = filtersList[n].trim();
                var lf = f.split(" ");
                var fi = 0; //index of fieldname
                if (lf[fi] === "AND"|| lf[fi] === "OR"){ // 0th  if first filter
                    fi++;
                };

                var field_name = lf[fi];
                var v = field_name.split("_").pop();
                view_filter_histogram[v] = view_filter_histogram[v] + 1; //increment counter in histogram object for this view
        }
    }
    // get the view with the greatest number of filters (ties broken by vPriority)
    var max = 0;
    var max_view = "";
    Object.keys(view_filter_histogram).forEach(function(key) { //get the arg max of the histogram object
        if (view_filter_histogram[key] > max){
            max = view_filter_histogram;
            max_view = key;
        }
    });

    var fv = max_view; // first view used for temp query is the one with the most filters applied to it

    views.splice(views.indexOf(fv),1) //make sure first view first
    views = [fv].concat(views);
    if(views_needed.length === 1 || selected_datamartView.length === 1){
        alert("WARNING: The entered search criteria only requires one view, redirecting to single view search...");
        selected_category = views[0]; // only affects download name
        q = writeQuery(views[0],false);
    }
    else{
        var q = writeTempQuery(fv); // write temp query
        // select all the necessary fields
        q = q + " SELECT DISTINCT ";
        //list ALL fields for display from ALL views
        for (var i = 0; i < selected_fields.length; i++){
            q = q + selected_fields[i].toString().replace(/[-|/]/g,"_").replace(/[%]/g,"percent") + ", ";
        }
        q = q.substring(0,q.length-2); //remove trailing comma
        // from the join of all the necessary views
        q = q + " FROM (";
        q = q + joinQueries(views);
        q = q + ")";
        console.log("wrote unified query: \n", q);
    }
        return(q);
}
