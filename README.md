# Development Instructions for SIMON Search Web User Interface

A guide to creating a local develop environment for the SIMON search Web UI and deploying on Google App Engine. Note that this repository has two app engine services.

0. The `bq_searchPage` directory contains the source code for the app engine service for the Web app.

1. The `exportCSV` directory contains the source code for the app engine service that supports downloading of results from BQ.

## Installing Pre-requisites

0. (If on windows machine) Install [Git Bash](https://git-scm.com/download/win)

1. Install [python2.7/pip](https://www.python.org/download/releases/2.7.2/)

2. Install the [Google Cloud SDK](https://cloud.google.com/sdk/downloads)

3. Install [Google Chrome](https://www.google.com/chrome/) (this web app only supports Chrome)

4. Install [Atom](https://atom.io) text editor (optional)

5. Install [atom-live-server](https://atom.io/packages/atom-live-server) package (optional)

## Cloning this project
To get project source code on your local filesystem, from your shell run:

```sh
 mkdir merckdevsearch && cd merckdevsearch
 git clone https://<your-bitbucket-username>@bitbucket.org/rlsc/merckdevsearch.git
```

## Running Local Test Environment for the Web App (using the atom-live-server package)

0. Launch Atom

1. File > Add Project Folder > choose the directory in which you cloned the source code.

2. Packages > atom-live-server > Start server. This will automatically open a tab http://127.0.0.1:3000/. However you should navigate to http://localhost:3000 for testing due to a known issue with authenticating with the google api when locally serving from 127.0.0.1:3000.

3. Click the bq_searchPage Folder icon and you will be brought to the locally hosted SIMON search Web Application.

4. After saving any code changes on your local filesystem the Web App will automatically refresh and load to reflect your changes.

5. For Debugginng in Chrome use the developer console by clicking on the 3 vertically stacked dots in the upper right-hand corner > More tools > Developer tools. If you have never done javascript development with chrome before I highly  recommend reviewing this [video](https://developers.google.com/web/tools/chrome-devtools/javascript/) and/or the text [documentation page](https://developers.google.com/web/tools/chrome-devtools/)

## Running a local test environment for the exportCSV `ws`

0. As is [best practice](https://cloud.google.com/appengine/docs/standard/python/getting-started/python-standard-env) install the necessary libraries:

```sh
pip install -t lib -r requirements.txt
```

1. Go to the `exportCSV/webservice` directory and execute this command:

```sh
cd ~/merckdevsearch/exportCSV/webservice
gunicorn -b :8080 main:app
```
## Deploying the Web App on Google App Engine

0. set the `service` parameter in the `app.yaml`. Note that this will change the URL to: https://`service-name`-dot-rising-field-149920.appspot.com/ . If you wish to deploy to the default URL: https://rising-field-149920.appspot.com/ then comment out the `service` parameter line in `app.yaml`

1. In your shell navigate to the directory containing the `app.yaml` and invoke the Google Cloud SDK `app deploy` method:

```sh
cd ~/merckdevsearch
gcloud app deploy
```

## Deploying the Download service on Google App engine

0. set the `service` parameter to `ws-dev` in the download service  `app.yaml`. Note that this will change the URL for the web service to: https://ws-dev-dot-rising-field-149920.appspot.com/

1. Execute this command inside the  `exportCSV/webservice` directory.

```sh
gcloud app deploy --project rising-field-149920 -v export-csv --no-promote
```
## Troubleshooting

0. If you are deploying the web app to a new URL (local or public) and the authentication pop-up is broken, ensure that the URL is added as an Authorized JavaScript Origin under RLSC Web Client 1 in the GCP Console under the [Credentials section of APIs & Services](https://console.cloud.google.com/apis/credentials?project=rising-field-149920).


## Questions
For questions about this application and its development process contact <jacob.ferriero@accenture.com>
