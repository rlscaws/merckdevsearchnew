--before running this query delete the current subcategory info for these views
--delete from sub_categories6 where subcategory in ("Measured Physiochemical Properties", "Calculated Physiochemical Properties","Physiochemical Properties", "In Vitro Pharmacology Assay Individual Results", "In Vitro Pharmacology Assay Summary Results")
--standard SQL
--destination table: merck_dev.sub_categories6 append

SELECT DISTINCT * FROM(
(with dlcsumbase as ( -- these are the base columns for pivoting
    SELECT "AVG" as abr,"AVERAGE_VALUE" as field_name, "FLOAT" as datatype UNION ALL
    SELECT "AVG_QUAL" as abr,"AVERAGE_VALUE_QUALIFIER" as field_name, "STRING" as datatype UNION ALL
    SELECT "AVG_UNIT" as abr,"AVERAGE_VALUE_UNIT" as field_name, "STRING" as datatype UNION ALL
    SELECT "DEV" as abr,"standard_deviation" as field_name, "FLOAT" as datatype UNION ALL
    SELECT "ERR" as abr,"standard_error" as field_name, "FLOAT" as datatype UNION ALL
    SELECT "GED" as abr,"global_event_date" as field_name, "TIMESTAMP" as datatype UNION ALL
    SELECT "GEO" as abr,"geometric_average_value" as field_name, "FLOAT" as datatype UNION ALL
    SELECT "GEO_QUAL" as abr,"geometric_average_qualifier" as field_name, "STRING" as datatype UNION ALL
    SELECT "GSE" as abr,"geometric_standard_error" as field_name, "FLOAT" as datatype UNION ALL
    SELECT "ICT" as abr,"included_count" as field_name, "FLOAT" as datatype UNION ALL
    SELECT "LVD" as abr,"latest_value_date" as field_name, "TIMESTAMP" as datatype UNION ALL
    SELECT "MAX" as abr,"maximum_value" as field_name, "FLOAT" as datatype UNION ALL
    SELECT "MAX_QUAL" as abr,"maximum_value_qualifier" as field_name, "STRING" as datatype UNION ALL
    SELECT "MED_RES_QUAL" as abr,"median_response_qualifier" as field_name, "STRING" as datatype UNION ALL
    SELECT "MIN" as abr,"minimum_value" as field_name, "FLOAT" as datatype UNION ALL
    SELECT "MIN_QUAL" as abr,"minimum_value_qualifier" as field_name, "STRING" as datatype UNION ALL
    SELECT "MND" as abr,"min_dose" as field_name, "FLOAT" as datatype UNION ALL
    SELECT "MRD" as abr,"median_result_dose" as field_name, "FLOAT" as datatype UNION ALL
    SELECT "MRD_UNIT" as abr,"median_result_dose_unit" as field_name, "STRING" as datatype UNION ALL
    SELECT "MRR" as abr,"median_result_response" as field_name, "FLOAT" as datatype UNION ALL
    SELECT "MXD" as abr,"max_dose" as field_name, "FLOAT" as datatype UNION ALL
    SELECT "RND" as abr,"avg_response_min_dose" as field_name, "FLOAT" as datatype UNION ALL
    SELECT "RND_QUAL" as abr,"avg_res_min_dose_qualifier" as field_name, "STRING" as datatype UNION ALL
    SELECT "RNS" as abr,"avg_res_min_standard_deviation" as field_name, "FLOAT" as datatype UNION ALL
    SELECT "RNT" as abr,"avg_res_min_included_count" as field_name, "FLOAT" as datatype UNION ALL
    SELECT "RXD" as abr,"avg_response_max_dose" as field_name, "FLOAT" as datatype UNION ALL
    SELECT "RXD_QUAL" as abr,"avg_res_max_dose_qualifier" as field_name, "STRING" as datatype UNION ALL
    SELECT "RXS" as abr,"avg_res_max_standard_deviation" as field_name, "FLOAT" as datatype UNION ALL
    SELECT "RXT" as abr,"avg_res_max_included_count" as field_name, "FLOAT" as datatype UNION ALL
    SELECT "VAR" as abr,"variance_value" as field_name, "FLOAT" as datatype UNION ALL
    SELECT "VCT" as abr,"value_count" as field_name, "FLOAT" as datatype
)
-- result_type_name pivot
SELECT subcategory, result_value_type_name, field_name, is_pivoted, datatype
from(
    select distinct
    CONCAT(result_type_name,"_DLCsum") as subcategory,
    result_value_type_name,
    CONCAT(
        --add leading _ if first character is number, also replace "_" with "|" and " " with ||
        IF(SUBSTR(result_type_name,0,1) in ("0","1","2","3","4","5","6","7","8","9") ,concat("_",REPLACE(REPLACE(result_type_name," ","||"),"_","|")),REPLACE(REPLACE(result_type_name," ","||"),"_","|")),
        "_",
        REPLACE(REPLACE(IF(result_value_type_name="VALUE","",result_value_type_name)," ","||"),"_","|"),
        "_",
        abr,
        "_DLCsum"
    ) as field_name,
    true as is_pivoted,
    datatype from
    (
    SELECT DISTINCT result_type_name, result_value_type_name, field_name as base_field_name, abr, datatype from
    (SELECT DISTINCT result_type_name, result_value_type_name from `rising-field-149920.merck_dev.v_dlc_sum_result_cmpd`
    WHERE
    result_type_name is not null)
    cross join
    dlcsumbase
    )
)
)
union all
--variant_type_name pivot DLCsum
(SELECT DISTINCT
  CONCAT(variant_type_name,"_DLCsum") as subcategory,
  "null" as result_value_type_name,
  CONCAT(REPLACE(variant_type_name, "_","|"),"_DLCsum") as field_name,
  true as is_pivoted,
  "STRING" as datatype
  FROM
  `rising-field-149920.merck_dev.v_dlc_sum_result_cmpd`
  where variant_type_name is not null
  )

union all
--variant_type_name pivot DLCevent
(SELECT DISTINCT
  CONCAT(variant_type_name,"_DLCevent") as subcategory,
  "null" as result_value_type_name,
  CONCAT(REPLACE(variant_type_name, "_","|"),"_DLCevent") as field_name,
  true as is_pivoted,
  "STRING" as datatype
  FROM
  `rising-field-149920.merck_dev.v_dlc_test_event_result`
  WHERE variant_type_name is not null
  )

union all
--metadata dlcsumbase
(
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "abbreviation_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "assay_category_description_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "assay_category_id_parent_DLCsum" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "assay_category_name_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "assay_category_short_name_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "assay_category_view_status_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "assay_id_DLCsum" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "assay_id_transfer_from_DLCsum" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "assay_id_transfered_from_DLCsum" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "assay_long_name_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "assay_objective_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "assay_registration_date_DLCsum" as field_name, false as is_pivoted, "TIMESTAMP" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "assay_restricted_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "assay_short_name_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "assay_status_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "assay_view_registered_by_person_id_DLCsum" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "attribute_description_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "compound_id_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "concentration_dependent_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "distinguishing_feature_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "ebr_code_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "file_extension_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "first_result_date_DLCsum" as field_name, false as is_pivoted, "TIMESTAMP" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "inactive_date_DLCsum" as field_name, false as is_pivoted, "TIMESTAMP" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "last_result_date_DLCsum" as field_name, false as is_pivoted, "TIMESTAMP" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "m_code_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "medical_condition_name_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "objective_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "parent_id_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "preferred_species_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "project_end_date_DLCsum" as field_name, false as is_pivoted, "TIMESTAMP" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "project_long_name_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "project_objective_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "project_short_name_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "project_start_date_DLCsum" as field_name, false as is_pivoted, "TIMESTAMP" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "project_status_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "protocol_name_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "protocol_registration_date_DLCsum" as field_name, false as is_pivoted, "TIMESTAMP" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "protocol_type_id_DLCsum" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "protocol_version_DLCsum" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "protocol_version_transfer_from_DLCsum" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "protocol_view_protocol_version_DLCsum" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "protocol_view_restricted_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "public_assay_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "registered_by_person_id_DLCsum" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "research_project_id_DLCsum" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "restricted_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "restricted_use_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "result_data_last_load_date_DLCsum" as field_name, false as is_pivoted, "TIMESTAMP" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "result_type_description_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "result_type_name_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "result_type_status_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "result_value_description_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "result_value_type_name_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "sample_id_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "sample_view_restricted_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "screening_domain_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "selectable_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "spvs_id_DLCsum" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "spvs_name_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "target_description_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "target_name_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "target_short_name_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "target_status_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "technology_id_DLCsum" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "technology_name_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "technology_parent_id_DLCsum" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "technology_short_name_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "technology_status_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "template_assay_DLCsum" as field_name, false as is_pivoted, "TIMESTAMP" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "variant_qualifier_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "variant_type_name_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCsum" as subcategory,"null" as result_value_type_name, "variant_unit_DLCsum" as field_name, false as is_pivoted, "STRING" as datatype

)
union all
-- metadata DLCevent
(
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "abbreviation_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "annotation_text_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "assay_category_description_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "assay_category_id_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "assay_category_id_parent_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "assay_category_name_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "assay_category_short_name_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "assay_category_status_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "assay_id_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "assay_id_transfer_from_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "assay_id_transfered_from_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "assay_long_name_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "assay_objective_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "assay_registration_date_DLCevent" as field_name, false as is_pivoted, "TIMESTAMP" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "assay_restricted_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "assay_short_name_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "assay_status_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "assay_type_id_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "attribute_description_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "compound_id_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "concentration_dependent_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "data_object_type_name_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "distinguishing_feature_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "distribution_batch_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "distribution_set_id_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "ebr_code_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "event_date_DLCevent" as field_name, false as is_pivoted, "TIMESTAMP" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "experiment_description_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "file_extension_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "first_result_date_DLCevent" as field_name, false as is_pivoted, "TIMESTAMP" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "inactive_date_DLCevent" as field_name, false as is_pivoted, "TIMESTAMP" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "investigator_person_id_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "last_result_date_DLCevent" as field_name, false as is_pivoted, "TIMESTAMP" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "load_job_number_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "load_row_number_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "m_code_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "masked_result_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "medical_condition_name_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "mixture_group_number_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "notebook_number_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "notebook_page_reference_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "objective_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "parent_id_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "plate_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "preferred_species_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "produced_by_person_id_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "project_end_date_DLCevent" as field_name, false as is_pivoted, "TIMESTAMP" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "project_long_name_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "project_objective_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "project_short_name_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "project_start_date_DLCevent" as field_name, false as is_pivoted, "TIMESTAMP" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "project_status_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "protocol_name_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "protocol_registration_date_DLCevent" as field_name, false as is_pivoted, "TIMESTAMP" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "protocol_restricted_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "protocol_type_id_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "protocol_type_name_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "protocol_variant_set_id_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "protocol_version_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "protocol_version_transfer_from_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "public_assay_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "published_result_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "qa_reference_group_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "qa_reference_result_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "qa_reference_type_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "registered_by_person_id_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "relative_activity_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "research_project_id_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "research_project_restricted_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "restricted_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "restricted_use_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "result_data_last_load_date_DLCevent" as field_name, false as is_pivoted, "TIMESTAMP" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "result_id_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "result_status_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "result_type_description_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "result_type_name_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "result_value_description_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "result_value_type_name_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "role_name_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "rt_id_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "rvt_id_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "sample_id_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "screening_domain_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "selectable_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "subject_id_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "suspect_data_flag_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "target_description_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "target_name_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "target_short_name_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "target_status_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "technology_id_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "technology_name_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "technology_parent_id_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "technology_short_name_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "technology_status_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "template_assay_DLCevent" as field_name, false as is_pivoted, "TIMESTAMP" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "test_sample_id_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "tested_concentration_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "tested_concentration_units_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "time_change_DLCevent" as field_name, false as is_pivoted, "FLOAT" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "time_units_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "value_fixed_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "variant_qualifier_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "variant_type_name_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "variant_unit_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "variant_value_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "well_x_location_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype UNION ALL
    SELECT "Metadata_DLCevent" as subcategory,"null" as result_value_type_name, "well_y_location_DLCevent" as field_name, false as is_pivoted, "STRING" as datatype
)

union All
(
SELECT subcategory, result_value_type_name, field_name, is_pivoted, datatype
from(
    select distinct
    CONCAT(result_type_name,"_DLCevent") as subcategory,
    result_value_type_name as result_value_type_name,
    CONCAT(
        --add leading _ if first character is number, also replace "_" with "|"
        IF(SUBSTR(result_type_name,0,1) in ("0","1","2","3","4","5","6","7","8","9") ,concat("_",REPLACE(REPLACE(result_type_name," ","||"),"_","|")),REPLACE(REPLACE(result_type_name," ","||"),"_","|")),
        "_",
        REPLACE(REPLACE(IF(result_value_type_name="VALUE","",result_value_type_name)," ","||"),"_","|"),
        "_",
        abr,
        "_DLCevent"
    ) as field_name,
    true as is_pivoted,
    datatype from
    (
    SELECT DISTINCT result_type_name, result_value_type_name, field_name as base_field_name, abr, datatype from
    (SELECT DISTINCT result_type_name, result_value_type_name FROM `rising-field-149920.merck_dev.v_dlc_test_event_result`
    where result_type_name is not null)
    cross join
      ( SELECT "VALUE" as abr, "value" as field_name, "FLOAT" as datatype
        UNION ALL SELECT "VALUE_QUAL" as abr, "value_qualifier" as field_name, "STRING" as datatype
        UNION ALL SELECT "VALUE_UNIT" as abr, "value_unit" as field_name, "STRING" as datatype
      )
    )
)
)
)
WHERE field_name is not null and datatype is not null and is_pivoted is not null
