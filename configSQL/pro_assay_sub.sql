select distinct view, pro, assay, subcategory from (
-- dlc unchanged because already in view
select distinct "DLCsum" as view, IF(project_long_name is not null,CONCAT(project_long_name,"_DLCsum"),"(null)_DLCsum") as pro, IF(assay_long_name is not null ,CONCAT(assay_long_name,"_DLCsum"),"(null)_DLCsum") as assay, "Metadata_DLCsum" as subcategory from `rising-field-149920.merck_dev.v_dlc_sum_result_cmpd`

UNION ALL

select distinct "DLCevent" as view, IF(project_long_name is not null,CONCAT(project_long_name,"_DLCevent"),"(null)_DLCevent") as pro, IF(assay_long_name is not null ,CONCAT(assay_long_name,"_DLCevent"),"(null)_DLCevent") as assay, "Metadata_DLCevent" as subcategory from `rising-field-149920.merck_dev.v_dlc_test_event_result`

UNION ALL

select distinct "DLCsum" as view, IF(project_long_name is not null,CONCAT(project_long_name,"_DLCsum"),"(null)_DLCsum") as pro, IF(assay_long_name is not null ,CONCAT(assay_long_name,"_DLCsum"),"(null)_DLCsum") as assay, CONCAT(variant_type_name,"_DLCsum") as subcategory from `rising-field-149920.merck_dev.v_dlc_sum_result_cmpd`

UNION ALL

select distinct "DLCsum" as view, IF(project_long_name is not null,CONCAT(project_long_name,"_DLCsum"),"(null)_DLCsum") as pro, IF(assay_long_name is not null ,CONCAT(assay_long_name,"_DLCsum"),"(null)_DLCsum") as assay, CONCAT(result_type_name,"_DLCsum") as subcategory from `rising-field-149920.merck_dev.v_dlc_sum_result_cmpd`

UNION ALL

select distinct "DLCevent" as view, IF(project_long_name is not null,CONCAT(project_long_name,"_DLCevent"),"(null)_DLCevent") as pro, IF(assay_long_name is not null ,CONCAT(assay_long_name,"_DLCevent"),"(null)_DLCevent") as assay, CONCAT(variant_type_name,"_DLCevent") as subcategory from `rising-field-149920.merck_dev.v_dlc_test_event_result`

UNION ALL

select distinct "DLCevent" as view, IF(project_long_name is not null,CONCAT(project_long_name,"_DLCevent"),"(null)_DLCevent") as pro, IF(assay_long_name is not null ,CONCAT(assay_long_name,"_DLCevent"),"(null)_DLCevent") as assay, CONCAT(result_type_name,"_DLCevent") as subcategory from `rising-field-149920.merck_dev.v_dlc_test_event_result`

UNION ALL
-- update start here
select distinct "invivoPKPD" as view, IF(program is not null,CONCAT(program ,"_invivoPKPD"),"(null)_invivoPKPD") as pro, IF(procedure_name is not null, CONCAT(procedure_name, "_invivoPKPD"),"(null)_invivoPKPD") as assay,"PK" as subcategory from `rising-field-149920.merck_dev.bioassay_master_results_dw_tbl`

UNION ALL

select distinct "invivoPKPD" as view, IF(program is not null,CONCAT(program ,"_invivoPKPD"),"(null)_invivoPKPD") as pro, IF(procedure_name is not null, CONCAT(procedure_name, "_invivoPKPD"),"(null)_invivoPKPD") as assay,"PD" as subcategory from `rising-field-149920.merck_dev.bioassay_master_results_dw_tbl`

UNION ALL

select distinct "invivoPKPD" as view, IF(program is not null,CONCAT(program ,"_invivoPKPD"),"(null)_invivoPKPD") as pro, IF(procedure_name is not null, CONCAT(procedure_name, "_invivoPKPD"),"(null)_invivoPKPD") as assay,"PKPD" as subcategory from `rising-field-149920.merck_dev.bioassay_master_results_dw_tbl`

UNION ALL

select "MetID" as view, "Metabolite ID" as pro, "Metabolite ID" as assay, "Metabolite ID" as subcategory

UNION ALL

select distinct "PPDM" as view, IF(sample_program_name is not null, Concat(sample_program_name,"_PPDM"),"(null)_PPDM") as pro,  IF(study_type is not null, CONCAT(study_type,"_PPDM"),"(null)_PPDM") as assay, "PPDM Assay Results" as subcategory from `rising-field-149920.merck_dev.mv_v_tier2_ppdm`

UNION ALL

select distinct view as view, pro as pro, "Physiochemical Properties" as assay, "Physiochemical Properties" as subcategory from `rising-field-149920.merck_dev.view_pro` where view = 'ChemProp'

UNION ALL

select distinct view as view, pro as pro, "Physiochemical Properties" as assay, "Measured Physiochemical Properties" as subcategory from `rising-field-149920.merck_dev.view_pro` where view = 'ChemProp'

UNION ALL

select distinct view as view, pro as pro,  "Physiochemical Properties" as assay, "Calculated Physiochemical Properties" as subcategory from `rising-field-149920.merck_dev.view_pro` where view = 'ChemProp'

UNION ALL

select distinct "InvitroPharm" as view, IF(project_short_name is not null, CONCAT(project_short_name,"_InvitroPharm"),"(null)_InvitroPharm") as pro, IF(assay_long_name is not null, CONCAT(assay_long_name,"_InvitroPharm"),"(null)_InvitroPharm") as assay, "In Vitro Pharmacology Assay Individual Results" as subcategory
from (

(SELECT DISTINCT assay_id FROM `rising-field-149920.merck_dev.biodata_assay_c50_detail_tbl`
) t1
join
(SELECT assay_id, assay_long_name FROM `rising-field-149920.merck_dev.dlc_assay_view`) t2
on t1.assay_id = t2.assay_id
join
(SELECT assay_id, project_short_name FROM `rising-field-149920.merck_dev.biodata_project_assay_ct_view`) t3
on t2.assay_id = t3.assay_id
)

UNION ALL

select distinct "InvitroPharmSum" as view, IF(project_short_name is not null, CONCAT(project_short_name,"_InvitroPharmSum"),"(null)_InvitroPharmSum") as pro, IF(assay_long_name is not null, CONCAT(assay_long_name,"_InvitroPharmSum"),"(null)_InvitroPharmSum") as assay, "In Vitro Pharmacology Assay Summary Results" as subcategory
from (

(SELECT DISTINCT assay_id FROM `rising-field-149920.merck_dev.biodata_assay_ic50plus_sum_cmpd`
) t1
join
(SELECT assay_id, assay_long_name FROM `rising-field-149920.merck_dev.dlc_assay_view`) t2
on t1.assay_id = t2.assay_id
join
(SELECT assay_id, project_short_name FROM `rising-field-149920.merck_dev.biodata_project_assay_ct_view`) t3
on t2.assay_id = t3.assay_id
)

)
where view is not null and pro is not null and assay is not null and subcategory is not null
