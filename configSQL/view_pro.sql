
select distinct * FROM (
-- select distinct "DLCsum" as view, CONCAT(IF(project_long_name is not null, project_long_name, "(null)"),"_DLCsum") as pro from `rising-field-149920.merck_dev.v_dlc_sum_result_cmpd`
--
-- UNION ALL
--
-- select distinct "DLCevent" as view, CONCAT(IF(project_long_name is not null, project_long_name, "(null)"),"_DLCevent") as pro from `rising-field-149920.merck_dev.v_dlc_test_event_result`
--
-- UNION ALL
--
-- select distinct "invivoPKPD" as view, CONCAT(IF(program is not null, program, "(null)"),"_invivoPKPD") as pro from `rising-field-149920.merck_dev.v_in_vivo_pkpd`
--
-- UNION ALL
--
-- select distinct  "ChemProp" as view, CONCAT(IF(project is not null, project, "(null)"),"_ChemProp") as pro from `rising-field-149920.merck_dev.v_compound_phys_properties`
--
-- UNION ALL

select "MetID" as view, "Metabolite ID" as pro from `rising-field-149920.merck_dev.v_webmtbs_unified_view`

UNION ALL

select distinct "PPDM" as view, CONCAT(IF(project_short_name is not null, project_short_name, "(null)"),"_PPDM") as pro from `rising-field-149920.merck_dev.v_tier2_ppdm`

UNION ALL

select distinct "InvitroPharm" as view, CONCAT(IF(project_short_name is not null, project_short_name, "(null)"),"_InvitroPharm") as pro from `rising-field-149920.merck_dev.v_in_vitro_assay_results`

UNION ALL

select distinct "InvitroPharmSum" as view, CONCAT(IF(project_short_name is not null, project_short_name, "(null)"),"_InvitroPharmSum") as pro from `rising-field-149920.merck_dev.v_in_vitro_assay_sum_results`

UNION ALL

select distinct "DLCsum" as view, CONCAT(IF(project_long_name is not null, project_long_name, "(null)"),"_DLCsum") as pro from `rising-field-149920.merck_dev.v_dlc_sum_result_cmpd`

UNION ALL

select distinct "DLCevent" as view, CONCAT(IF(project_long_name is not null, project_long_name, "(null)"),"_DLCevent") as pro from `rising-field-149920.merck_dev.v_dlc_test_event_result`

UNION ALL

select distinct "invivoPKPD" as view, CONCAT(IF(program is not null, program, "(null)"),"_invivoPKPD") as pro from `rising-field-149920.merck_dev.v_in_vivo_pkpd`

UNION ALL

select distinct  "ChemProp" as view, CONCAT(IF(project is not null, project, "(null)"),"_ChemProp") as pro from `rising-field-149920.merck_dev.v_compound_phys_properties`

-- -- UNION ALL

-- select distinct "MetID" as view, CONCAT(IF(program_MetID is not null, program_MetID,"(null)"),"_MetID") as pro from `rising-field-149920.merck_dev.v_webmtbs_unified_view`

-- UNION ALL

-- select distinct "PPDM" as view, CONCAT(IF(project_short_name is not null, project_short_name, "(null)"),"_PPDM") as pro from `rising-field-149920.merck_dev.v_tier2_ppdm`

-- UNION ALL

-- select distinct "InvitroPharm" as view, CONCAT(IF(project_short_name is not null, project_short_name, "(null)"),"_InvitroPharm") as pro from `rising-field-149920.merck_dev.v_in_vitro_assay_results`

-- UNION ALL

-- select distinct "InvitroPharmSum" as view, CONCAT(IF(project_short_name is not null, project_short_name, "(null)"),"_InvitroPharmSum") as pro from `rising-field-149920.merck_dev.v_in_vitro_assay_sum_results`
)
where view is not null and pro is not null
